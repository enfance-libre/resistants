# Description

La liste des résistants enfance libre est gérée par le code dans ce repo git.

Ce code gère la liste des résistants en vue carte, vue liste, vue photo et en vue détaillée.

La liste est embarquée dans le site global squarespace à deux endroits:

- Sur la home page (https://www.enfance-libre.fr/)
- Sur la page résistants (https://www.enfance-libre.fr/resistants).

# Installation de l'environement de dev

- Installer vscode (https://code.visualstudio.com/) + les extensions recommandés
- Installer git (https://git-scm.com/downloads)
- Installer la version LTS de node (https://nodejs.org/en)
- Installer yarn (https://yarnpkg.com/getting-started/install)
- Checkout du repository git depuis framagit
- Installer les dependances (executer yarn install dans le reportoire du repo git)

# Ajouter une famille

## Quelles familles ajouter

On ajoute les familles après leur annonce sur Discord.

## Modifier le code pour ajouter une famille

- Retrouver la fiche de la famille dans Notion [Liste des Familles à publier sur le site](https://www.notion.so/5b69e02b296d4a578f8c8ab7fe8b05da?v=bd01159ed0bb42fa9dd16caa1fba93e7).
- Si la photo avec le logo enfance libre n'est pas sur la fiche de la famille il faut la copier depuis le message d'annonce Discord vers la propriété "Fichiers & médias".
- Ajouter la famille dans la liste des résistants dans le fichier [src/resistants.ts](src/resistants.ts) en prenant les informations depuis Notion
  - `id`: basé sur les noms des parents sans accents au format `prenom1-prenom2` ou `prenom-nom` pour les familles monoparentales sans accents ni caractères spéciaux.
  - `noms`: les noms complets des parents au format `Prénom1 Nom1 et Prénom2 Nom2` ou `Prénom1 et Prénom2 NomCommun`
  - `enfants`: Prénoms des enfants au format `Enfant 1, Enfant 2 et Enfant 3`
  - `departement`: Département parmi la liste de valeur défini dans le `type Departement` dans le fichier [src/Resistant.ts](src/Resistant.ts). Si le département manque dans la liste il faut le rajouter.
  - `presentation`: Le text de presentation pour le site web. A prendre depuis le contenu de la page notion. ⚠️ Il est souvent nécessaire de nettoyer les fins de lignes et saut de ligne ainsi que les points médian sur le contenu Notion. (https://jsstringconverter.bbody.io/ peut-être utilisé pour convertir le textet en string js, il faut cocher `Add Newlines` et mettre `Variable Name` à vide)
  - `latitude` & `longitude`: à déterminer avec google maps à partir de la ville de la famille et prendre dans l'url les 2 elements qui suivent le @ par exemple pour [Lyon](https://www.google.fr/maps/place/Lyon/@45.7579507,4.8351301,18724m/data=!3m2!1e3!4b1!4m6!3m5!1s0x47f4ea516ae88797:0x408ab2ae4bb21f0!8m2!3d45.764043!4d4.835659!16zL20vMGRwcmc?hl=fr&entry=ttu&g_ep=EgoyMDI0MDkyMy4wIKXMDSoASAFQAw%3D%3D) les latitude et longitude sont 45.7580409 et 4.752729
  - `date_declaration`: à prendre dans la preuve d'envoi du courrier de DC ou à defaut dans le corps du courrier ou à défaut la date d'intégration dans notion au format YYYY-MM-DD.
  - `mention`: mention additionnelle. Laissé vide la plupart du temps.
  - `video`: vidéo de présentation. Laissé vide la plupart du temps.
- Ajouter la photo: Ajouter la photo dans le répertoire [src/photos](src/photos) avec le nom `<id>.jpg` dimension 2048x2048
- Ajouter la déclaration: Ajouter le pdf de la déclaration dans le répertoire [src/declarations](src/declarations) avec le nom `<id>.pdf`

## Vérifier les changements

yarn dev peut être utilisé pour vérifier que le contenu pour la famille.

- http://localhost:5173/resistants/#/ permet de vérifier que la liste s’affiche bien
- http://localhost:5173/resistants/#/id permet de vérifier que le détail d’une famille s’affiche bien

## Publier les changements

- Commiter avec un commit message “feat: ajout de Prénom1 et Prénom2”
- Le push vers main va déclencher la CI de mise à jour du site web.

# Autres changement (addresse, liste d'enfants, statut)

## Modifier la famille

- Se baser sur le nom pour retrouver la famille dans resistant.ts(il n'ya pas d'identifiant commun qui lie a Notion)
- Appliquer les modifications:

* en cas de changement d'adresse modifié les latitude/longitude
* en case de nouveaux enfant mettre à jour la la propriété enfant
* en cas de fin de résistance définir la propriété date_fin_resistance au format YYYY-MM-DD

- tester les changements et commiter (voir au dessus)

# Archi

## Code

Le code utilisé dans squarespace pour le chargement du modules est le suivant:

```html
<div id="root"></div>
<script
  type="module"
  crossorigin
  src="https://enfance-libre.frama.io/resistants/resistants.js"
></script>
<link
  rel="stylesheet"
  href="https://enfance-libre.frama.io/resistants/resistants.css"
/>
```

Il faut faire attention à ce que que les points d'entrés exposé par la build soient toujours syncrhonisé avec le code dans squarespace.

## Gestion des urls du site squarespace vs frama.io

Le js et les assets du module résistants sont servis depuis la gitlab page de framagit (https://enfance-libre.frama.io/resistants) alors que le site est géré par squarespace sous le domaine https://www.enfance-libre.fr/.
Les assets doivent donc être gérés en url absolue, pour ceci on utilise notemment le paramètre "base" dans la config bite différement en dev et en build de ci.

## Différences entre la home page et page résistants

- La taille du composant
- Le mode par défaut: en mode carte sur la home page et liste sur la page résistants
- La gestion de la navigation: La navigation sur le détail dans la home page redirigre vers le détails dans la page résistants
