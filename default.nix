{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = with pkgs; [ nodejs-18_x (yarn.override { nodejs = nodejs-18_x; }) ];
}
