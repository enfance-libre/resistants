import react from "@vitejs/plugin-react-swc";
import { defineConfig } from "vite";
import { imagetools } from "vite-imagetools";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), imagetools()],
  base: "/resistants",
  build: {
    rollupOptions: {
      output: {
        // this name is used in the code to inject module in squarespace
        entryFileNames: "resistants.js",
        assetFileNames: (meta) => {
          const name = meta.name;
          if (name !== "squarespace-site.css" && name.indexOf(".css") > 0) {
            // this name is used in the code to inject module in squarespace
            return "resistants.css";
          }
          return "assets/[name]-[hash].[ext]";
        },
      },
    },
    outDir: "public",
  },
});
