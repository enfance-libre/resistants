export const Separator = () => (
  <div
    className="sqs-block horizontalrule-block sqs-block-horizontalrule"
    data-block-type="47"
    id="block-yui_3_17_2_1_1663092647761_201089"
  >
    <div className="sqs-block-content">
      <hr />
    </div>
  </div>
);
