import { IframeHTMLAttributes } from "react";

interface Props {
  youtubeId: string;
  width: string;
  height: string;
  onClick: () => void;
  className?: string;
  style?: IframeHTMLAttributes<HTMLIFrameElement>["style"];
}

export const EmbeddedYoutube = ({
  youtubeId,
  width,
  height,
  onClick,
  className,
  style = {},
}: Props) => (
  <div onClick={onClick} style={{ cursor: "pointer" }}>
    <iframe
      width={width}
      height={height}
      src={`https://www.youtube-nocookie.com/embed/${youtubeId}?si=KdcQ7aHwf-uzdNOZ&amp;controls=0`}
      title="YouTube video player"
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      allowFullScreen
      className={className}
      style={{ pointerEvents: "none", ...style }}
    ></iframe>
  </div>
);
