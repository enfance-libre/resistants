import React from "react";
import { Outlet } from "react-router";
import { ScrollRestoration } from "react-router-dom";

export const Layout = () => (
  <div>
    <ScrollRestoration />
    <Outlet />
  </div>
);
