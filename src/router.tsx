import React from "react";
import { createHashRouter } from "react-router-dom";
import { Layout } from "./Layout";
import NotFound from "./routes/NotFound";
import { ListeResistants } from "./routes/Liste";
import { PageResistant } from "./routes/PageResistant";

export const router = createHashRouter([
  {
    path: "/",
    element: <Layout />,
    errorElement: <NotFound />,
    children: [
      {
        path: "/",
        element: <ListeResistants />,
      },
      {
        path: ":idResistant",
        element: <PageResistant />,
      },
    ],
  },
]);
