import React from "react";
import { RouterProvider } from "react-router-dom";
import { DisplayModeContext } from "./context/DisplayModeContext";
import { HomeContext } from "./context/HomeContext";
import { router } from "./router";
import { ViewMode } from "./routes/ViewMode";
import "./style/global.css";
import "./style/row.css";
import "./style/thumbs.css";

interface Props {
  defaultViewMode: ViewMode;
  home: boolean;
}

export const App = ({ defaultViewMode, home }: Props) => {
  const [viewMode, setViewMode] = React.useState<ViewMode>(defaultViewMode);

  return (
    <DisplayModeContext.Provider
      value={{ mode: viewMode, setMode: setViewMode }}
    >
      <HomeContext.Provider value={{ isHome: home, setMode: () => {} }}>
        <RouterProvider router={router} />
      </HomeContext.Provider>
    </DisplayModeContext.Provider>
  );
};
