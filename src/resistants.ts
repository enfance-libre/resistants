import { Resistant } from "./Resistant";

export const resistants: Resistant[] = [
  {
    id: "ramin-marjorie",
    noms: "Ramïn Farhangi et Marjorie Bautista",
    enfants: "Zeÿa",
    departement: "Ariège",
    presentation:
      "“À l'image des objecteurs de conscience qui refusaient le service militaire pour ne pas s'entraîner à tuer, nous refusons l'idée d'un service scolaire obligatoire qui entraînerait notre enfant à obéir.”",
    video: "EPwB2-Mdmag",
    lien_declaration:
      "https://www.enfance-libre.fr/blog/declaration-officielle-ramin-marjorie",
    date_declaration: "2022-05-02",
    mention: "Cofondateurs du mouvement Enfance Libre",
    latitude: 43.17409,
    longitude: 1.40833,
  },
  {
    id: "jonathan-caroline",
    noms: "Jonathan Attias et Caroline Perez",
    enfants: "Lia et Mani",
    departement: "Haute-Vienne",
    presentation:
      'Qui peut prétendre à la liberté quand ses semblables ne le sont pas ?\n\nVoilà à quoi ressemble notre pensée et donc notre démarche.\n\nNous  souhaitons œuvrer à un monde plus juste, solidaire et consciencieux et agissons pour que nos enfants fassent partie de cet espoir pour lequel on dédie notre vie.\n\nRejoindre "enfance libre" est pour nous donc, une évidence.',
    video: "_wyI3ZsKPXk",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Lettre-Desobeissance-Civile-IEF-Jon-et-Caroline.pdf",
    date_declaration: "2022-09-01",
    date_fin_resistance: "2024-09-02",
    latitude: 45.63127,
    longitude: 0.79877,
  },
  {
    id: "amelie-vincent",
    noms: "Amélie Taveneau et Vincent Blondeau",
    enfants: "Simon",
    departement: "Maine-et-Loire",
    presentation:
      "“Depuis ses 3 ans, notre fils Simon « ne veut pas aller à l'école ». C'est donc en toute conscience que nous avons choisi de faire l'IEF avec lui, et plus précisément l'éducation auto-dirigée. Le contraindre à aller à l'école sans qu'il en ait l'élan de lui-même s'apparenterait à une violence éducative ordinaire pour nous. \n\nPar ailleurs, nous nous posons la question : quelles sont les chances pour les familles d'unschoolers pour que les dossiers d'autorisations soient acceptés alors que pour seul « projet pédagogique » soit inscrit le jeu libre des enfants ? Est-ce « entendable » pour l'Education Nationale alors que ça ne l'est parfois pas lors des contrôles académiques, et ce malgré toute la littérature sur le sujet ? \n\nCe pourquoi nous décidons d'entrer en désobéissance civile, afin de préserver cette liberté pédagogique pour notre fils, pour nous et pour toutes les familles en France. Pour préserver l'Enfance, pour préserver la Liberté.”",
    lien_declaration:
      "https://unschoole-ta-vie.webnode.fr/l/nous-nous-declarons-officiellement-en-desobeissance-civile-aupres-de-notre-gendarmerie/",
    date_declaration: "2022-04-29",
    date_fin_resistance: "2023-09-03",
    latitude: 47.56111,
    longitude: -0.67314,
  },
  {
    id: "marlene-benjamin",
    noms: "Marlène et Benjamin Vuillaume",
    enfants: "cinq enfants",
    departement: "Haute-Garonne",
    presentation:
      "“Nous sommes sept et le choix de vivre sans école, nous concerne tous. C’est un mode de vie à part entière, choisis en libre conscience par chacun d’entre nous, un rythme au quotidien qui nous rend heureux ! \n\nDe quel droit l’État devrait décider si nous pouvons continuer à vivre comme cela ou pas ?”",
    lien_declaration: "",
    date_declaration: "2022-05-01",
    latitude: 43.12728363110282,
    longitude: 1.2706931818441678,
  },
  {
    id: "audreyanne-michael",
    noms: "Audrey-Anne et Michaël Delhommeau",
    enfants: "Inessa et Gatien",
    departement: "Loire-Atlantique",
    presentation:
      "“Il y a 6 ans, nous avons, en conscience, fait le choix de l’instruction en famille et plus précisément des apprentissages libre, informel et autonome.\n\nCe choix a été concerté avec nos enfants, dans l’optique de préserver leur intégrité physique et morale, de répondre au mieux à leurs besoins,  rythmes et spécificités, et d’être à l’écoute de leur consentement et discernement intérieur. En effet, de notre point de vue, les apprentissages se font dans le plaisir et l’enthousiasme, au quotidien et tout au long de la vie, au fur et à mesure des explorations, des expérimentations, des rencontres et des projets, à la découverte du monde et de tout ce qui le compose.\n\nC’est pourquoi nous œuvrons aujourd’hui POUR le libre choix d’instruire nos enfants au regard de ce qui nous semble le plus juste pour eux et conformément à l’article 26.3 de la déclaration des droits de l’homme ainsi que comme le conseil constitutionnel le rappelle « pour l’intérêt supérieur de l’enfant ».”",
    lien_declaration:
      "https://www.enfance-libre.fr/blog/2022/5/26/entre-en-dsobissance-civile-de-la-famille-delhommeau",
    date_declaration: "2022-05-22",
    date_fin_resistance: "2024-09-02",
    latitude: 47.03496,
    longitude: -1.43546,
  },
  {
    id: "david-coralie",
    noms: "David et Coralie Renaudeau",
    enfants: "Lilou, Noa et Soan",
    departement: "Deux-Sèvres",
    presentation:
      "“C'est suite à l'observation d'un état de stress permanant et un constat de décalage important entre les besoins de nos enfants et ce qui leur était proposé dans le système scolaire conventionnel, que nous avons ensemble et individuellement choisi d'évoluer en instruction en famille il y a 6 ans.\n\nC'est donc à travers la pédagogie de l'exploration invitant à prendre soin de soi, de la relation à l'autre, du vivant et du monde, que nous souhaitons poursuivre ce chemin d'apprentissages libres et informels ensemble, à l'écoute de leurs élans, par projet et par le jeu, portés par le mouvement de la vie et la richesse des rencontres.\n\nC'est pourquoi nous souhaitons œuvrer pour le droit d’instruire librement nos enfants en contribuant à pérenniser ce droit fondamental pour les familles qui en ont besoin et les générations à venir.”",
    lien_declaration:
      "https://www.enfance-libre.fr/blog/2022/6/15/entre-en-dsobissance-civile-de-la-famille-renaudeau",
    date_declaration: "2022-06-15",
    latitude: 46.89229,
    longitude: -0.74299,
  },
  {
    id: "myriam-timothee",
    noms: "Myriam et Timothée Chatelain",
    enfants: "Sam et Mayan",
    departement: "Haute-Savoie",
    presentation:
      "“Nous sommes Myriam et Timothée Chatelain, parents de Sam et Mayan et nous souhaitons leur offrir la possibilité d'apprendre sereinement, dans un environnement qui respecte leurs besoins et leurs envies, et qui leur laisse le temps de vivre et être. Nous avons presque toujours été en instruction en famille, depuis 6 ans.\n\nNous sommes conscients de l'importance de l'existence de l'école publique et ne la dénigrons pas. Cependant nous pensons qu'elle n'est pas adaptée à tous les enfants et que cette loi ne va faire qu' augmenter les difficultés auxquelles font face les professeurs des écoles, ainsi que les cas de phobies scolaires et de dépressions chez les enfants et adolescents qui ne pourront plus être entendus.\n\nCette loi est pour nous un non-sens et va à l'encontre de l'intérêt général de l'enfant. En tant que parents, il est important pour nous de prendre clairement position sur ce sujet qui nous tient à cœur.”",
    lien_declaration: "",
    date_declaration: "2022-05-01",
    date_fin_resistance: "2023-09-03",
    latitude: 45.96173,
    longitude: 5.93189,
  },
  {
    id: "sarah-jerome",
    noms: "Sarah Piquart Delquié et Jérôme Delquié",
    enfants: "Elouan et Liam",
    departement: "Aude",
    presentation:
      "“Le choix de vie de l’IEF s'est installé naturellement à partir de 2015 suite à la demande insistante d'Elouan de ne plus aller à l'école. Liam n'ayant jamais souhaité découvrir cette institution nous avons continué notre vie en découvrant ensemble de multiples façons d’apprendre, de vivre, de transmettre, d’être en relation . Après le premier et unique contrôle d'Elouan en 2017, une relation exclusivement épistolaire avec l’éducation nationale s'est installée . Aujourd'hui l’éducation nationale nous met en demeure de scolariser Liam sans jamais l'avoir rencontré .\n\nNous nous engageons aujourd'hui dans ce mouvement de désobéissance civile afin que l' intérêt supérieur de nos enfants, leur épanouissement, leur liberté d'apprendre où, quand et comme ils le veulent soient véritablement respectés .”\n\n“Que vos choix reflètent vos espoirs et non vos peurs”  N. Mandela",
    lien_declaration:
      "https://www.enfance-libre.fr/blog/declaration-officielle-famille-delquie",
    date_declaration: "2022-05-15",
    latitude: 43.02086,
    longitude: 2.39191,
  },
  {
    id: "florence-alexis",
    noms: "Florence et Alexis Godart",
    enfants: "Alissa et de Mélina",
    departement: "Ille-et-Vilaine",
    presentation:
      "“Pour nous l'ief ne se résume pas à l'instruction de nos enfants, au fil  des années c'est simplement devenu notre vie, nous sommes heureux,  épanouis, libres de découvrir et d'apprendre quand nous le souhaitons.  C'est un enthousiasme d'apprendre avec nos filles tout comme de les voir  apprendre pour elles mêmes. Les rencontres avec les différentes  familles ief sont la richesse de cette vie que nous souhaitons protéger.\n\nNous nous estimons en droit de ne pas demander une autorisation qui mettrait en danger notre équilibre familial.”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Declaration-Florence-et-Alexis-Godart.pdf",
    date_declaration: "2022-08-01",
    latitude: 48.17018,
    longitude: -2.06637,
  },
  {
    id: "roseline-jonathan",
    noms: "Roseline et Jonathan Gondange",
    enfants: "Hanaé, Éline, Nael, Timoté et Téodore",
    departement: "Vaucluse",
    presentation:
      "“Dans notre famille, le choix de l’instruction à la maison est celui de tous. Nos 4 enfants, âgés de 9 ans pour la plus grande, et juste un an pour le dernier, s’épanouissent ensemble, et grandissent dans la joie et l’entraide. Les voir apprendre aussi vite et si aisément au quotidien à la maison est une joie chaque jour. Nos journées sont toutes remplies de petits bonheurs qui font que nous aimons notre vie de famille ensemble et à la maison.\n\nIl est pour nous inconcevable que cela s’arrête, sachant que les contrôles académiques chaque année sont favorables. En tant que fervents défenseurs de la liberté quelle qu’elle soit, nous nous engageons dans cette aventure de désobéissance à cette nouvelle loi, non seulement pour nos enfants, mais aussi pour tous les enfants qui souhaiteront au fil des années futures pouvoir connaître cette joie d’apprendre et de grandir autrement.”",
    lien_declaration: "",
    date_declaration: "2022-05-01",
    latitude: 43.86454,
    longitude: 5.242,
  },
  {
    id: "emmanuelle-philippe",
    noms: "Emmanuelle Chapleau et Philippe Lacot",
    enfants: "Salma, Soren, Ezra et Romy",
    departement: "Puy-de-Dôme",
    presentation:
      "“L'instruction en famille est le cadre de notre culture familiale. En effet, notre intention de parents est et demeurera le lien à nos enfants, et ainsi donc à leur sensibilité propre, leurs émotions, leurs singularités. Raison pour laquelle, au sein de notre foyer, chacun a l'opportunité de gouverner son instruction.\nNous sommes une famille de 6, dont l'aînée a connu un parcours scolaire jusqu'au moment où elle a eu besoin, pendant un an, de cet itinéraire bis que représente aussi l'instruction en famille. Pour nos fils, cette voie a été une évidence depuis leur plus jeune âge, et s'est inscrit dans une continuité affirmée de leur part, la liberté pour apprendre. \nLa désobéissance civile apparaît donc pour nous une œuvre essentielle et collective, constitutive de la liberté d’instruction, pour tous nos enfants. Parallèlement nous l'espérons, ce sera l'occasion d’ une prise en compte démocratique et une juste considération.”",
    video: "XVHiogVvQHM",
    lien_declaration:
      "https://www.enfance-libre.fr/blog/2022/6/3/entre-en-dsobissance-civile-de-la-famille-chapleau-lacot",
    date_declaration: "2022-06-03",
    latitude: 45.99331,
    longitude: 2.7651,
  },
  {
    id: "sita-pascal",
    noms: "Sita et Pascal Bargibant",
    enfants: "Aluna et Liara",
    departement: "Morbihan",
    presentation:
      "“L’instruction en famille est arrivée de manière naturelle dans notre foyer. Cette approche nous permet d’instruire nos filles en trois langues et d’orienter leurs activités, culturelles, sociales et sportives en fonction de leurs besoins, de leurs rythmes.\n\nNous sommes profondément attachés aux valeurs de la République et avons jusqu'à présent respecté le principe de déclaration d'IEF ainsi que les inspections, malgré les divergences de visions éducatives.\n\nAujourd'hui nous décidons de ne pas nous soumettre au nouveau régime d'autorisation en affichant publiquement notre désobéissance civile.\n\nUne telle dérive liberticide serait la porte ouverte à bien d’autres.\n\nNous entamons cette démarche de manière pacifique mais avec détermination et conviction.”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Declaration-Sita-et-Pascal-Bargibant.pdf",
    date_declaration: "2022-08-29",
    date_fin_resistance: "2024-09-01",
    latitude: 47.68069,
    longitude: -3.16379,
  },
  {
    id: "mathilde-cyril",
    noms: "Mathilde et Cyril Vannier",
    enfants: "Naïs, Ëvy et Saël",
    departement: "Ille-et-Vilaine",
    presentation:
      "“Parents de 3 enfants, en IEF depuis plusieurs années, nous sommes heureux de vivre cette expérience en famille et sentons une grande liberté d’être tant pour nous que pour nos enfants, qui peuvent explorer des domaines très divers et variés de la vie et d’eux même.\n\nLa proposition de loi faite par le gouvernement de demander des autorisations pour exercer un droit inscrit dans des textes officiels, nous apparait comme exagérée, abusive et liberticide. Nous nous sentons dans l’obligation d’agir pour la défense des droits humains.\n\nCette action corrèle également une pensée éducative que nous souhaitons transmettre à nos enfants, qu’il est de notre devoir de citoyen de faire respecter les valeurs civiques et sociales de notre pays et de notre humanité. Nous nous le devons à nous même, à nos enfants et aux générations futures pour que la vie soit protégée.”",
    lien_declaration: "",
    date_declaration: "2022-05-01",
    date_fin_resistance: "2024-03-19",
    latitude: 48.3959,
    longitude: -1.90139,
  },
  {
    id: "sylvaine-olivier",
    noms: "Sylvaine et Olivier Cougé",
    enfants: "Tanaë, Côme et Lune",
    departement: "Vendée",
    presentation:
      "“ Avec nos 3 enfants, Tanaë 13 ans, Côme et Lune 10 ans, nous vivons l'instruction en famille depuis 8 ans et sommes en lien avec de nombreuses familles IEF dans notre département de Vendée. \nNos enfants sont épanouis, et prêts à aller à l'école s'il le fallait, mais leur choix et le nôtre, reste actuellement porté vers l'IEF car nous aimons tous notre rythme de vie.\nDe plus, nous bénéficions, comme beaucoup, de 2 années d'autorisation supplémentaires puisque nos derniers rapports sont favorables.\nPour autant nous sommes profondément dérangés par ce qui se joue aujourd'hui.\nL'annonce de la loi a été source de beaucoup de réflexion en famille et nous y étions tous opposés depuis le début. \nMalgré toutes les manifestations, courriers ou rencontres pour se faire entendre, nos revendications communes sont restées sous silence et sans réponse aucune. Ainsi nos voix ne s'entendent pas.\nAlors, nous n'avons plus de choix. \nAujourd'hui nous refusons d'obéir à cette loi que nous trouvons absurde, tant par son fondement, que par son application. \nNous trouvons trop injuste la liberté qui nous serait accordée à nous, alors que d'autres en sont privés de manière complètement aléatoire et ridicule (selon les académies ou selon cet absurde critère d'être détenteur du bac par exemple)\nNous refusons cette obligation de demander l'autorisation d'être libres d'instruire nos enfants tel que nous le souhaitons.\nQui a décidé un jour que mettre un enfant au monde, c'était le partager avec l'éducation nationale ?\nQuel homme peut avoir ce droit d'avoir tout pouvoir sur la vie, le rythme et la qualité de vie d'une famille ? De toutes les familles ?\nC'est pourquoi nous décidons de ne pas demander cette autorisation qui, si elle est refusée, pourrait mettre en péril notre harmonie familiale. Nous décidons que notre tranquillité d'esprit ne soit pas tributaire d'une décision arbitraire et extérieure. Quitte à être un peu inquiets de la suite, nous préférons affirmer nos valeurs.\nC'est là aussi une manière de faire sens, dans la continuité de notre choix d'IEF il y a 8 ans. \nC'est pourquoi nous décidons de faire ce qui nous est juste, à la hauteur de nos moyens, pour défendre la liberté des hommes à instruire leurs enfants avec la pédagogie qui leur convient, pour défendre la liberté des enfants qui ne veulent pas aller sur les bancs de l'école...\nNous choisissons la liberté et la diversité des approches, dans le respect de la constitution, et attendons avec impatience le retour à un régime déclaratif.”",
    video: "ky-EfVTLzIA",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Notre-declaration-de-desobeissance-Civile-Sylvaine-et-Olivier-Couge.pdf",
    date_declaration: "2022-08-29",
    date_fin_resistance: "2023-09-03",
    latitude: 46.41427,
    longitude: -1.57619,
  },
  {
    id: "julie-jeanchristophe",
    noms: "Julie et Jean-Christophe Reboutier",
    enfants: "Mona, Ewenn et Lukaz",
    departement: "Morbihan",
    presentation:
      "“Nous sommes une famille de 5 personnes et chacun a le libre choix de ses apprentissages.\n\nMona choisi le lycée. Ewenn, lui est allé à l’école jusqu’à fin avril 2022. C’était  son souhait d’y aller. Mais l’école n’était pas adaptée à ses besoins.  Si bien que le jour de son anniversaire et malgré la promesse de l’y  fêter avec ses amis, il ne voulait pas y aller.\n\nDepuis  un an Ewenn oscillait entre phobie scolaire et désir d’aller à l’école,  tous les aménagements possibles avaient été mis en place par le corps  enseignant. Nous ne souhaitions pas, à 5 ans, lui coller une étiquette limitante dans la construction de son identité, aussi lorsque l’on nous a proposé de faire un dossier dans  le but de potentiellement obtenir une assistante de vie scolaire, au  risque de créer une barrière encore plus grande entre lui et ses  camarades, nous avons fait le choix de l’instruction en famille.\n\nCela n’a pas entravé son évolution, bien au contraire, avec des parents disponibles, à l’écoute, le libre choix de ses apprentissages, Ewenn a exprimé le désir d’apprendre à compter.\n\nAlors qu’il comptait difficilement au-delà de  3/4 fin mai, il compte maintenant, aisément jusqu’à 20, fait de simples  additions et soustractions et compte jusqu’à plusieurs centaines avec  de l’aide. Il associe bien chiffre et dénombrement, ce qui n’était pas  le cas. Sa psychomotricienne, qui avait des réserves le concernant sur l’IEF, fait le même constat que nous ; Ewenn est plus posé, plus en lien, et mieux dans ses baskets.\n\nNous aurions pu faire une demande d’autorisation car  nous remplissons plusieurs cases du formulaire. Mais cela aurait  signifié que nous adhérons, légitimons cette loi qui relègue les parents  au rang d’ignorants, d’incompétents, incapables de prendre soin et de  connaître leur(s) propre(s) enfant(s). Comment légitimer un tel raisonnement ? Comment valider une démarche qui laisse une si grande place à l’arbitraire ?\n\nAujourd’hui en ce n’est pas simplement l’instruction en famille que nous défendons mais la liberté de choisir la manière dont sont instruits nos enfants. \n\nCe  n’est pas l’école/l’éducation nationale à laquelle nous nous opposons  mais la substitution de l’Etat sur le droit des parents à choisir le  mode d’apprentissage le plus adapté à leurs enfants. \n\nEt bien au-delà, c’est la liberté et la démocratie que nous défendons.\n\nC’est pourquoi en tant que citoyens éclairés, il nous apparaît comme une obligation d’entrer en désobéissance civile.\n\nNotre démarche est et restera pacifique et déterminée.”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/desobeissance-civile-Julie-et-JC.pdf",
    date_declaration: "2022-08-22",
    latitude: 47.8295428,
    longitude: -3.2307308,
  },
  {
    id: "marianne-denis",
    noms: "Marianne Cramer et Denis Gaudillère",
    enfants: "Amy et Nathan",
    departement: "Tarn",
    presentation:
      "“Nous sommes convaincus qu’une éducation pour tous n’est pas forcément synonyme d’éducation pour chacun. Nous nous élevons contre toute tentative d’uniformiser de façon radicale les pratiques éducatives, de promouvoir une forme de « monoculture éducative » où tous les enfants devraient être scolarisés. \n\nNous nous élevons contre cette injustice profonde qui veut contraindre tous les enfants à entrer dans le même moule éducatif.”\n\n« Il est plus désirable de cultiver le respect du bien que le respect de la loi » \n\nHenry-David Thoreau",
    video: "ePGVpnGlPCE",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Courrier-Desobeissance_Marianne-Denis-VDEF.pdf",
    date_declaration: "2022-09-05",
    latitude: 43.90516,
    longitude: 2.31513,
  },
  {
    id: "mathilde-dorian",
    noms: "Mathilde Catalifaud et Dorian Bouchet",
    enfants: "Owen, Livia et Mira",
    departement: "Lot",
    presentation:
      "“Lorsque notre premier enfant est venu au monde, c'était déjà une évidence: nous  sommes là pour l'accompagner au long de sa vie et non pour le diriger.  Nous avons fait le choix de protéger les rythmes et les besoins de nos  enfants.\n\nNous pensons que cette  loi n'est que le début d'une intrusion dans la vie des familles et de  leur manière de vivre. Interdire une manière d'enseigner, faire fermer  des écoles de manière arbitraire sur des prétextes mensongers ne peut  être soutenu.\n\nLa loi nous  impose (avec raison) de protéger l'intérêt supérieur de nos enfants,  sans violence éducative, psychologique ou physique. Aller à l'encontre  de leur choix concernant leur volonté de faire l'instruction en famille  irait à l'encontre de cette loi et de nos convictions.”",
    video: "3DAotvErssg",
    lien_declaration:
      "https://www.enfance-libre.fr/s/desobeir-Mathilde-Dorian.pdf",
    date_declaration: "2022-09-01",
    latitude: 44.67177,
    longitude: 2.10137,
  },
  {
    id: "karene-jalil",
    noms: "Karène et Jalil Arfaoui",
    enfants: "Jade et Nora",
    departement: "Tarn",
    presentation:
      "“Nous refusons de sacrifier notre vie aux exigences illégales d’un état qui nous manipule.\n\nNous refusons d’appliquer une loi et une décision que nous ne comprenons pas et que personne ne sait justifier.\n\nNous refusons d’être les faire-valoir d’une école qui a été soigneusement détruite par nos gouvernants.\n\nNous refusons d’être privés de nos droits les plus fondamentaux.”\n\n« Chacun a la responsabilité morale de désobéir aux lois injustes »  (Martin Luther-King)",
    video: "hMFheWUQK-s",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Courrier-desobeissance-Arfaoui.pdf",
    date_declaration: "2022-09-05",
    latitude: 43.92748,
    longitude: 2.14432,
  },
  {
    id: "nicolas-elodie",
    noms: "Nicolas Verrier et Elodie Duchesne",
    enfants: "Rayan, Maylie et Aaron",
    departement: "Haute-Garonne",
    presentation:
      "“Nos enfants ont toujours eu le choix d'aller à l'école ou non. Ils ont décidé en conscience de rester en ief. Nous sommes heureux de leur apporter autant de richesse et de liberté concernant leurs apprentissages.  \n\nNous avons fait un choix de vie où l'on prend le temps qu'il faut pour apprendre, apprendre à vivre ensemble, à se respecter, à prendre soin de soi, des autres, à apprendre la vie tout simplement.\n\nA ce jour nous ne pouvons et ne voulons plus faire marche arrière. Nous souhaitons garder cette liberté d'instruction et peu importe les raisons qui ont mené vers ce mode d'apprentissage, tous les parents devraient pouvoir garder la liberté d'instruire leurs enfants comme ils l'entendent.  \n\nNous sommes prêts à défendre cette liberté d'instruire nos enfants pour nous mais également pour toutes les autres familles qui ont perdu ce droit arbitrairement. \n\nNous sommes donc entrés en désobéissance civile afin de défendre et conserver ce droit. Nous sommes dans un pays « libre » et, en tant que citoyens et parents, nous avons le droit de nous adapter au rythme de nos enfants. Les rendre motivés et heureux dans leurs apprentissages serait-il en 2022 puni par la loi ?”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/declaration-Elodie-et-Niocolas.pdf",
    date_declaration: "2022-08-31",
    latitude: 43.07031,
    longitude: 0.64606,
  },
  {
    id: "jerome-gwenaelle",
    noms: "Jérôme et Gwénaëlle Poiroux",
    enfants: "Manon, Océane et Chloé",
    departement: "Aveyron",
    presentation:
      "“Nous sommes en tant que parents issus de l'école républicaine. A l'arrivée de notre fille ainée, par mimétisme, nous l'avons inscrite dans l'école du village dès ses 3 ans. Mais malgré la meilleure volonté des enseignants, le système éducatif n'était pas approprié pour elle. \n\nC'est en 2016 que nous avons découvert, validé et adopté l'instruction en famille. Et c'est tout naturellement que nous avons continué pour les petites sœurs. Oui, c'est un engagement prenant, une vraie philosophie de vie, mais cela ne nous a pas découragé, au contraire!\n\nNous décidons de devenir résistants car nous trouvons aberrant de devoir demander une autorisation pour un droit légitime d'une part, autorisation qui de surcroît se limite à quatre catégories seulement! Comment peut-on croire qu'on puisse ainsi réduire la diversité des familles en IEF! \n\nC'est également notre façon de soutenir les autres familles, car nous pensons que c'est maintenant qu'il faut se battre afin d'empêcher l'interdiction pure et simple de l'IEF en 2024. Nous sommes profondément convaincus que cette complémentarité d'instruction est nécessaire, vitale dans le système d'éducation français afin de proposer une sortie de secours pour certains enfants, une bouffée d'oxygène pour d'autres ou tout simplement un cocon d'épanouissement hors des murs d'un système éducatif qui ne leur est pas adapté.”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Lettre-de-desobeissance-civile-Famille-POIROUX.pdf",
    date_declaration: "2022-09-30",
    latitude: 44.57207,
    longitude: 2.39564,
  },
  {
    id: "isabelle-nicolas",
    noms: "Isabelle et Nicolas Sibert",
    enfants: "Pauline, Flora, Jérémie et Athina",
    departement: "Creuse",
    presentation:
      "“La descolarisation s'est présentée à nous de façon pratico-pratique pour partir en itinérance en camion il y a 11 ans. Pour nos deux premières filles ça a été un soulagement alors qu'elles ne s'étaient       jamais plaintes de l'école. Les deux derniers non jamais été scolarisés. Au fil des années notre choix c'est fait de façon plus consciente et plus éclairée. Aujourd'hui nous ne pouvons laissé nos enfants faire le choix de retourner en formation que lorsque leur demande est vraiment motivée. Nous les voyons s'épanouir physiquement sans restriction, relationnellement sans complexe, humainement pleinement. Nous leur laissons le temps d'être.\n\nDe part nos nombreuses rencontres nous avons souvent discuter avec des parents perdus avec des enfants en mal-être à l'école, et certains, de part notre partage d'expérience ont déscolarisé pour       un mois, un ans ou plus, leurs enfants. Pour tous cela a été bénéfique. Les enfants ont souvent retrouvés confiance en eux, et le goût d'apprendre. Avec la demande d'autorisation, cette respiration n'est plus possible.\n\nPour nous la demande d'autorisation est clairement une entrave à la liberté d'être ! Nous ne nous plierons pas devant ces valeurs de surveillance, d'élitisme, et de séparatisme que la loi promeut.”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/courrier-DASEN-Sibert.pdf",
    date_declaration: "2022-09-29",
    latitude: 45.78373,
    longitude: 1.98893,
  },
  {
    id: "helene-d",
    noms: "Hélène Drulhe",
    enfants: "Kin Rosalie",
    departement: "Aveyron",
    presentation:
      "En observant grandir ma fille avec autant d'entrain, de curiosité, de gourmandise, d'enthousiasme, le libre choix de l'instruction en famille, à travers notamment les apprentissages libres et autonomes, m'apparait comme une évidence. Seulement voilà, ce libre choix n'est plus, il est désormais soumis à un régime d'autorisation, niant les droits de l'enfant et des parents.\n\nFace aux incohérences et aux non-sens de cette loi, et forte du bien fondé de la démarche, je m'inscris dans la désobéissance civile, pour le libre choix d'instruire nos enfants.",
    lien_declaration:
      "https://www.enfance-libre.fr/s/lettre-EL-helene-drulhe.pdf",
    date_declaration: "2022-10-28",
    date_fin_resistance: "2023-09-03",
    latitude: 44.17386,
    longitude: 2.53359,
  },
  {
    id: "deborah-florian",
    noms: "Déborah et Florian Muller",
    enfants: "Zoé, Zendé, et Zora",
    departement: "Ardèche",
    presentation:
      "Nous avons déscolarisé nos 3 enfants en 2020 suite à la gestion catastrophique de la crise covid. Zora et Zendé ont ensuite souhaité retourner à l école car nous avions déménagé entre temps et ils avaient envie d’être avec leurs amis en classe. Zoé quant à elle souhaite continuer l ief car elle a eu de mauvaises expériences au collège et se sent mieux à la maison; Zoé est vraiment beaucoup plus épanouie aujourd'hui. \n\nNous avons donc rejoint Enfance Libre, car protéger la liberté de choix d’instruction pour nos enfants est primordial.",
    lien_declaration:
      "https://www.enfance-libre.fr/s/lettre-desobeissance-civile-deborah.pdf",
    date_declaration: "2022-11-01",
    date_fin_resistance: "2023-09-01",
    latitude: 44.84107,
    longitude: 4.60679,
  },
  {
    id: "ophelie-sylvain",
    noms: "Ophélie Singeot et Sylvain Bouvet",
    enfants: "Enakiel, Aïnoura et Yoéli",
    departement: "Aveyron",
    presentation:
      "“Nous sommes une famille de 5 personnes avec 3 enfants de 5 à 7 ans n'ayant jamais été à l'école. Depuis leur naissance, nous grandissons avec eux et apprenons le respect des rythmes de chacun, des envies bourrées d'enthousiasme, de la vie et de la liberté! Mes connaissances en psychologie de l'enfant me permettent d'affirmer qu'il s'agit du meilleur choix pour notre famille jusqu'à ce qu'un jour ils décident par eux-mêmes une autre orientation qui leur conviendrait mieux. Nous entrons en désobéissance civile parce que pour nous, il n'est pas possible de sacrifier bonheur et liberté sur l'autel d'une loi mensongère, injuste, illégitime et inégale. Nous ne changerons jamais notre vie sauf si l'un d'entre nous le décide. L'ingérence de l'état, qui soit-dit en passant devient de plus en plus dictatorial, nous paraît dangereux. Les citoyens forment l'état, il leur revient donc de s'opposer aux débordements des dirigeants. Nous attendons le retour au régime déclaratif en espérant que la justice relève tous les articles de loi auxquels s'opposent cet article 49 de la loi sur les séparatismes.\n\n Nous sommes nomades et les réponses futures nous éclaireront sur un éventuel départ du pays comme beaucoup d'autres familles en IEF. En attendant nous affirmons haut et fort notre mécontentement: nous sommes heureux, nos enfants sont instruits, nous sommes libres, épanouis et conscients ❤️”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Desobeissance-civile-Sylvain-Ophelie.pdf",
    date_declaration: "2022-11-26",
    latitude: 44.39914,
    longitude: 2.7699,
  },
  {
    id: "marina-frederic",
    noms: "Marina Cremers et Frederic Blanpain",
    enfants: "Léo-Pol, Joa-Louca, Lili-Rose, Luna-Violette et Emilien",
    departement: "Creuse",
    presentation:
      "“Nous sommes parents de sept enfants. Nous avons toujours vécu au plus près de nos valeurs.\n\n\"Comment je pourrais rester moi-même si j'arrête de faire tout ce en quoi je crois.\" (Desmond T. Doss)\n\nChoisir pour la venue au monde de ses enfants la douceur de son foyer, vivre plus simplement en étant plus proches les uns des autres, choisir une parentalité positive puis, tout naturellement, prendre le chemin de l'IEF sont de ces choix qui font partie intégrante de nos vies.\n\nIl y aurait tant à dire sur l'IEF... Chaque famille à sa propre façon de vivre l'IEF; tout comme il existe une variété de pédagogies et de méthodes d'apprentissages (Steiner-Waldorf, Decroly, Freinet, Montessori, Charlotte Mason, l'unschooling et j'en passe...). Et c'est cette variété qui est une véritable richesse. L'IEF permet de s'adapter aux besoins divers des enfants et leur permet d'apprendre à leur propre rythme. L'IEF contribue à l'équilibre et à l'épanouissement d'un certain nombre d'enfants, aujourd'hui, en France.\n\n Vouloir rentrer tous ces enfants dans un même moule serait pour eux et leurs familles une véritable souffrance.\n\n Les enfants portent naturellement en eux cette soif d'apprendre. Ils sont tout à fait aptes à apprendre de façon informelle, lorsqu'ils ont la possibilité d'explorer à l'envie leurs propres intérêts.\n\nDès lors, pourquoi vouloir priver ces enfants et leurs familles de quelque chose qui fonctionne?\n\nL'IEF est une liberté fondamentale qui ne peut être soumise à une autorisation arbitraire.\n\n Pour défendre cette liberté fondamentale, nous nous unissons aux familles d'Enfance Libre, pour faire entendre notre voix et notre refus de devoir demander une autorisation pour ce qui est un droit.\n\n Au nom de tous les enfants qui sont l'avenir, pour demain...”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/decldesciv-marina-frederic.pdf",
    date_declaration: "2022-11-15",
    latitude: 46.1892,
    longitude: 2.33353,
  },
  {
    id: "sonia-jeanchristophe",
    noms: "Sonia et Jean-Christophe Herbiet",
    enfants: "Uwe, Keza et Aïna",
    departement: "Meurthe-et-Moselle",
    presentation:
      "“L’IEF représente pour nous plus qu’une autre forme d’instruction. C’est vivre en conscience, dans le respect de nos rythmes et de nos individualités. L’instruction en famille est aujourd’hui un des piliers sur lequel repose l’équilibre de notre famille. Nous ne pouvons laisser quiconque s’immiscer dans notre espace le plus intime sans en requérir notre consentement. La nouvelle loi sur l’IEF représente donc une menace pour nos libertés individuelles et c’est pourquoi nous décidons d’entrer en désobéissance civile.”\n\n“Quand un peuple ne défend plus ses libertés et ses droits il devient mûr pour l'esclavage.” Jean-Jacques Rousseau”",
    lien_declaration:
      "https://www.enfance-libre.fr/s/Lettre-dc-Famille-Herbiet.pdf",
    date_declaration: "2022-12-01",
    latitude: 48.65322,
    longitude: 6.19711,
  },
  {
    id: "arnaud-pauline",
    noms: "Arnaud Raymond et Pauline Bonvoisin",
    enfants: "Hugo et Noé",
    departement: "Haute-Garonne",
    presentation: "",
    latitude: 43.458669210640394,
    longitude: 2.003393843386162,
    date_declaration: "2023-05-08",
    date_fin_resistance: "2024-05-31",
    video: "Z3MZU3x0rL4",
  },
  {
    id: "alexandre-julie",
    noms: "Alexandre Lourenço et Julie Porcel",
    enfants: "Ulyssandra",
    departement: "Landes",
    presentation:
      " La raison principale de notre choix de l'Instruction En Famille est de pouvoir vivre à 100 % l'enfance de notre Princesse, Ulyssandra. La seconde raison est de lui apporter le maximum d'attention afin qu'elle puisse acquérir toutes les connaissances qu'elle désire et ce sans contrainte.\n" +
      "\n" +
      "Depuis 3 ans, nous assurons son éducation et son instruction et notre fille a trouvé dans cet apprentissage tout l'épanouissement nécessaire.\n" +
      "\n" +
      "Aujourd'hui, les \" instruits \" de notre gouvernement ont décidé d'imposer un régime d'autorisation, très restrictif et sans réelle valeur. En réponse, nous avons décidé d'entrer en Désobéissance Civile car cette nouvelle loi bafoue nos libertés d'enseignement et de circulation.\n" +
      "\n" +
      "Interdiction est faite aux parents de pratiquer l'Instruction En Famille en invoquant une loi dite \" loi séparatisme \". Donc en pratiquant l'Instruction En Famille, nous sommes considérés comme des fanatiques dangereux pour nos enfants ou pour la nation.\n" +
      "\n" +
      "L'intérêt supérieur de notre enfant est au centre de cette nouvelle loi, pourtant le souhait de notre enfant n'est pas pris en compte. Surtout quand elle affirme ne pas vouloir aller dans un établissement scolaire ! \n" +
      "\n" +
      "L'éducation nationale qui n'a jamais contrôlé Ulyssandra, nous adresse une mise en demeure de scolariser, que nous ne respecterons pas évidemment.\n" +
      "\n" +
      "Nous continuerons à instruire nous-même notre fille dans le respect de son rythme d’apprentissage et de son rythme biologique.\n" +
      "\n" +
      "\n" +
      '" Le rôle de l’éducation est d’aider chacun d’entre nous à vivre librement et sans peur. ~ Jiddu Krishnamurti " "',
    latitude: 44.11489200339857,
    longitude: -0.011507420350574667,
    date_declaration: "2023-04-17",
  },
  {
    id: "marie-alexis",
    noms: "Marie et Alexis Raoult Bulion",
    enfants: "Gabriel",
    departement: "Morbihan",
    presentation:
      "L'Instruction En Famille est un mode de vie choisi afin de prioriser le bien-être et l'épanouissement de notre enfant. \n" +
      "Nous refusons de laisser une loi liberticide et des décisions arbitraires nous priver de notre droit à l'instruction. Avec cette loi, l'IEF devient une exception et une exception soumise à autorisation ne peut être une liberté.\n" +
      "Nous souhaitons exercer notre droit en tant que \"parents de choisir, par priorité, le genre d'éducation donnée à leurs enfants\" (Art. 26, Déclaration Universelle des Droits de l'Homme).\n" +
      "C'est pourquoi nous entrons en désobéissance civile et demandons le retour à un régime déclaratif de l'IEF.",
    latitude: 47.67608267119849,
    longitude: -3.169116314329733,
    date_declaration: "2023-05-27",
  },
  {
    id: "lola-morgan",
    noms: "Lola Pouchin et Morgan Le Calvé",
    enfants: "Corto",
    departement: "Vaucluse",
    presentation:
      "Notre fils est arrivé il y a trois ans et nous avons beaucoup appris.\n" +
      "Nous avons pris soin de lui.\n" +
      '"Allons-nous trop donner de notre personne ?" a été notre interrogation.\n' +
      "Nous avons appris à prendre soin de nous et nous avons surtout reçu.\n" +
      "Nous avons quitté la ville pour l'entourer de chants d'oiseaux plutôt que de bruits de véhicules.\n" +
      '"Allons-nous nous couper de notre vie sociale ?" a été notre interrogation.\n' +
      "Nous avons appris à vivre connectés aux humains, mais aussi à d'autres formes de vie.\n" +
      "Nous l'avons laissé écouter ses sensations corporelles.\n" +
      '"Allons-nous nous oublier ?" a été notre interrogation.\n' +
      "Nous avons appris à remettre notre corps au centre.\n" +
      "Enfin, nous l'avons écouté lorsqu'il a refusé d'aller à l'école.\n" +
      '"Allons-nous pouvoir continuer à travailler ? Allons-nous devenir marginaux ? Comment pourrons-nous avoir du temps pour nous ?"\n' +
      "Nombreuses sont encore nos interrogations.\n" +
      "Nous sommes en train d'apprendre à dire non. Non à un travail qui nous rend malades, non à une société qui rend la Terre malade, non à la rentabilisation de tous les instants de nos vies.\n" +
      "Pour notre fils, pour nous, pour tous les enfants et leurs parents qui choisissent une vie différente, pour la diversité des choix et des idées, aujourd'hui, nous avons le courage de dire non à cette loi qui restreint nos libertés.",
    latitude: 43.89521556605255,
    longitude: 5.566065844440149,
    date_declaration: "2023-09-05",
  },
  {
    id: "perrine-francois",
    noms: "Perrine et François Jauffret",
    enfants: "Gaëlle, Eloïse et Maëlia",
    departement: "Jura",
    presentation:
      "Nous sommes Perrine et François, enseignants et parents de 3 filles. \n" +
      "Nous avons choisi l’instruction en famille pour ralentir, pour changer de mode de vie, pour donner du temps aux enfants et à notre famille. L’instruction en famille nous permet d’avoir un équilibre de vie quotidien qui nous convient. \n" +
      "\n" +
      "Demander l’autorisation à une administration pour définir notre mode de vie n’est pas acceptable dans un pays de liberté. Dans ce cadre nous nous mettons en désobéissance civile jusqu’au retour au régime déclaratif.\n" +
      "\n" +
      "Nous avons choisi l’instruction en douceur,\n" +
      "Pour offrir aux enfants un temps qui demeure.\n" +
      "Du temps pour rêver, explorer et grandir,\n" +
      "Dans ce voyage où chaque jour l’on respire.\n" +
      "\n" +
      "Du temps pour les contes, les pages tournées,\n" +
      "Où l’apprentissage se fait en liberté.\n" +
      "A travers les livres, les mondes prennent vie,\n" +
      "Et les esprits curieux s’épanouissent ici.\n" +
      "\n" +
      "Du temps pour la nature, les saisons à venir\n" +
      "A travers les forêts, les prairies à parcourir.\n" +
      "Chaque brin d’herbe, chaque fleur deviennent enseignants,\n" +
      "Et la terre nous offre son savoir étourdissant.\n" +
      "\n" +
      "Du temps pour la famille, aux liens à renforcer\n" +
      "Dans l’écrin de l’amour, des savoirs à partager.\n" +
      "Chaque instant passé ensemble devient un trésor,\n" +
      "Et la vie se colore d’un bonheur encore plus fort.\n" +
      "\n" +
      "Alors que le temps s’écoule, précieux et lent,\n" +
      "Nous tissons des souvenirs, telle une étoffe d’argent.\n" +
      "L’instruction en famille, un choix qui nous unit,\n" +
      "A travers le temps qui passe, nos cœurs sont en harmonie. ",
    latitude: 46.974937884101415,
    longitude: 5.798187689414306,
    date_declaration: "2023-09-01",
  },
  {
    id: "andrea-gherard",
    noms: "Andrea Malterre et Gherard Christon",
    enfants: "Garance et Gabriel",
    departement: "Côte-d'Or",
    presentation:
      " En 2023, il nous paraît essentiel d'accompagner nos enfants dans la découverte du monde qui les entoure, dans le lien au vivant, de les sensibiliser à la protection de ce qui les entoure, de la Terre sur laquelle ils vivent et de tous les autres êtres vivants. C'est pour nous une priorité. \n" +
      "L'IEF ne se résume pas à \"instruire\" nos enfants d'une manière qui serait calquée sur celle de l'Éducation Nationale. C'est un véritable mode de vie dans lequel l'exploration est reine. Apprendre par l'expérience, découvrir, expérimenter de manière concrète, être dans le mouvement de la vie, côtoyer des enfants et adultes de tous âges est aussi essentiel au bon développement de l'enfant selon nous. \n" +
      "L'IEF est cette opportunité incroyable d'apprendre en s'amusant, d'avoir du temps pour jouer, construire sa pensée, vivre et ainsi apprendre les choses de la vie quotidienne d'une famille... Tout en permettant à l'enfant, au fil de ses périodes sensibles, d'acquérir les notions du socle commun. \n" +
      "La liberté d'instruction est capitale. Chaque enfant, chaque parent, chaque famille doit pouvoir à chaque instant de la vie décider du mode d'instruction qui est adapté pour elleux. Pouvoir aller a l'école si cela nous convient et convient à l'enfant ou en sortir quand bon nous semble et quelle qu'en soit la raison, est essentiel à nos yeux. Pour cela, nous réclamons le retour au régime déclaratif. Et dans cette attente, la désobéissance civile nous paraît être l'unique voie pour militer a la fois activement et pacifiquement en faveur de la cause. \n" +
      "« Chacun a la responsabilité morale de désobéir aux lois injustes » - Martin Luther-King",
    latitude: 47.28036977323367,
    longitude: 4.229962943628936,
    date_declaration: "2023-08-08",
  },
  {
    id: "manon-adrien",
    noms: "Manon Boucher et Adrien Pecqueur",
    enfants: "Lou, Lino et Awadi",
    departement: "Charente-Maritime",
    presentation:
      "Nous sommes Lou, 4 ans, qui serait entré en grande section à la rentrée, Lino, 8ans, qui aurait fait sa rentrée en CM1, Awadi, 16ans, lycéenne en vacances et bientôt en terminale, Manon, 36 ans, professeur des écoles en disponibilité et en questionnement et Adrien, 37 ans, autoentrepreneur dans le domaine du spectacle, de l'animation et du jeu. Nous habitons à la Rochelle depuis 2019 où nous profitons de l'océan et des nombreux aménagements cyclables. Notre expérience de l'école nous a amenés à proposer l'IEF à nos 3 enfants pour l'année prochaine dans l'idée d'expérimenter une autre forme d'instruction. Awadi a refusé, souhaitant poursuivre sa scolarité comme elle l'avait commencée, dans le lycée Saint Exupéry où elle a déjà passé ses deux dernières années scolaires. En revanche, Lino et Lou ont été séduits par le projet. L'idée de disposer de plus de temps ensemble, de nous adapter au rythme naturellement propre à chacun et de pouvoir découvrir de nouvelles choses au plus près de nos préoccupations et intérêts du moment nous semble une opportunité intéressante. Notre situation professionnelle nous permet aujourd'hui, d'envisager sereinement cette expérience. Nous espérons par ce projet et par notre association à votre lutte, arriver à faire bouger la loi pour renouer avec la liberté de choisir ce qui convient le mieux à nos enfants.",
    latitude: 46.1610926712308,
    longitude: -1.1483218482524866,
    date_declaration: "2023-07-08",
    date_fin_resistance: "2024-11-19",
  },
  {
    id: "lea-frederic",
    noms: "Léa et Frédéric Helias",
    enfants: "Nolan et Nolwen",
    departement: "Seine-et-Marne",
    presentation:
      "L'instruction en famille est venue sonner comme une évidence aux 2 ans de notre aîné qui en a maintenant 8. Nous voulions l'accompagner dans ses besoins spécifiques. C'est tout naturellement que nous avons poursuivi le même chemin pour sa sœur née en 2019.\n" +
      "L'IEF n'est pas un choix contre l'école mais simplement ce qui correspond le mieux à notre famille. C'est plus qu'un mode d'instruction, c'est notre mode de vie.\n" +
      "C'est pourquoi, après un refus cette année pour instruire notre fille, nous avons décidé d'entrer en désobéissance civile pour conserver la liberté d'instruire nos enfants mais aussi pour œuvrer collectivement pour tous les enfants. \n" +
      "Notre démarche est publique et restera bien entendu totalement pacifique. ",
    latitude: 48.62733109998248,
    longitude: 2.5919796530166983,
    date_declaration: "2023-09-05",
  },
  {
    id: "bertrand-angelique",
    noms: "Bertrand et Angélique Le Moal",
    enfants: "Alexandre et Juliette",
    departement: "Corse-du-Sud",
    presentation:
      "Comment obéir à une loi voulue par un seul homme ?\n" +
      "Comment obéir à une loi justifiée par des mensonges ?\n" +
      "Comment obéir à une loi que le Conseil d’état assimilait à un régime d’interdiction insuffisamment justifié et proportionné ?\n" +
      "Comment obéir à une loi d’interdiction déguisée en régime d’autorisation ?\n" +
      "Comment obéir à une loi votée par nos assemblées sur des chiffres faux ?\n" +
      "Comment obéir à une loi qui ne respecte pas les traités internationaux ?\n" +
      "Comment obéir à une loi que même notre Président ne respecte pas en affirmant que « l’école est obligatoire » ?\n" +
      "Comment obéir à une loi néfaste pour nos enfants ?\n" +
      "N’ayant pas trouvé de réponse à ces questions, nous sommes ici.",
    latitude: 41.9401214167443,
    longitude: 8.8403180698947,
    date_declaration: "2023-09-03",
  },
  {
    id: "guilhem-eglantine",
    noms: "Guilhem de Lépinay et Églantine Berthet",
    enfants: "Lou et Nao",
    departement: "Haut-Rhin",
    presentation:
      "L'instruction en famille a été naturelle pour notre famille, une suite logique à la liberté de mettre ou non nos enfants à la crèche. Notre cadre de vie, avec la nature omniprésente et un foisonnement d'activités différentes auxquelles les enfants peuvent participer avec de nombreux autres adultes et enfants, leur permet de se développer librement en respectant leurs envies et besoins.\n" +
      "Notre mode de vie saisonnier nous autorise de plus de voyager en France ou ailleurs pendant l'hiver pour découvrir d'autres façon d'habiter, de faire ou de penser.\n" +
      "\n" +
      "L'année dernière, nous avons obtenu après recours l'autorisation pour l'IEF de Lou pour la petite section de maternelle.\n" +
      "Cette année, malgré un contrôle pédagogique de notre fille jugé très satisfaisant, nous subissons un refus et notre recours est ensuite écarté sans une ligne de justification supplémentaire.\n" +
      "\n" +
      "Notre situation n'est pas un cas isolé : de nombreuses familles subissent des refus arbitraires avec des justifications pré-formatées et impersonnelles.\n" +
      "\n" +
      "Nous voulions rejoindre l'association Enfance Libre dès sa création en réaction à cette loi liberticide : nous la rejoignons cette année, et espérons notamment plusieurs choses de ce mouvement :\n" +
      "qu'il fasse avancer la cause de toutes les personnes qui ont choisi comme nous la voie de l'instruction en famille ou bien pour celles et ceux qui au cours de leur scolarité auraient besoin d'une parenthèse,\n" +
      "qu'il fasse parler du sujet de l'instruction en famille largement méconnu en dehors de familles la pratiquant,\n" +
      "\n" +
      "Bravo à toutes les familles ayant eu la force de lancer ce mouvement auquel nous sommes fiers de participer aujourd'hui.",
    latitude: 47.45784692938239,
    longitude: 7.235140516853269,
    date_declaration: "2023-08-28",
  },
  {
    id: "florence-pierre-alexandre",
    noms: "Florence Pelletier et Pierre-Alexandre Conte",
    enfants: "Camille",
    departement: "Yvelines",
    presentation:
      "Pour nous, l’instruction en famille est un choix mûrement réfléchi, fruit d'interrogations et de découvertes ayant eu lieu il y a plusieurs années.\n" +
      "\n" +
      "Depuis la naissance de notre fils, nous faisons en sorte de respecter ses besoins et ses envies. Il serait pour nous aberrant de stopper net cette obligation que nous avons envers lui en le scolarisant, sans prendre en compte la personne qu’il est, et la personne qu’il pourra également devenir.\n" +
      "\n" +
      "Nous sommes contre cette loi liberticide forçant les parents à demander l’autorisation pour instruire leurs propres enfants et entrons en résistance, pour le retour d’un régime déclaratif plus respectueux de nos droits.",
    latitude: 48.94103210098159,
    longitude: 2.158018788557546,
    date_declaration: "2023-09-05",
    date_fin_resistance: "2024-10-16",
  },
  {
    id: "emeline-sebastien",
    noms: "Emeline Nigay et Sébastien Arod",
    enfants: "Raphaël, Léa et Adam",
    departement: "Rhône",
    presentation:
      "En tant que parents, nous avons comme but premier l’intérêt supérieur de nos enfants.\n" +
      "Depuis qu’ils sont nés nous nous questionnons sur les choix que nous faisons avec cet objectif\n" +
      "en tête. A ce jour le choix de l’instruction en famille est ce qui nous semble le plus cohérent, c’est pour\n" +
      "nous la meilleure façon de respecter le rythme d’apprentissage et les besoins de nos enfants.\n" +
      "Nous les voyons apprendre, grandir et s’épanouir au quotidien, à leur rythme, au sein de notre\n" +
      "famille et à travers le monde et pour l’instant ils ne manifestent pas le souhait d’aller à l’école.\n" +
      "Nous ne les scolariserons jamais contre leur volonté.\n" +
      "Nous entrons en désobéissance civile afin de préserver leur liberté.",
    latitude: 45.82575205650618,
    longitude: 4.621868085074032,
    date_declaration: "2023-09-09",
  },
  {
    id: "olivia-pascoal",
    noms: "Olivia Abadia et Pascoal Francisco Pinto Domingos",
    enfants: "Thiago, Liam et Ama",
    departement: "Landes",
    presentation:
      "A ce jour nos enfants font leurs apprentissages à leurs rythmes, dans le respect de leurs besoins physiologique, social, émotionnel et psychique. De plus Liam a subi des violences physiques lors de sa scolarité par le corps enseignant. Thiago lui a été témoin de maltraitance physique sur des enfants de sa classe et a subi des violences psychologiques. Souvenirs amers pour mes garçons qui ne souhaitent plus vivre ses méthodes coercitives.\n" +
      "\n" +
      "De par leurs épanouissements dans ce mode d'apprentissage, de vie, de ces découvertes et des rencontres diverses et variées, ils expriment clairement le refus d'être re/scolarisé. \n" +
      "Leurs besoins et leur bien-être étant notre priorité absolue, heureux nous-mêmes de ce mode de vie enrichissant et en accord avec nos convictions, c'est tout naturellement que notre petite Ama reste également  auprès de nous. \n" +
      "\n" +
      "Nous refusons fermement de demander une autorisation à quiconque pour prendre en compte les besoins de nos enfants et faire notre devoir en tant que parents. La loi sur le séparatisme article 49 est une entrave à nos droits et aux leurs.\n" +
      "\n" +
      "Nous ne céderons pas. Le pouvoir n'a de légitimité que celle que le peuple lui octroie.\n",
    latitude: 43.551535917454025,
    longitude: -1.151856801583541,
    date_declaration: "2023-09-07",
    date_fin_resistance: "2024-06-03",
  },
  {
    id: "marina-romain",
    noms: "Marina Barreira et Romain Coffinet",
    enfants: "Manael et Nathéan",
    departement: "Seine-Saint-Denis",
    presentation:
      "Après trois années de scolarisation de notre ainé, il nous a fait part de son désir de recommencer l’instruction en famille. Nous avons donc décidé de lui offrir cette pause dont il semblait avoir tant besoin, pensant partir pour une année hors de temps et revenir ensuite à la normale. Nous avons donc revu nos plans et réorganisé tout notre quotidien afin que lui et son frère puissent bénéficier de cette halte pour mieux redémarrer.\n" +
      "Nous voilà trois ans plus tard, toujours en IEF, et aujourd’hui en lutte pour préserver ce mode de vie dans lequel nous avons trouvé notre équilibre.\n" +
      "Après de multiples tentatives de dialogue et deux demandes refusées pour notre cadet, nous avons décidé d’arrêter de nous soumettre et d’affirmer haut et fort que nous porterons et défendrons la parole de nos enfants partout où elle est ignorée et méprisée !\n" +
      "Nous continuerons, pour nos enfants, à nous élever contre l’article 49 de la loi séparatisme ainsi que contre l’arbitraire administratif et l’iniquité devant la loi qui en découle, ainsi que contre la scolarisation à tout prix qui ne respecte en rien l’intérêt supérieur de l’enfant.\n" +
      "Un jour ils seront fiers du chemin que nous avons accompli pour eux et pour tous les autres enfants de ce pays.",
    latitude: 48.88622890832501,
    longitude: 2.4334594164313836,
    date_declaration: "2023-09-13",
  },
  {
    id: "virginie-eric",
    noms: "Virginie Duclaux et Eric Mercier",
    enfants: "Maïlys, Alban, Maxence et Lucie ",
    departement: "Isère",
    presentation:
      "Nous sommes Virginie Duclaux et Eric Mercier, parents de quatre enfants, dont la dernière est instruite en famille. Parce que nous faisons le choix de suivre nos convictions éducatives mûrement pesées, parce qu’il nous est impensable de renoncer à lui offrir ce que nous croyons être le meilleur pour elle. Parce que nous souhaitons lui offrir le temps de grandir émotionnellement, intellectuellement, dans sa confiance en elle et son rapport aux autres.\n" +
      "\n" +
      "Nous choisissons de désobéir à la loi, qui n’offre pas, dans les faits, les garanties que les parlementaires ont cru défendre. Une loi qui laisse un pouvoir arbitraire à l'administration déshumanisée, kafkaïenne. Sans succès, nous sommes allés au bout de tous les recours gratuits possibles. Personne ne nous a entendus, nous, parents, réclamer à corps et à cris le droit d’offrir notre temps, notre amour à notre enfant. Alors nous reprenons notre liberté, et réclamons le retour au régime déclaratif. \n" +
      "\n" +
      "La diversité des pratiques sert la richesse et la liberté, la démocratie. Nos enfants seront des citoyens éclairés qui connaissent l’importance de choisir sa vie, la valeur du mot liberté.",
    latitude: 45.11104148774449,
    longitude: 5.729435845925877,
    date_declaration: "2023-09-21",
  },
  {
    id: "laure-labadie",
    noms: "Laure Labadie",
    enfants: "Eloa",
    departement: "Lot",
    presentation:
      "Depuis ma grossesse, je me suis passionnée pour l’Humain, ses besoins fondamentaux, ses émotions et l’impact de l’éducation sur son développement, son rapport futur aux autres et à lui-même. J’ai pris conscience de l’importance de mes choix concernant ma fille, jusque dans une dimension politique ; les rapports de domination-soumission étant, je le crois, le terreau de la violence et de la guerre. \n" +
      "Aussi, j’ai tenté de respecter au mieux le rythme d’évolution de ma fille et il m’a semblé naturel, à « l’âge scolaire », de continuer sur la même lancée et j’ai choisi les apprentissages libres et autonomes pour Eloa, me plaçant non pas au-dessus d’elle mais à côté.\n" +
      "Ayant moi-même été scolarisée et pour accompagner au mieux ma fille, j’en suis passée par ma propre « déscolarisation », c’est-à-dire réévaluer mes attentes et mes peurs et travailler ma confiance et mon lâcher-prise.\n" +
      "Ce fût une superbe aventure et je me sens privilégiée d’avoir pu partager l’enfance de ma fille ainsi que d’avoir pu observer et nourrir son évolution, sans pression.\n" +
      "\n" +
      "Les inspections avec l’Education Nationale n’ont pas toujours été simples car nous ne parlions pas le même langage..\n" +
      "Aujourd’hui, après 2 contrôles défavorables l’année dernière, l’administration veut forcer la scolarisation d’Eloa pour quelques mois seulement (elle aura 16 ans mi-février 2024). J’ai été naïve de croire qu’une scolarisation forcée pour quelques mois était impensable et qu’ils souhaitaient seulement nous suivre jusqu’au bout; aussi, je n’ai pas contesté le 1er contrôle défavorable, ce que je regrette amèrement. De plus, les inspecteurs ont agi avec stratégie lors du 2nd contrôle en ne me contre-disant pas et en me donnant des conseils, me faisant croire à un avenir.\n" +
      "La mise en demeure de scolarisation, décision qui bouleverse la vie qu’Eloa a toujours connue, a donc été un choc pour nous deux. Je refuse ce genre de procédé.\n" +
      "Eloa exprime clairement son refus d’être scolarisée et j’ai décidé de respecter son consentement. \n" +
      "\n" +
      "Pour tous les enfants présents et futurs, je suis triste de voir ce droit disparaître et profondément choquée des stratégies employées pour imposer une norme. Face aux enjeux de notre époque, il me semble que, contrairement au conformisme, la diversité et la créativité sont une richesse.\n",
    latitude: 44.28297675215938,
    longitude: 1.4296558199274951,
    date_declaration: "2023-10-05",
    date_fin_resistance: "2024-06-08",
  },
  {
    id: "valene-emilien",
    noms: "Valène Zahzouh et Emilien Hurpy",
    enfants: "Louis, Eden et Valentin",
    departement: "Eure",
    presentation:
      "Nous sommes les parents de Louis (12 ans) scolarisé, Eden (3 ans) en IEF depuis septembre et Valentin (1 an et demi). \n" +
      "\n" +
      "Emilien, mon conjoint et moi-même mettons tout en oeuvre dans nos sphères privée et professionnelle afin d’être en mesure d’être présents quotidiennement pour nos enfants.\n" +
      "\n" +
      "Notre demande d’instruction en famille reposait essentiellement sur notre profond désir de respecter le rythme d’apprentissage et la physiologie de nos enfants ainsi que sur l’évidence qu’en tant que parents nous sommes les plus à même d’accueillir et accompagner leur évolution, leur unicité.\n" +
      "Scolariser Eden sans que cela paraisse être son souhait, de manière forcée et sans raison valable autre qu’une obligation arbitraire qui ne prend aucunement en compte les besoins spécifiques de chaque enfant va à l’encontre de notre équilibre familial et n’est pas en adéquation avec nos valeurs profondes.\n" +
      "C’est pourquoi nous avons fait le choix conscient de refuser de nous plier à cette loi injuste qui oblige à demander l’autorisation d’instruire, ne serait-ce que parce qu’il s’agit là d’un acte fort quoique pacifique de revendiquer une liberté fondamentale.\n" +
      "Donner cet exemple à nos enfants apparait également comme une forme d’instruction, car défendre ce qui nous semble juste est aussi une façon de s’encrer solidement en tant qu’être humain et citoyen de notre monde.\n",
    latitude: 48.96967,
    longitude: 1.44133,
    date_declaration: "2023-10-11",
  },
  {
    id: "stephanie-david",
    noms: "Stéphanie Filippa et David Collado",
    enfants: "Mélusine et Emrys",
    departement: "Haute-Garonne",
    presentation:
      "Depuis la naissance de notre ainée Mélusine, nous nous émerveillons de voir comment l'enfant est capable de s'épanouir, apprendre et grandir dans un environnement suffisamment riche en expériences. Il nous est apparu clairement que l'IEF était ce qui apporterait le plus d'équilibre à tous les membres du foyer.\n" +
      "La loi actuelle sur l'IEF ne permet plus aux familles de tester ce mode d’apprentissage, et met en danger les enfants qui ne peuvent s'épanouir dans le système scolaire classique. Par conséquent, nous refusons de nous y soumettre et entrons en désobéissance civile.",
    latitude: 43.399701272331676,
    longitude: 1.2997455040893293,
    date_declaration: "2023-10-12",
  },
  {
    id: "julia-dawy",
    noms: "Julia Hamon et Dawy Moy",
    enfants: "Kenaël et Alyx",
    departement: "Haute-Vienne",
    presentation:
      "Notre famille est installée en Haute-Vienne, dans la campagne Limousine, où nous vivons avec nos 2 fils: Kenaël 11 ans et Alyx 3 ans.\n" +
      "\n" +
      "Nous sommes entrés en désobéissance civile car le gouvernement ne cesse de nous priver de nos libertés fondamentales et que lorsque la question de l'IEF s'est posé pour nos enfants l'an dernier, nous avons constaté les dégâts occasionnés par cette nouvelle demande d'autorisation discriminatoire et arbitraire.\n" +
      "\n" +
      "Nous souhaitons élever nos enfants avec des valeurs et des principes moraux qui ne correspondent plus avec le système éducatif national actuel.\n" +
      "\n" +
      "De fait, plutôt que de gaspiller notre temps et notre énergie dans des démarches laborieuses, stressantes et aléatoires d'une région à l'autre, nous nous sommes engagés aux côté du collectif Enfance Libre afin de guider au mieux nos enfants vers un apprentissage heureux, autonome et épanouissant !",
    latitude: 45.85643484201413,
    longitude: 1.4380234409080361,
    date_declaration: "2023-09-10",
    date_fin_resistance: "2024-05-31",
  },
  {
    id: "anne-lise-marc",
    noms: "Anne-Lise Clipet et Marc Lafontaine",
    enfants: "June",
    departement: "Oise",
    presentation:
      "Nous sommes Anne-Lise et Marc, heureux parents de June née en 2019. Epanouie, pleine de vie et d'un naturel curieux, notre fille a grandi au sein d'un cocon familial rassurant, respectueux de ses besoins et de sa sensibilité. Après une tentative infructueuse de scolarisation, l'effet miracle qu'a eu l'IEF sur notre fille nous a poussé à nous battre pour défendre ce droit dans l'intérêt de tous les enfants qui le souhaitent et pour qui les contraintes et les trop nombreuses règles scolaires ne conviennent pas et ruinent leur motivation et leur confiance en eux.",
    latitude: 49.4000136509991,
    longitude: 1.8759151599933288,
    date_declaration: "2023-10-20",
  },
  {
    id: "virginie-yves",
    noms: "Virginie Anscutter-Loriot et Yves Loriot",
    enfants: "Noah",
    departement: "Seine-Maritime",
    presentation:
      "Nous sommes Virginie et Yves Anscutter-Loriot, parents d’un petit Noah. Depuis sa naissance, nous l’accompagnons dans ses apprentissages qu’ils expérimentent à son rythme et c’est donc naturellement que nous avons envisagé de l’instruire en famille. D’autant qu’il ne souhaite pas allé à l’école bien qu’ayant des amis scolarisés.\n" +
      "Nous avons été abasourdis lorsque le texte de loi est passé mais naïvement nous avons cru que l’on pourrait obtenir une autorisation en prenant un grand soin dans la préparation de notre dossier. Mais face à un premier refus, au recours qui a suivi, puis encore un refus et puis notre recours au Tribunal avec encore un refus pour le référé suspension, nous avons d’ailleurs peu d’espoir pour le jugement au fond qui n’est pas encore terminé à ce jour. Depuis mars 2023, on vit dans le stress et l’attente des décisions ; qui pourrait supporter ça chaque année ?\n" +
      "C’est pourquoi, au lieu de subir continuellement, nous avons décidé de rassembler notre courage et de dire non à cette loi qui restreint de manière inconsidérée nos libertés.",
    latitude: 49.46907672817366,
    longitude: 1.1145488373053412,
    date_declaration: "2023-10-03",
    date_fin_resistance: "2024-05-27",
  },
  {
    id: "jenny-thierry",
    noms: "Jenny Lejeune et Thierry Brandt",
    enfants: "Lou",
    departement: "Doubs",
    presentation:
      "De quel droit des inconnus peuvent juger ce qui est mieux pour lui ? Qui peut affirmer que sa famille ne fait pas partie de sa situation propre ? Qui peut briser son équilibre émotionnel impunément et en Présentation de la famille toute conscience ? Nous, ses parents, connaissons Lou et savons que pour son intérêt supérieur l’instruction hors école lui convient parfaitement. \n" +
      "\n" +
      "Aujourd’hui nous sommes en **lutte** pour préserver notre équilibre. Nous sommes en lutte pour préserver l’équilibre des milliers de familles, des enfants, de **nos** enfants et de Lou. Nous sommes en lutte pour notre **liberté de choisir** !",
    latitude: 47.14832918192116,
    longitude: 6.342331883356072,
    date_declaration: "2023-10-17",
    date_fin_resistance: "2024-03-20",
  },
  {
    id: "perrina-benjamin",
    noms: "Pérrina de Haas et Benjamin Autissier",
    enfants: "Lioumizia",
    departement: "Isère",
    presentation:
      "L'instruction en famille s'est affirmée à nos consciences comme le font les évidences, avec la force tranquille de la simplicité. Elle s'est ancrée au plus profond de nos convictions, et ce pour de multiples raisons qui toutes convergent vers l'intérêt supérieur de notre enfant. Bien-être, épanouissement et réalisation de soi constituant la trame de notre cheminement. L'instruction en famille représente tellement plus qu'un simple mode d'instruction, c'est en soi un projet de vie global qui implique un engagement total. Le plus important restant, avant toute autre considération, l'écoute et le respect de la voix de l'enfance.\n" +
      "\n" +
      "Le simple fait que les hommes aient pris soin d'inscrire la liberté d'instruction dans la déclaration universelle des droits de l'homme vient témoigner de son caractère fondamental et asseoir de fait sa légitimité.\n" +
      "\n" +
      "Les enfants ne sont rien de moins que notre avenir à tous. Le temps de l'enfance, inestimable trésor tout aussi fragile qu'irremplaçable, est un écrin où gravitent tous les possibles. C'est le socle sur lequel toute vie s'érige.\n" +
      "\n" +
      "Enfance et Liberté participent du précieux de l'humanité et il serait aujourd'hui interdit de les conjuguer ?\n" +
      "\n" +
      "Nous sommes parents et lorsque nous avons pris la décision d'inviter un nouvel être à la vie, nous avons pris un engagement irrévocable. Nous sommes résolus et nous déploierons toutes nos énergies, nous offrirons à notre fille ce que nous considérons être le meilleur pour elle. Et nous choisissons cet instant d'éternité qu'est une enfance libre, pleine et sereine : que sa découverte du monde et son rapport à la société se fondent sur l'expérience de la liberté, de l'égalité et de la confiance.\n" +
      "\n" +
      "Pouvons-nous prétendre être libres lorsque nous devons demander l'autorisation ? Pouvons-nous prétendre être libres alors que nous sommes soumis à l'arbitraire ? C'est le profond respect que nous portons aux valeurs de la république qui nous engage sur la voie de la désobéissance civile.\n" +
      "\n" +
      '"Ils ont essayé de nous enterrer,\n' +
      "\n" +
      "ce qu'ils ne savaient pas, c'est que nous étions des graines.\"\n" +
      "\n" +
      "(Proverbe mexicain)",
    latitude: 45.404758343943946,
    longitude: 5.017109781114582,
    date_declaration: "2023-09-03",
  },
  {
    id: "sebastien-marie",
    noms: "Sébastien Dufour et Marie Casteran",
    enfants: "Charlie, Tao et Adam",
    departement: "Dordogne",
    presentation:
      "Nous entrons en désobéissance civile pour défendre notre liberté d'instruire nos enfants, en France et dans le monde; et de vivre comme bon nous semble.\n" +
      "\n" +
      "Dufour Sébastien (père), Casteran Marie (mère), Dufour-Espeisse Charlie (19 ans) , Dufour Tao (7 ans) , Dufour Adam (3 ans).\n" +
      "\n" +
      'Charlie, est depuis cette année étudiante en BTS Tourisme à Montpellier, Tao en "IEF de plein droit" depuis janvier 2022, Adam en "IEF illégal" depuis septembre 2023.\n' +
      "\n" +
      "Nous sommes petits commerçants saisonniers ce qui nous permet d'être disponibles pour nos enfants, choisir et se répartir notre temps de travail professionnel.\n" +
      "\n" +
      "Nous sommes très soutenus par nos familles et [ami.es](https://ami.es/) qui se joignent à nous pour l'éducation et l'épanouissement de nos enfants (grands parents, parrains, marraines , oncles, tantes...)",
    latitude: 44.88766448630741,
    longitude: 1.1639922095283575,
    date_declaration: "2023-09-15",
    date_fin_resistance: "2024-09-01",
  },
  {
    id: "helene-volodia",
    noms: "Hélène Simonetta et Volodia Heidelberger",
    enfants: "Pandore",
    departement: "Moselle",
    presentation:
      "Depuis qu'elle est entrée dans l'âge d'instruction obligatoire, nous avons toujours fait en sorte de pouvoir offrir le choix à notre enfant sur sa scolarité. À l'école lors du passage de cette loi, son choix s'est à nouveau porté sur l'instruction en famille suite à une année compliquée. Mais comment respecter son choix avec cette nouvelle loi ? Nous avons tenté la demande d'autorisation pour motif 4 et non pour motif 1 parce que déjà il n'était pas question de juste obtenir l'autorisation pour être tranquille, mais de faire valoir nos droits ; la situation propre de notre fille et ses besoins ont alors été nié par une administration qui n'a jamais eu le moindre contact avec elle.\n" +
      "\n" +
      "Il n'était pas envisageable, pour nous, titulaire de l'autorité parentale et donc premiers garants de l'intérêt supérieur de notre enfant, de l'inscrire dans une école à cette rentrée scolaire, contre sa volonté et surtout à l'encontre de son bien être.\n" +
      "\n" +
      "Nous choisissons donc la voie de la désobéissance civile, non pour quémander une autorisation, mais pour réclamer le retour à un régime déclaratif de l'instruction en famille pour toutes les familles !",
    latitude: 49.25107765471377,
    longitude: 6.040557777896552,
    date_declaration: "2023-09-08",
  },
  {
    id: "audrey-jeremie",
    noms: "Audrey Depienne et Jérémie Donnard",
    enfants: "Caleb",
    departement: "Finistère",
    presentation:
      "L'IEF représente pour nous un véritable choix et projet de vie et d'engagement et investissement parental.\n" +
      "\n" +
      "Ce choix nous permet de passer du temps de qualité avec notre enfant.\n" +
      "\n" +
      "Il nous permet aussi d'être acteurs de l'éducation et de l’instruction de notre fils, afin de lui transmettre les valeurs morales et familiales qui nous tiennent à cœur et qui, à nos yeux, lui permettront de s'épanouir pleinement:\n" +
      "\n" +
      "- l'amour, l'écoute, l'empathie, la bienveillance, l'altruisme\n" +
      "- le respect de soi, des autres et de notre environnement,\n" +
      "- la tolérance, la solidarité, la fraternité,\n" +
      "- l'égalité, le partage,\n" +
      "- la coopération, la confiance en soi et en l'autre,\n" +
      "- la liberté, l'autonomie,\n" +
      "- l'honnêteté, la loyauté, …\n" +
      "\n" +
      "Le respect du rythme biologique et des différents besoins de notre fils est également une priorité pour nous.\n" +
      "\n" +
      "Il est, à nos yeux, indispensable à son bon équilibre, à son bon développement (psychomoteur, socio-affectif, neurologique, …) et à sa bonne santé de manière générale.\n" +
      "\n" +
      "Depuis sa naissance, nous avons à cœur de respecter son  rythme de vie.\n" +
      "\n" +
      "Aussi bien au niveau du sommeil, que de l'alimentation, de l'activité physique et des apprentissages.\n" +
      "\n" +
      "Cela nécessite une attention et une observation particulière, une communication régulière, mais aussi de la patience et une grande capacité d'adaptation, car ce rythme évolue perpétuellement.\n" +
      "\n" +
      "Cela demande donc du temps, c’est également pour cela que nous avons choisi l'IEF.\n" +
      "\n" +
      "Passer du temps avec notre fils, répondre à ses besoins et le voir heureux et épanoui est donc ce qu'il y a de plus précieux pour nous.\n" +
      "\n" +
      "C'est pourquoi, nous avons fait le choix de militer et résister avec toutes les familles qui ont rejoint Enfance Libre.\n" +
      "\n" +
      "Nous pensons également à toutes les familles et enfants qui aimeraient faire ce choix à l'avenir et espérons que ce combat leur servira.\n" +
      "\n" +
      "Enfin, nous menons aussi ce combat afin que nos droits fondamentaux soient respectées ainsi que les grands principes de notre pays: la liberté, la fraternité et l'égalité.",
    latitude: 48.43419248888258,
    longitude: -4.73387418270936,
    date_declaration: "2023-10-16",
  },
  {
    id: "anne-laure-damien",
    noms: "Anne-Laure et Damien de Beauregard",
    enfants: "Enaelle et Lénaïc",
    departement: "Bouches-du-Rhône",
    presentation:
      "L'instruction en famille étant un droit constitué par de nombreux textes fondateurs \n" +
      "ratifiés par la France comme notamment la Déclaration des Droits de \n" +
      "l'Homme ou la Convention Européenne des Droits de l'Homme, nous refusons\n" +
      " d'accepter d'être privés de ces droits humains inaliénables en tant que\n" +
      " premiers et principaux éducateurs de nos enfants.\n" +
      "\n" +
      "Entrer en désobéissance civile est à nos yeux le moyen le plus évident dans \n" +
      "cette situation ubuesque afin de retrouver notre liberté de choix pour \n" +
      "l'instruction de nos enfants.\n" +
      "\n" +
      "Petite part de colibri, notre engagement dans cette démarche a comme objectif \n" +
      "de maintenir richesse et diversité d'instruction dans notre société.",
    latitude: 43.59909418616063,
    longitude: 5.481102155258618,
    date_declaration: "2023-10-28",
  },
  {
    id: "mathilde-thomas",
    noms: "Mathilde et Thomas Rodriguez",
    enfants: "Mahée, Nahema, Miha",
    departement: "Ariège",
    presentation:
      "Nous sommes Mathilde et Thomas RODRIGUEZ, parents de trois petites filles de 7ans, 5ans et 3ans. Nos enfants ont toujours été instruites en famille.\n" +
      "\n" +
      "Nous refusons de demander une autorisation chaque année pour exercer notre liberté d’instruire nos enfants par nous-même. Ainsi, nous sommes entrés en désobéissance civile en septembre 2023.\n" +
      "\n" +
      "Nous nous battons pour un retour au régime déclaratif. La loi telle qu’elle est actuellement, ne respecte ni l’intérêt supérieur de l’enfant ni le bien-être des familles.",
    latitude: 43.1292305443638,
    longitude: 1.0616247131313628,
    date_declaration: "2023-10-02",
  },
  {
    id: "marion-quentin",
    noms: "Marion et Quentin de Menis",
    enfants: "Nino",
    departement: "Creuse",
    presentation:
      "Nous sommes Marion et Quentin, heureux parents de Nino né le 28 janvier 2019. Un petit garçon plein de surprises, qui ne commence pas une journée sans son lot de chatouilles et de rires ! Son appel à la vie nous réjouit tous les jours !\n" +
      "\n" +
      "Être parent, ce n'est pas un manuel livré à la naissance d'un enfant et qu'il faut suivre à la lettre... c'est du rire, de la joie, de la magie et de la spontanéité, des émotions en dents de scie, etc etc... mais c'est aussi un merveilleux moment de remise en question autour de notre bagage éducatif, socioculturel et professionnel...\n" +
      "\n" +
      "Sur ces derniers points, Nino, enfant d’aujourd’hui, a grandement contribué aux parents de demain que nous souhaitons être. Devenir des êtres de conscience et responsables dans ce monde pour être acteur d'un espoir de vie et d'amour. C'est pourquoi Quentin, ingénieur dans le secteur de l'énergie pendant 12 ans a souhaité démissionner et se tourner vers des activités associatives alliant éthique humaine et écologie; et Marion, professeur des écoles après reconversion, s'est mise en disponibilité et reste en questionnement quant à sa reprise.\n" +
      "\n" +
      "Nous vivons aujourd'hui dans un petit hameau de Creuse où nous y avons trouvé les valeurs humaines et écologiques qui nous ressemblent et que nous souhaitons proposer à notre fils.\n" +
      "\n" +
      "Un contexte humain, philosophiquement résolu dans une démarche de décroissance matérielle et de sobriété qui souligne l’essentiel des moments vécus avec leur plus belle intensité.\n" +
      "\n" +
      "L’école républicaine actuelle, par les choix politiques successifs, les moyens attribués, les exigences attendues envers les enfants, ne véhicule pas les valeurs qui nous semblent primordiales pour leur avenir.\n" +
      "\n" +
      "C'est pourquoi nous nous engageons dans cette voie de désobéissance civile auprès d'Enfance Libre, en espérant faire changer la loi pour le retour au régime déclaratif et faire respecter la liberté d'instruire nos enfants selon le choix de chacun.",
    latitude: 46.042966056051604,
    longitude: 2.1551813803029565,
    date_declaration: "2023-11-15",
    date_fin_resistance: "2024-09-02",
  },
  {
    id: "rachel-bastien",
    noms: "Rachel Bernard et Bastien Trévesaïgues",
    enfants: "Léandre et Colette",
    departement: "Tarn",
    presentation:
      "Nos deux enfants étaient scolarisés l’année dernière, et ont manifesté le souhait de quitter cet espace qu’ils considéraient commepasser en IEF trop coercitif. Nous avons décidé de tenir compte de leurs besoins et de favoriser autant que possible leur épanouissement, et avons donc pris la décision de passer en IEF. Nous ne reconnaissons pas la légitimité de l’article 49 de la loi CRPR, et avons donc décidé d’entrer en désobéissance civile aux côtés d’Enfance Libre. Pour nos enfants et pour tous les autres.",
    latitude: 43.944348475000076,
    longitude: 2.482735675122331,
    date_declaration: "2023-10-06",
    date_fin_resistance: "2024-09-02",
  },
  {
    id: "angelique-jonathan",
    noms: "Angélique Chéron et Jonathan Laurent",
    enfants: "Elliott",
    departement: "Aveyron",
    presentation:
      "Nous sommes Jonathan et Angélique tous les deux parents de Eliott qui a 4 ans et demi et d'un petit bébé qui s'en vient.\n" +
      "\n" +
      "Nous sommes paysans, chevriers, fromagers sur le plateau du Larzac dans une ferme SCTL (Société Civile des Terres du Larzac). Cette ferme fait partie des fermes qui ont survécu au projet\n" +
      "\n" +
      "d'extension du camp militaire dans les années 70. Sans la résistance et la désobéissance de centaines de personnes, non seulement nous ne serions pas paysans sur ce territoire mais c'est tout le visage, la biodiversité et la richesse des alentours Millavois qui auraient disparu engloutis par 14000 hectare d’extension de camp militaire.\n" +
      "\n" +
      "Aujourd'hui, c'est à nous de résister face à une loi dénuée de bon sens, qui met en péril la richesse éducative de notre pays.\n" +
      "\n" +
      "Nous ne pouvons accepter un régime d’uniformité quant à la manière de penser, d’apprendre et d’appréhender le monde. C’est notre devoir à nous TOUS, citoyen-nes, ayant droit à la parole, de permettre à nos enfants de pouvoir penser, apprendre, appréhender le monde de divers manières et surtout de la manière dont il le souhaite.Nous voulons permettre à notre petit garçon, à nos enfants de pouvoir choisir, mais aussi préserver la richesse et la biodiversité du paysage éducative de la France.",
    latitude: 44.100497492601704,
    longitude: 3.078230498121882,
    date_declaration: "2023-09-25",
  },
  {
    id: "celine-fabrice",
    noms: "Céline Guillemot et Fabrice Martinez",
    enfants: "Ange, Célestin et Azur",
    departement: "Drôme",
    presentation:
      "Nous sommes Fabrice et Céline, parents de 3 enfants, Ange (13 ans), Célestin (9 ans) et Azur (2,5 ans). Nous instruisons nos enfants en famille depuis 2016. Nous avons fait ce choix pour leur permettre de vivre à leur rythme. Nous avons rempli nos engagements en acceptant tous les contrôles de l'inspection et les visites de la mairie. Lorsque la loi a changé vers le régime d'autorisation, nous n'avons pas rempli la demande en 2022. En effet, nous n'envisageons pas que l'Etat ait une telle emprise sur notre vie familiale et ne comprenons pas de quel droit il pourrait choisir à notre place ce qui est bon pour nos enfants ? L'école n'étant pas le seul moyen d'accéder à l'instruction, nous voulons que nos enfants, s'ils le souhaitent, puissent y aller mais en aucun cas sous la contrainte de l'Etat. \n\n" +
      "Après avoir été dénoncés par la mairie de notre village puis un temps de questionnement profond de nos peurs, nos doutes, nos freins, nos\n\n" +
      "ressources, nous avons finalement franchi le pas vers la désobéissance civile.\n\n" +
      "Soyons « plusieurs à être nombreux » à défendre la liberté de choisir !",
    latitude: 44.69474401100994,
    longitude: 5.490208086011624,
    date_declaration: "2024-01-09",
  },
  {
    id: "sultane-mehmet",
    noms: "Sultane et Mehmet Salim",
    enfants: "Ali, Nil et Imran",
    departement: "Seine-Saint-Denis",
    presentation:
      "Nous sommes Sultane et Mehmet SALIM, parents de 3 enfants . Les 2 ainés 13 ans et 6 ans, ont toujours été en IEF et ont bénéficié d'un plein droit de 2 ans (à l'arrivée de la loi confortant les principes de la République).\n" +
      "Pour notre petit dernier, il a fallu faire une demande d'autorisation et nous avons naïvement cru que, parce que nous avions toujours été en IEF, nous aurons cette autorisation. Nous nous sommes vite rendu compte que nous avons été trompés et que c'était la fin de l' IEF... et n'avons pas demandé d'autorisation pour l'année en cours.\n" +
      "\n" +
      "Aujourd'hui, nous nous battons pour un retour au régime déclaratif. La loi telle qu'elle est actuellement, ne respecte ni l'intérêt supérieur de l'enfant ni le bien-être familial. C'est une loi liberticide, et nous n'accepterons jamais de demander une autorisation à une institution pour élever nos enfants. L'institution “Education Nationale” n'est pas légitime à donner ou refuser une autorisation à des parents pour s'occuper de leurs enfants.\"",
    latitude: 48.87529844766604,
    longitude: 2.4845335485767626,
    date_declaration: "2023-12-26",
  },
  {
    id: "marie-matthieu",
    noms: "Marie et Matthieu Jaskulski-Vion",
    enfants: "Gaspard",
    departement: "Eure",
    presentation:
      "Nous sommes Marie et Matthieu, notre fils Gaspard a 4 ans, et après une première année d'instruction en famille autorisée par le nouveau régime assorti d'un contrôle positif et élogieux, nous nous sommes heurtés à un refus, contre toute attente, dû au revirement de la politique de notre académie.\n" +
      "\n" +
      "Nous nous sommes évertués à faire entendre et comprendre notre situation et avons défendu nos droits au tribunal, sans y trouver la justice en laquelle nous croyions.\n" +
      "\n" +
      "Nous avons donc décidé d'entrer en désobéissance civile afin de maintenir l'espace épanouissant que nous avons construit en famille.\n" +
      "\n" +
      "Notre désobéissance est garante de nos promesses parentales, mais elle est aussi militante. \n" +
      "\n" +
      "Nous maintenons notre engagement de défense de la liberté d'instruction aux côtés d'Enfance Libres et du collectif régional auquel nous avons adhéré.",
    latitude: 49.15456878003762,
    longitude: 1.043745186340715,
    date_declaration: "2024-01-15",
    date_fin_resistance: "2024-03-07",
  },
  {
    id: "benedicte-yves",
    noms: "Bénédicte Charrin et Yves Pointcheval",
    enfants: "Titouan",
    departement: "Ille-et-Vilaine",
    presentation:
      "Yves et Bénédicte, parents de Titouan, 8 ans. Notre rapport au temps ainsi que celui à la nature sont teintés de notre vécu de marins. Cette expérience nous a aussi convaincu des bénéfices d’une vie simple, au rythme de la nature, protégée du stress et de la frénésie de consommation si répandus dans notre société. Nous avons aussi vu, en semi-autonomie, la diversité des connaissances et compétences qui nous sont accessibles, ce qui nous a ouvert le champ des possibles.\n" +
      "Côté éducation, nous avons été formés et avons travaillé avec des pédagogies nouvelles, qui nous ont convaincues. Lorsque Titouan a eu 3 ans, nous avons trouvé près de chez nous une école publique avec peu d’enfants et utilisant des pédagogies alternatives. Titouan s’y plaisait beaucoup, et nous en étions également très contents.\n" +
      "En septembre 2020, l’obligation du port du masque pour les enfants de 6 ans nous a paru inappropriée car, selon nous, cela ne peut pas permettre d’apprendre à lire et écrire dans de bonnes conditions, ni à développer ses compétences sociales. Nous avons alors découvert l’IEF. Cette expérience a dépassé toutes nos attentes. Nous nous sommes rendu compte du potentiel de ce type d’instruction : libéré des contraintes d’horaires et de temps, les possibilités en matière d’instruction nous sont apparues énormes. En premier lieu, ce mode de vie permet de respecter le rythme biologique de l’enfant. Ensuite, tout permet l’apprentissage : la lecture d’une recette, l’écriture au sujet d’un animal, les jeux de société (mots, chiffres, stratégie…), le bricolage, les sorties champignons / pêche... Le temps dégagé offre la possibilité de faire des activités sportives, de la musique, tout en ayant des temps de jeux avec les amis, des temps à soi… Partageant avec Titouan ce constat positif, nous avons continué. C’est maintenant pour nous trois un véritable choix de vie qui correspond pleinement à nos valeurs. C’est pourquoi, nous souhaitons continuer à l’offrir à Titouan.\n" +
      "Depuis la modification du régime déclaratif en régime soumis à autorisation, nous avons pu observer de nombreuses dérives et limites de ces nouvelles modalités, qui, sur la base d’arguments fallacieux, visent avant tout à interdire l’IEF. Fermement opposés à ces pratiques que nous considérons anti-démocratiques, nous décidons aujourd’hui, en septembre 2023 alors que nous disposons d’une autorisation de plein droit, de rentrer en désobéissance civile auprès du collectif Enfance Libre",
    latitude: 47.81372431001263,
    longitude: -2.0206334977707368,
    date_declaration: "2023-11-16",
  },
  {
    id: "elodie-mathieu",
    noms: "Élodie Berçon et Mathieu Moreau",
    enfants: "Azilys, Manoha et Cylian",
    departement: "Cher",
    presentation:
      "Alors qu'ils avaient 6 et 4 ans, en 2018 nous déscolarisons nos enfants parce que notre fille avait perdu son sourire, sa joie de vivre et son sommeil. Notre fils développait quant à lui de plus en plus un vocabulaire plus qu'inapproprié pour son âge. Il en aura fallu de la patience pour leur redonner cette envie naturelle d'apprendre, mais avec beaucoup d'amour tout est possible ! \n" +
      "\n" +
      "Aujourd'hui nos 3 enfants sont épanouis dans leur quotidien, leurs activités, nos voyages, riches de toutes ces rencontres et moments de partage. Il est inconcevable pour nous de les contraindre à faire ou aller à ce qu'ils ne souhaitent pas. \n" +
      "\n" +
      "C'est pourquoi nous respectons leur choix de continuer à vivre et apprendre ainsi. Cela sonne pour nous comme un devoir de faire respecter et entendre leur voix, et de désobéir à cette nouvelle loi que nous estimons liberticide, discriminatoire et injuste afin de revenir à un régime déclaratif pour ainsi protéger le droit pour tous les enfants et leur famille de choisir une instruction qui leur convient.",
    latitude: 46.54576202529733,
    longitude: 2.532583977286711,
    date_declaration: "2024-02-06",
  },
  {
    id: "caroline-gael",
    noms: "Caroline et Gaël Daufouy Bouchet",
    enfants: "Sélène",
    departement: "Loire-Atlantique",
    presentation:
      "A ces 3 ans, nous avons pu choisir pour notre enfant, car il y avait soit l’entrée à l’école, soit l’Instruction En Famille.\n" +
      "Par une simple déclaration, nous avons pu expérimenter cette façon de s’instruire, tous les trois, en famille, depuis 2018.\n" +
      "Nous y avons découvert une richesse dans les rencontres de tous âges, de nouvelles façons d’apprendre et un accompagnement au plus près des besoins de chaque membre de la famille.\n" +
      "Nous pouvions donc chaque année proposer à notre fille d’essayer l’école.\n" +
      "Ce n’est plus le cas aujourd’hui !\n" +
      "Notre fille si elle rentre à l’école ne pourra plus revenir en Instruction En Famille.\n" +
      "Et après avoir rempli, un lourd et long dossier, nous ne pouvons pas, non plus être sûrs, qu’elle puisse rester en Instruction En Famille.\n" +
      "Nous sommes donc désormais en désobéissance civile.\n" +
      "Nous souhaitons que chaque famille, chaque enfant qui en aurait besoin pour quelques années ou de nombreuses, puisse s’Instruire En Famille. \n" +
      "Elle ou il, par une simple déclaration, avec ses parents, use de sa liberté de choix afin de respecter son intérêt supérieur.",
    latitude: 47.2382327,
    longitude: -1.5603345,
    date_declaration: "2024-02-13",
  },
  {
    id: "isabelle-julio",
    noms: "Isabelle Cattan et Julio Hernando",
    enfants: "Izia et Stanislas",
    departement: "Côtes-d'Armor",
    presentation:
      "Nous sommes Isabelle et Julio, heureux parents de 2 magnifiques enfants, Izia (8 ans) et Stanislas (6 ans), les enfants s’instruisent en famille depuis toujours.\n" +
      "Les enfants sont libres, heureux et épanouis et souhaitent continuer l’IEF, nous respectons leur choix et leur envie.\n" +
      "Ce qui compte pour nous, qu’ils aient une vie d’enfant, qu’ils conservent leur curiosité et leur rythme naturel d’apprentissage.\n" +
      "L’IEF est ce qui apporte le plus d’équilibre à tous les membres du foyer.\n" +
      "La loi actuelle sur l’IEF ne permet plus aux familles d’aller vers ce choix, c’est pour cela que nous rentrons en désobéissance civile.",
    latitude: 48.437701,
    longitude: -3.4679109,
    date_declaration: "2024-01-30",
  },
  {
    id: "andrea-jerome",
    noms: "Andréa et Jérôme Gaudillère",
    enfants: "Kais, Léya et Soan",
    departement: "Jura",
    presentation:
      "L'aventure IEF à commencer pour notre famille (mon mari Jérôme, moi Andréa et nos 3 enfants Kaïs, Léya et Soan âgés de 10, 5 et 1 an), lors d'une période « imposée » à tous, nous avons trouvé ce qui correspondait aux besoins de notre fils et répondait au maximum à nos choix, nous le scolarisons à domicile dès octobre 2020.\n" +
      "Sa sœur n'est jamais allée dans une école, et notre petit dernier doit bénéficier de ce choix afin de rester à l'écoute de son rythme d'apprentissage, son rythme de vie. \n" +
      "L'ief fait parti intégrante de nos vies, demandé une autorisation équivaut à demander l'accord sur notre choix de vie qui repose sur le respect du rythme de l'enfant, c'est pourquoi nous rentrons en désobéissance civile aux côtés d'Enfance-Libre.",
    latitude: 46.4676404,
    longitude: 5.920744,
    date_declaration: "2024-02-20",
  },
  {
    id: "samuel-laurence",
    noms: "Samuel Giteau et Laurence Gourlay",
    enfants: "Daphné, Aude, Hanaé et Lyssandre",
    departement: "Deux-Sèvres",
    presentation:
      "Devenir parents a été un chamboulement dans notre vie.\n" +
      "Être responsable d'un petit être n'est pas une chose facile.\n" +
      "Grâce à la naissance de notre fille Daphné, puis Aude, ensuite Hanaé et enfin Lyssandre, cela nous a permis de découvrir l'éducation bienveillante.\n" +
      "La scolarisation en petite section de notre fille ainée s'est très mal passée. Le choix de l'IEF s'est donc imposé à nous très vite. Nous en avons vu les effets bénéfiques pour notre enfant et plus tard ses sœurs et son frère.\n" +
      "Pour nous, l'instruction ne s'arrête pas à l'enseignement des matières académiques, mais permet un accompagnement individualisé et bienveillant de chaque enfant.\n" +
      "Le choix de l'IEF nous a permis de progresser sur le plan personnel. En effet, elle nous a ouvert le champ des possibles dans la diversité des apprentissages, dans notre relation aux autres et au monde.\n" +
      "Nos enfants ont aujourd'hui entre 3 et 12 ans : ils sont curieux, enthousiastes, épanouis, ils écoutent leurs cœurs plutôt que leur peurs, ils aiment apprendre et ont confiance en eux. Toutes ces qualités nous apparaissent comme indispensables pour devenir des adultes responsables.\n" +
      "Le choix de l'IEF est une belle voie d'expansion.\n" +
      "Pour l'instant, aucun de nos enfants ne souhaitent retourner à l'école, nous ne les scolariseront jamais contre leur volonté.\n" +
      "Nous entrons en désobéissance civile afin de préserver leur liberté.",
    latitude: 46.9419764,
    longitude: -0.5227487,
    date_declaration: "2024-02-26",
  },
  {
    id: "virginie-mikael",
    noms: "Virginie et Mikael Tirloy",
    enfants: "Mouhammad-Amine, Khadija, Ibrahim et Soumay",
    departement: "Pas-de-Calais",
    presentation:
      "Le jour où la question de l'instruction de notre premier enfant est apparue, l'IEF était une évidence.\n" +
      "Nous estimons en temps que parents, que nous sommes les plus à même d'accompagner nos enfants vers leur vie d'adulte.\n" +
      "La liberté de choix dans leurs apprentissages est essentielle pour nous et ne peut nous être imposée.\n" +
      'La décadence du niveau des enfants instruits dans le système dit "traditionnel", les violences que ces petites têtes blondes subissent quotidiennement dans les établissements scolaires: harcèlement, menaces, lynchage etc... sont aussi des raisons supplémentaires de notre choix.\n' +
      "En effet, il n'est pas envisageable de laisser nos enfants à une institution qui ne peut garantir leur intégrité morale et physique.\n" +
      "Depuis onze ans, en cohérence avec nos valeurs et nos choix nous vivons cette magnifique aventure qu'est l'instruction en famille.\n" +
      "Nous souhaitons la poursuivre et cela encore pour de nombreuses années.\n" +
      "Respecter leur rythme et leur unicité. Prendre le temps de vivre, grandir et s'instruire autrement pour être puis devenir.",
    latitude: 50.5299909,
    longitude: 2.6025947,
    date_declaration: "2024-02-22",
  },
  {
    id: "fleur-gael",
    noms: "Fleur d'Echon et Gaël de Haas",
    enfants: "Cyann et Solal",
    departement: "Rhône",
    presentation:
      "Bonjour à Toustes !\n" +
      "Nous sommes une famille de 4 personnes : Gaël et Fleur, Solal (né en 2009) et Cyann (née en 2012).\n" +
      "Les enfants ont été scolarisé/es à 3 ans, mais leur malaise grandissant et leur envie d’apprendre déclinant au fil des ans, nous avons voulu essayer l’IEF fin 2019… et les événements qui ont suivi ont fini de nous convaincre qu’il était urgent de proposer à nos jeunes pousses une autre façon d’avancer dans la vie que les schémas conventionnels.\n" +
      "On a cru au début pouvoir concilier les attendus scolaires et cette nouvelle vie qui s’offrait à nous, on a beaucoup essayé, tâtonner, on s’est aussi pas mal cassé le nez… Pas toujours si simple de se reprogrammer 😊\n" +
      "Petit à petit, nous avons mis de côté les apprentissages scolaires pour nous concentrer sur le chemin déjà parcouru, plutôt que sur tout ce qu’il reste à apprendre, et laisser la place à leurs enthousiasmes de s’exprimer, sans jugement de valeur… L’intention est de les aider à comprendre leur façon d’être, d’apprendre et d’assimiler des informations, et de leur montrer que le Savoir n’est pas une notion figée, mais un ensemble de compétences en constante évolution. L’objectif est de les aider à développer leur autonomie et leur créativité, afin de s’épanouir à leur rythme.\n" +
      "Ce n’est pas toujours évident de composer avec les rythmes/envies/émotions de chacun/e, mais les difficultés que nous pouvons rencontrer aujourd’hui ne sont plus arbitraires et font sens pour nous. Et surtout, elles ne sont rien comparées à la joie de voir ces jeunes humain/es avancer sur leur route, s’approprier les environnements dans lesquels iels évoluent, en créant leurs propres chemins d’assimilation.\n" +
      "Grandir est jalonné de processus subtils qu’il nous semble important de respecter, et l’instruction en famille nous permet de grandir ensemble chaque jour…\n" +
      "Nos enfants ne souhaitent pas retourner dans le cursus classique pour l’instant, et nous sommes prêt/es à nous battre (sans violence) pour conserver cette liberté fondamentale du choix d’instruction.",
    latitude: 45.7456141,
    longitude: 4.7294349,
    date_declaration: "2024-03-13",
  },
  {
    id: "caroline-maxime-gregory",
    noms: "Caroline Blum, Maxime Furiga et Grégory Unrein",
    enfants: "Camille, Coline et Nehuen",
    departement: "Haute-Savoie",
    presentation:
      "Nous sommes une famille recomposée avec 3 enfants en IEF de 4 à 13 ans. De gauche à droite sur la photo : Grégory (papa de Camille et Coline), Coline, Camille, Nehuen, Caroline (maman de Camille, Coline et Nehuen) et Maxime (papa de Nehuen).\n" +
      "Nous sommes fiers de rejoindre les résistant.e.s d’Enfance libre, pour le bien-être de nos enfants.\n" +
      "Camille et Coline sont déscolarisés depuis la rentrée 2018 et Nehuen n’a jamais été scolarisé. Nous sommes formés à la méthode Montessori et nous l’appliquons chaque jour au quotidien. Notre choix a été dicté par l’envie d’un rythme plus doux, adapté aux besoins et aux envies de toute la famille, par une vie au plus proche de la nature et par la volonté de vivre et de grandir ensemble, car nous avons tant de choses à apprendre de nos enfants. Nous ne voulons pas les « dresser » mais leur permettre de se découvrir entièrement, sans concession, sans pression, sans attente, en respectant leur personnalité et leur individualité.\n" +
      "Nous avons demandé des autorisations depuis 2 ans, qui nous ont été accordées. Mais aujourd’hui, nous refusons de continuer à nous soumettre à des contrôles de plus en plus intrusifs et malveillants. Nous refusons de continuer à cautionner une loi liberticide qui va à l’encontre de tout ce que nous souhaitons pour nos enfants et qui va à l’encontre de nos valeurs sociales, écologiques et philosophiques. Nous souhaitons aussi nous positionner pour les générations futures et leur montrer qu’un choix est toujours possible, avec conscience et respect. Aujourd’hui, grâce à nos choix, nous sommes fiers d’affirmer que nos enfants sont… eux. Tout simplement… Et c’est déjà beaucoup !\n" +
      "Comme l’a dit Maria Montessori : « Nul ne peut être libre s’il n’est indépendant. ». Aujourd’hui, nous reprenons notre indépendance pour devenir enfin libres ",
    latitude: 45.9588714,
    longitude: 6.6675767,
    date_declaration: "2024-03-18",
  },
  {
    id: "marie-julien",
    noms: "Marie Simon et Julien Blanc",
    enfants: "Paola, Django, Leïlo, Zayin et Bohème",
    departement: "Hautes-Alpes",
    presentation:
      "Pourquoi obliger tous les enfants à suivre la même voie? Pourquoi cette volonté d'uniformiser la pensée et le corps dès le plus jeune âge?\n" +
      "La nature, les animaux, les gens autour de nous offrent tellement de spectacles, de merveilles, de questions... Qu'en est il de l'école de la Vie?\n" +
      "Il est facile de se rendre compte que chaque être humain est différent d'un autre. Une telle infinité de facettes, et un seul type d'intelligence possible? Un seul type d'instruction valable? Nous n'y croyons pas.\n" +
      "L'instruction en famille nous ouvre le champ des possibles, des expérimentations, des partages, nous offre une liberté de mouvement et de pensée immense! Les enfants sont de grands curieux et des chercheurs très appliqués! Ils arrivent très bien à apprendre quand le sujet étudié les passionne.\n" +
      "Tous ont des centres d'intérêt différents, et l'école seule ne peut répondre à des particularités et rythmes si multiples.\n" +
      "Certains enfants seront très épanouis à l'école quand d'autres y seront en souffrance. Cela nous semble primordial de respecter le développement individuel de chacun de nos enfants et de leur apporter les ressources et aides nécessaires pour entreprendre et apprendre selon leurs besoins et envies.\n" +
      "Il n'existe pas de recette parfaite et nous ne prétendons pas à la perfection, nous sommes de simples parents qui souhaitons conserver notre libre arbitre et notre liberté de choix en ce qui concerne l'éducation de nos enfants.\n" +
      "Alors, permis de construire, permis de conduire, permis de pèche, permis d'avoir des poules,... et maintenant le permis de nous occuper de nos enfants...? Et bientôt il faudra un permis pour en faire aussi?\n" +
      "Nous trouvons qu'il est absurde de demander la permission pour un droit essentiel, intrinsèque au rôle des parents.\n" +
      "L'interdiction générale d'instruire les enfants en famille et les demandes d'autorisations sont à l'inverse de la démocratie. La liberté d'instruction est un droit fondamental, il est naturel que chacun puisse définir quel type d'éducation il souhaite offrir à ses enfants.\n" +
      "La France propose une école publique et gratuite pour tous les parents qui ne se sentent pas capables, n'ont pas le temps ou l'envie d'instruire en famille, et c'est parfait ainsi; or l'État doit avant tout respecter les décisions de chacun dans leurs choix pédagogiques s'ls sont faits dans l'intérêt supérieur de l'enfant.\n" +
      "Nous sommes une famille recomposée vivant dans un petit village de montagne, et nous sommes disponibles et enthousiastes pour donner une instruction bienveillante à nos enfants.\n" +
      "Marie: 36ans, bergère, utopiste et amoureuse des plantes et de la montagne.\n" +
      "Mère de Leïlo (9ans), Zayin (3ans) et Bohème (9mois).\n" +
      "Julien: 40ans, travailleur saisonnier, utopiste, licencié de philosophie, ayant vadrouillé à travers le monde des arts et du spectacle avant de s'installer dans les montagnes.\n" +
      "Père de Paola (11ans), Django (10ans), Zayin et Bohème.\n" +
      "Les trois grands sont à l'école du village, Paola et Django depuis la maternelle et Leïlo depuis le CP, et ce par choix de leur part.\n" +
      "Pour Zayin, qui devrait y rentrer en septembre, nous avons opté pour l'IEF.\n" +
      "Nous rejoignons Enfance Libre dans son mouvement de désobéissance civile, pour le retour à un régime déclaratif.",
    latitude: 44.7717481,
    longitude: 6.9716507,
    date_declaration: "2024-04-08",
  },
  {
    id: "cecile-jordan",
    noms: "Cécile Gironnay et Jordan Matuba",
    enfants: "Janae, Elikia et Amani",
    departement: "Seine-et-Marne",
    presentation:
      "Lorsque nous sommes devenus parents, tout un univers s'est ouvert à nous. Nous nous sommes beaucoup interrogés et renseignés sur la façon dont nous souhaitions accompagner nos enfants dans leur développement. Au fil de nos lectures et de nos rencontres, l'IEF nous a semblé être une évidence au regard de nos valeurs philosophiques, éducatives et familiales. Accompagner chacun de nos enfants dans son individualité, lui permettre d'avancer à son rythme, selon ses envies et besoins individuels est important pour nous. L'IEF est notre choix, un choix d'éducation et d'instruction parmi tant d'autres qui se doit d'être respecté. C'est pourquoi nous nous mobilisons contre la restriction de ce droit fondamental.",
    date_declaration: "2024-04-11",
    date_fin_resistance: "2025-01-30",
    latitude: 48.7845333,
    longitude: 2.4111284,
  },
  {
    id: "philomene-mathieu",
    noms: "Philomène et Mathieu Deslandes",
    enfants: "Louve, Hanaé, Mitsuki et Neige",
    departement: "Haute-Garonne",
    presentation:
      "Nous sommes une famille de six, 4 filles instruites à la maison.\n" +
      "La question de l’école s’est posée pour l’aînée alors qu’elle avait 3 ans, nous avons visité celle du village, mais notre enthousiasme restait très relatif et nous avons préférer commencer l’aventure de l’instruction en famille. Respecter son rythme, sa sensibilité, la laisser être une enfant, jouer, était une évidence : au fil des années, cette liberté essentielle est devenue indispensable, mais aussi une expérience très enrichissante pour tout le monde.\n" +
      "Quand la loi a changé, et bien qu’en totale contradiction avec tout ce qui fait sens pour nous, nous avons fait une demande d’autorisation pour notre dernière, Neige, alors âgée de 3 ans, pensant naïvement qu’elle ne pourrait pas être refusée. Face au refus de l’administration, nous avons scolarisé Neige dans une école alternative. Tous les recours légaux que nous avons tentés ont échoué, que ce soit avec l’Éducation Nationale ou la justice…\n" +
      "Si Neige s’était sentie bien dans l’école ou avait simplement voulu continuer, elle serait encore scolarisée en 2023/2024. Mais ce ne fut pas le cas… Au pied du mur, nous n’avions guère d’autres choix que l’entrée en désobéissance civile.\n" +
      "Nos filles aujourd’hui n’ont toujours aucune envie d’être scolarisées, nous pensons toujours que l’instruction en famille est mieux adaptée pour nos enfants… et la contrainte n’est vraiment pas la meilleure façon de prouver l’intérêt d’une scolarisation. La vision étriquée et limitante que l’on cherche à nous imposer apparaît archaïque au regard de la richesse et de l’extraordinaire potentiel d’un enfant et en tant que parents, nous voulons préserver cette richesse.",
    date_declaration: "2023-11-19",
    latitude: 43.3245101,
    longitude: 1.1207994,
  },
  {
    id: "virginie-gaetan",
    noms: "Virginie Foncillas Caballero et Gaëtan Doyer",
    enfants: "Maëlie",
    departement: "Tarn-et-Garonne",
    presentation:
      "Nous sommes Virginie et Gaëtan parents de Maëlie (12 ans). " +
      "Après 4 années de scolarité (maternelle et CP) mal vécues par notre fille, nous nous sommes lancés dans l’aventure IEF depuis 6 ans maintenant. " +
      "Nous avons trouvé un équilibre en conciliant vie de famille, profession et apprentissage (souhait que de nombreuses familles ont sans y parvenir). " +
      "Maëlie s’épanouit pleinement en apprenant à son rythme et découvre que l’apprentissage n’est pas limité à la simple scolarité mais à chaque moment de la vie dans de nombreux domaines. " +
      "Chaque instant passé au contact de la nature, à voyager, à découvrir par elle-même est un enrichissement que l’école ne pourra jamais lui apporter." +
      " Il est donc inenvisageable pour nous de sacrifier notre enfant pour une loi basée sur des mensonges qui bafoue la Convention Européenne de Sauvegarde des Droits de l’Homme et des Libertés Fondamentales ni de nous soumettre à une demande d’autorisation subjective. " +
      "Il est encore moins envisageable de la confier à un système défaillant en tout point qui plus est enferme l’enfant dans une uniformisation de la pensée et de l’être. " +
      "C’est pourquoi notre décision d’entrer en désobéissance civile au côté d’Enfance Libre nous est apparue comme une évidence. " +
      "Il est en effet, de notre devoir de parents de tout mettre en œuvre dans l’intérêt supérieur de notre enfant et une nécessité pour que les familles puissent un jour revenir à un régime déclaratif.",
    date_declaration: "2024-03-14",
    latitude: 44.0479856,
    longitude: 1.4575075,
  },
  {
    id: "maylis-benoit",
    noms: "Maylis Poitier et Benoit Colnot",
    enfants: "Cosima",
    departement: "Vosges",
    presentation:
      "Nous sommes Maylis et Benoît, les parents d’une petite fille qui aura bientôt 5 ans.\n" +
      "Sa venue au monde a profondément enrichi notre relation à l’humain. Nous avons adopté une parentalité bienveillante et consciente, basée sur le respect de chacun.\n" +
      "Ainsi, dans la continuité de ce que nous avions entrepris depuis sa naissance, lorsque la question de la scolarisation s’est posée, l’instruction en famille (IEF) s’est rapidement imposée comme une évidence, en accord avec nos valeurs et le type d’éducation que nous souhaitions lui offrir.\n" +
      "Pour nous, l’IEF n’est pas une opposition à l’école.\n" +
      "L’IEF incarne une approche éducative où notre enfant est au centre des apprentissages. La connexion avec la nature est essentielle, c’est pourquoi nous privilégions les activités en extérieur. Nous respectons ses rythmes et ses besoins.\n" +
      "L’IEF n’est pas un choix pris à la légère, mais le pilier de notre mode de vie. Notre quotidien est structuré autour de cette décision, qui, selon nous, respecte pleinement l’intérêt supérieur de notre enfant.\n" +
      "Aujourd’hui, cette liberté et ce droit sont menacés.\n" +
      "Nous avons donc pris la décision consciente d’entrer en désobéissance civile.\n" +
      "Pour notre fille, pour tous les enfants instruits en famille, et pour ceux qui pourraient un jour en avoir besoin, nous choisissons de lutter de manière pacifique et non violente pour le retour à un régime déclaratif.",
    date_declaration: "2024-06-02",
    latitude: 47.928645,
    longitude: 6.583342,
  },
  {
    id: "anna-norman",
    noms: "Anna Varenne et Norman Chancogne",
    enfants: "Salomé",
    departement: "Haute-Vienne",
    presentation:
      "Nous, Norman et Anna, avons découvert que nous avions la possibilité d’instruire notre enfant en famille lors de la grossesse de Salomé.\n" +
      "Nous avons alors réalisé que nous avions le CHOIX ! Cela nous a alors paru comme une évidence de choisir l’IEF. Car en désirant avoir un enfant, nous faisions aussi le choix de l’élever, d’en prendre soin et de vivre pleinement avec elle.\n" +
      "C’est pour cela qu’aujourd’hui nous avons choisi d’entrer en résistance face à cette loi qui nous semble injuste.\n" +
      "Nous souhaitons que tous parents, toutes familles puissent faire ce choix, libre et éclairé, de l’instruction de ses enfants!",
    date_declaration: "2024-05-29",
    date_fin_resistance: "2024-10-17",
    latitude: 45.7163726,
    longitude: 1.7901553,
  },
  {
    id: "noelie-nicolas",
    noms: "Noëlie Simon et Nicolas Haran",
    enfants: "Oïhana",
    departement: "Dordogne",
    presentation:
      "En découvrant l'instruction en famille, un autre monde s'est ouvert à nous avec la possibilité d'avoir des enfants.\n" +
      "L'instruction en famille est un des piliers de notre mode de vie, afin d'être dans la vie.\n" +
      "Prendre la responsabilité totale de l'éducation et de l'instruction de notre fille est devenue une évidence.\n" +
      "Issus tous deux de familles dites classiques, nos expériences de vie nous ont permis de changer de perspectives et d'accepter de sortir des rangs conventionnels, en restant adaptés au monde actuel. Nous aimons questionner le monde et remettre en cause les règles pré-établies afin d'évoluer vers une justesse et un équilibre subtil, nous correspondant.\n" +
      "Nous souhaitons que notre fille ait le temps: de se connaitre avec ses limites et besoins essentiels dans la sérénité et sans jugements, dans un espace verdoyant authentique; en ayant la liberté d'aller découvrir de nouveaux horizons et d'aller en confiance vers les autres selon son envie.\n" +
      "Les bureaucrates fassent à nous, submergés par la paperasse institutionnelle, manquent de temps pour se soucier de l'intérêt de l'enfant.\n" +
      "Nous refusons de passer plus de temps en dossiers de justifications, temps précieux indisponible pour notre fille.\n" +
      "A l'instant où j'écris, ma fille vient me voir pour me dire:\n" +
      '"Dans Maurice y\'a le son [o] sans la lettre "o" alors les 2 lettres "au" sont le son [o].*\n' +
      "Toutes les fois où je lui ai parlé de ce son, ne sont rien face à ce moment où elle vient de l'intégrer par elle-même afin de le retenir.\n" +
      "Peu importe son âge, c'était le moment pour elle. Ces anecdotes sont récurrentes, magiques et précieuses. Nous les préservons coute que coute.\n" +
      "Nous avons suivi l'ouverture de l'école dynamique à Paris. Nous rêvons avec \"En liberté\".\n" +
      'Suivant depuis le début l\'association "Enfance Libre", nous sommes fiers de rejoindre les résistants.',
    date_declaration: "2024-06-05",
    latitude: 45.1364877,
    longitude: 1.2075594,
  },
  {
    id: "mireille-thomas",
    noms: "Mireille et Thomas Fuchez",
    enfants: "Lucas et Gabriel",
    departement: "Mayenne",
    presentation:
      "Nous sommes Thomas et Mireille, parents de Lucas (14 ans) et Gabriel (10 ans). Nous avons découvert l’IEF sur le tard puisque Lucas a été scolarisé jusqu’en CE2. C’est à sa demande (et après avoir bien étudié le sujet) que nous nous sommes lancés dans l’aventure, du CM1 à la 5ème. Nous ne sommes pas opposés par principe à la scolarisation puisque Lucas a demandé de tenter l’expérience collège en 4ème, que nous y avons souscrit immédiatement et que ses premières années à l’école s’étaient très bien passées.\n" +
      "Gabriel, de son côté, a fait 1 an à l’école en petite section, puis a suivi le chemin de son frère et est en IEF depuis. Lui ne souhaite pas retourner à l’école pour le moment.\n" +
      "En tant que parents responsables et citoyens, nous souhaitons rester libres et décideurs du mode d’instruction et de la pédagogie qui correspondent le mieux à nos idées et notre philosophie de vie. À travers l’IEF, nous avons recentré l’apprentissage et l’éducation autour de notions fondamentales comme l’enthousiasme, la curiosité, la coopération, une vie équilibrée plus lente, en accord avec la nature, les rythmes physiologiques. Nous avons autant à apprendre de nos enfants que les enfants de nous, parents.\n" +
      "Nous refusons la nouvelle loi visant à lutter soi disant « contre le séparatisme » et ses conséquences qui balaient d’un revers de la main tous les principes et conventions relatives aux droits de l’homme et aux libertés fondamentales.\n" +
      "Notre décision d’entrer en désobéissance civile a été le fruit d’une longue réflexion et de nombreux échanges, et chaque jour un peu plus, nous remercions « Enfance libre » d’exister et de nous accompagner dans notre démarche, pour l’intérêt supérieur de nos enfants et de la société toute entière, et pour un retour au régime déclaratif de l’IEF.\n",
    date_declaration: "2024-06-13",
    latitude: 48.0578442,
    longitude: -0.8103822,
  },
  {
    id: "joanna-simon",
    noms: "Joanna Nageli et Simon Payraudeau",
    enfants: "Matteo et Tituan",
    departement: "Lot-et-Garonne",
    presentation:
      "Après avoir expérimenté l’enseignement et une grande variété de pédagogies pendant plusieurs années, dans différents pays, il était évident pour nous, avant même la naissance de Matteo, que notre enfant n’irait pas à l’école.\n" +
      "Nous avons préféré choisir un mode de vie nous permettant d’être disponible pour accompagner notre enfant dans tous les apprentissages de sa vie. En effet, en travaillant sur notre lieu d’habitat Matteo apprend tous les jours à nos côtés. Il développe toutes les capacités nécessaires à sa bonne intégration dans la société, tout simplement par observation et imitation de nos activités et nos comportements. Il partage notre quotidien et apprend librement tout ce qui retient son attention dans les domaines les plus variés, de la mécanique à la cuisine, en passant par le maraichage et l’agroforesterie. Il rencontre à travers notre vie quotidienne un grand nombre de personnes, de toutes professions, de tous âges et de tous horizons.\n" +
      "Nous refusons donc aujourd’hui d’obéir à la nouvelle loi imposant une demande d’autorisation préalable pour faire l’instruction en famille car nous estimons qu’elle restreint la liberté de choix des parents de choisir le mode d’instruction de leur enfant et par conséquent enfreint l’article 26 de la Déclaration universelle des droits de l’Homme à savoir: « Les parents ont, par priorité, le droit de choisir le genre d’éducation à donner à leurs enfants. »\n",
    date_declaration: "2024-06-19",
    latitude: 44.5001956,
    longitude: 0.8869536,
  },
  {
    id: "mylene-sylvain",
    noms: "Myléne Fady et Sylvain Ruzand",
    enfants: "Lilouan et Maïlinne",
    departement: "Isère",
    presentation:
      "Nous sommes Mylène et Sylvain parents de Lilouan et Maïlinne et nous\n" +
      "avons choisis l’école de la Vie.\n" +
      "Paysans au pied du Vercors nous avons appris en observant nos parents et grands parents, puis nous avons tâtonné, expérimenté, écouté et observé les messages du vivant. Tous les jours nous continuons d’apprendre.\n" +
      "C’est au milieu des humains (nous partageons notre lieu de vie avec d’autres personnes), des animaux de la ferme et de la forêt, des " +
      "insectes, des arbres et des plantes que nous avons choisis de vivre et nous souhaitons que cette belle diversité puisse aussi être une " +
      "source d’émerveillement et d’apprentissage au quotidien pour nos " +
      "enfants.\n" +
      "\n" +
      "L’être humain est un passionné si nous lui laissons l’espace et le temps d’explorer.\n" +
      "\n" +
      "Les enfants ont des centres d’intérêts différents et c’est pour " +
      "nous primordial de respecter les spécificités de chacun et de leur " +
      "apporter les ressources nécessaires pour apprendre selon leurs " +
      "envies et leurs besoins.\n" +
      "\n" +
      "Nous ne prétendons pas détenir une recette parfaite, mais nous sommes " +
      "convaincus que la richesse du monde dépend de la diversité des " +
      "apprentissages et du respect du développement individuel à l’inverse de l’uniformisation des pensées.\n" +
      "\n" +
      "Nous refusons de demander la permission pour une liberté fondamentale, " +
      "inhérente au rôle de parents.\n" +
      "\n" +
      "Nous choisissons la voie de la désobéissance civile, pour l’avenir de " +
      "nos enfants et pour toutes les familles qui souhaitent participer à " +
      "l’aventure de l’IEF.\n" +
      "\n" +
      "Nos enfants seront des citoyens éclairés qui connaissent l’importance " +
      "de choisir sa vie et non de la subir, la valeur du mot liberté.",
    date_declaration: "2024-06-13",
    latitude: 45.1425854,
    longitude: 5.3770652,
  },
  {
    id: "marie-kevin",
    noms: "Marie et Kévin Direz",
    enfants: "Helena",
    departement: "Haute-Saône",
    presentation:
      "Tous les deux en recherche permanente de l'équilibre intérieur et l'harmonie collective nous tenons à continuer d'apprendre, " +
      "d'inspirer tels une petite flamme qui en allumerait d'autres, afin d'être ce que nous voulons pour notre monde de demain.",
    date_declaration: "2024-07-24",
    latitude: 47.8560423,
    longitude: 6.431364,
  },
  {
    id: "emanuela-sylvain",
    noms: "Emanuela Carl et Sylvain Carignano",
    enfants: "Nils et Tilo",
    departement: "Ardèche",
    presentation:
      "Nous sommes Sylvain et Emanuela, les parents de Nils (15 ans) et Thilo (10 ans). En 2015, après les 3 années de maternelle de Nils, nous avons décidé de le déscolariser car il avait des symptômes de phobie de scolaire. Thilo avait alors 1 an et nous avons commencé à découvrir la vie en Instruction en Famille. C'est resté notre mode de vie pendant 9 ans, car même après notre séparation en 2018, nous avons fait le choix de continuer l'IEF en garde alternée.\n" +
      "Cette façon de vivre nous correspond pleinement. Nous ferons tout pour maintenir ce droit, pour nous-mêmes et pour toutes les familles qui en ont besoin/envie.",
    date_declaration: "2024-07-25",
    latitude: 44.5498937,
    longitude: 4.2920284,
  },
  {
    id: "lise-brahim",
    noms: "Lise Sempéré et Brahim Naim",
    enfants: "Imad, Sihem et Nahila",
    departement: "Vienne",
    presentation:
      "Nous sommes les parents de 3 petites têtes très curieuses nés en 2014, 2017 et 2021 et nous habitons dans la Vienne.\n" +
      "Considérant que le système éducatif ne correspondais pas à nos attentes, nous avons débuté l'instruction en famille en 2017.\n" +
      "Nous nous opposons à la loi n°2021-1109 du 24 Août 2021, dites loi sur le respect des principes de la République, car nous estimons que cette loi constitue une atteinte grave aux libertés fondamentales et aux principes démocratique qui fondent notre République, et c'est pourquoi, nous entrons en désobéissance civile.\n" +
      "Une démarche, qui nous espérons, permettra un retour au régime déclaratif.\n" +
      "\n" +
      "Lise et Brahim\n",
    date_declaration: "2024-08-23",
    date_fin_resistance: "2025-01-19",
    latitude: 46.8147853,
    longitude: 0.475941,
  },
  {
    id: "lucile-thomas",
    noms: "Lucile Oudot et Thomas Gomez",
    enfants: "Maélis",
    departement: "Isère",
    presentation:
      "Nous sommes Thomas et Lucile, parents de Maélis. \n" +
      "\n" +
      "Depuis sa naissance, nous pratiquons le parentage proximal et l'éducation respectueuse, et l'instruction en famille s'inscrit naturellement dans cette continuité. L'IEF est un choix mûrement réfléchi, pris en accord avec Maélis et dans son\n" +
      "intérêt supérieur. \n" +
      "\n" +
      "Ayant déjà expérimenté la crèche, elle refuse catégoriquement d'aller à l'école.\n" +
      "Cette option est pour nous la meilleure, car elle respecte pleinement le consentement, le rythme, les besoins, et le bien-être de Maélis, ainsi que son épanouissement, et son intégrité physique et morale.\n",
    date_declaration: "2024-08-28",
    latitude: 45.1842673,
    longitude: 5.6375998,
  },
  {
    id: "camille-simon",
    noms: "Simon et Camille Chenet",
    enfants: "Arthur et Raphaël",
    departement: "Doubs",
    presentation:
      "Nous sommes séparés de fait mais toujours d’excellents amis, solidaires dans\n" +
      "l'éducation de nos enfants et la recherche de leur intérêt suprême, et plus que jamais\n" +
      "dans le combat qui nous attend face à la suppression de l'instruction en famille, un\n" +
      "droit et une liberté fondamentale et universelle.\n",
    date_declaration: "2024-08-29",
    latitude: 47.4908196,
    longitude: 6.7450192,
  },
  {
    id: "marguerite-gillali",
    noms: "Marguerite Guillaume Zerki et Gillali Zerki",
    enfants: "Nausicaä et Galahad",
    departement: "Pyrénées-Orientales",
    presentation:
      "Nous, c'est Marguerite et Gillali, heureux parents de Nausicaä (8 ans) et Galahad (4 ans).\n" +
      "Nos enfants ont toujours été en instruction en famille et ce mode d'apprentissage convient parfaitement à toute la famille. Notre pédagogie est trilingue (français, anglais, japonais) et toutes les situations du quotidien peuvent servir de prétexte à développer des connaissances.\n" +
      "C'est pourquoi nous chérissons la liberté qu'est l'IEF car, dans notre cas, la maîtrise des rythmes d'apprentissage est primordiale.\n" +
      "Aujourd'hui, malgré 5 années de contrôles académiques très positifs, l'administration de l'éducation nationale désire nous priver de cette liberté de choix.\n" +
      "Notre motif 4 : « Trilinguisme en vue d'une expatriation » ayant déjà été accepté l'année dernière pour notre cadet, nous sommes tombés de haut.\n" +
      "Notre situation n'a pas changé.\n" +
      "Mais le changement de DASEN et le zêle de certains fonctionnaires à appliquer de « nouvelles consignes internes » au mépris de la loi, ont voulu avoir raison de notre choix de vie.\n" +
      "Après prise de contact avec le collectif de l'académie de Montpellier pour l'instruction en famille (CMIEF), nous avons vite compris que la grande majorité des dossiers motif 4 était refusés pour des motifs fallacieux.\n" +
      "Ainsi, alors même que ce motif 4 « situation propre à l'enfant » a été exigé par le conseil constitutionnel afin de validé la constitutionalité de cette loi, l'administration refuse quasiment systématiquement les dossiers présentés sur ce motif.\n" +
      "Nous avions, assez naïvement, décidé de faire confiance à l'éducation nationale pour respecter tant la lettre que l'esprit de la loi. Cette confiance a été trahie.\n" +
      "Passé le choc, nous avons dès le mois de juin 2024, décidé d'entrer en désobéissance civile après avoir acquis la conviction qu'un retour au régime déclaratif est indispensable.\n" +
      "Notre déception a rapidement fait place à la colère alors que pendant les trois mois d'été, nous suivions le parcours de familles brutalisées et paupérisées (frais d'avocats) par une administration cynique et sourde à la détresse qu'elle génère.\n" +
      "De nombreuses familles ont lutté pour obtenir leur sésame pour la rentrée : RAPO, avocats, référé suspension, tribunaux administratifs, recours gracieux, manifestations... en vain. Essayer de jouer selon les règles est inutile face à l'arbitraire.\n" +
      "Nous avons désormais l'intime conviction que les décisions de l'éducation nationale sont abusives et que l'intérêt supérieur de l'enfant n'entre jamais en ligne de compte, et cela, quel que soit l'échelon de leur hiérarchie.\n" +
      "Nous nous engageons au côté d'Enfance Libre car lutter contre les abus et la violence symbolique qu'entraîne cette loi injuste est un devoir citoyen.\n" +
      "Nous refusons de quitter le pays prématurément ou d'essayer de passer « sous les radars » car nous estimons la liberté de choix du mode d'instruction de nos enfants fondamentale même si aujourd'hui, en France, une volonté politique tente par tous les moyens de la réduire à néant via des procédés détournés.\n" +
      "Il est à noter qu'en son temps, Rosa Parks refusa de céder son siège car elle était tout simplement « fatiguée ».\n" +
      "A l 'heure actuelle de nombreuses familles sont au-delà de la fatigue, face à une application de la loi profondément injuste et n'ont comme seul recours que l'entrée en désobéissance civile pour ce faire entendre. Ce qui est le comble de l'ironie pour une loi qui prétend lutter contre le séparatisme.\n",
    date_declaration: "2024-08-28",
    latitude: 42.3944492,
    longitude: 2.5809725,
  },
  {
    id: "corine-guillaume",
    noms: "Corine Tessier et Guillaume Bouthié",
    enfants: "Samuel",
    departement: "Pyrénées-Orientales",
    presentation:
      "Nous sommes Corinne et Guillaume, des Pyrénées Orientales, les parents de Samuel, instruit en famille depuis 2020.\n" +
      "\n" +
      'Nous avons profité du "plein droit" pendant 2 ans, puis nous avons tenté une demande d\'autorisation pour cette année (refusée sans surprise) tout en sachant déjà que nous entrerions en résistance au bout du compte.\n' +
      "\n" +
      "Nous y voilà donc, merci de nous accueillir et surtout merci aux premiers résistants, que nous suivons sur les réseaux depuis le début et qui, par leur parcours, nous ont aidé à franchir le cap de la désobéissance civile\n",
    date_declaration: "2024-08-28",
    latitude: 42.3846557,
    longitude: 2.514671,
  },
  {
    id: "melanie-laurent",
    noms: "Mélanie Hicks et Laurent Cabaret",
    enfants: "Angelique",
    departement: "Cher",
    presentation:
      "Nous sommes Mélanie et Laurent, parents d'Angélique, 11 ans. Nous avons\n" +
      "fait le choix dès sa naissance de l'accompagner pleinement et l'IEF était une continuation naturelle à notre choix de vie. Pouvoir lui offrir une vie plus libre, moins contraignante, moins formatée, plus sereine ainsi que du temps pour se découvrir et de trouver ce qui la fait vibrer est essentielle pour nous.\n" +
      "\n" +
      "Face aux modifications du code de l'éducation (art. 49) et en parallèle\n" +
      "face à une inspection académique devenant de plus en plus intolérante, inflexible, critique et parfois moralisatrice, nous avons bien senti que le vent tournait et que si nous ne nous positionnions pas clairement nous allions le regretter.  Et pourtant\n" +
      "nous avons profité du plein droit...\n" +
      "\n" +
      "Mais cette année, demander l'autorisation nous était impossible. Nous nous sommes posés avec l'idée de voyager ou essayer d'être hors radar mais nous aurions été forcé à vivre une vie que nous ne souhaitions pas vraiment. Donc l'action juste pour nous était de dire NON, non à cette demande d'autorisation qui va à l'encontre de notre liberté parentale et qui est dangereuse pour toute future famille voulant jouir de ce droit fondamental quelque qu'en soit les raisons.\n" +
      "\n" +
      "Nous remercions très sincèrement Enfance Libre et toutes les familles qui ont posé leurs voix avant nous. Vos retours d'expériences nous ont donné le courage de nous jeter à l'eau et d'être enfin alignés avec nos convictions et valeurs en nous positionnant clairement.\n" +
      "\n" +
      "Nous souhaitons plein de courage à tout le monde.\n",
    date_declaration: "2024-08-28",
    latitude: 46.7529157,
    longitude: 2.5260973,
  },
  {
    id: "celine-benoit",
    noms: "Céline Captin et Benoît Couteau",
    // La famille ne souhaite pas publier le nom de ses enfants
    enfants: "2 enfants",
    departement: "Hérault",
    presentation:
      "Nous sommes les parents de 2 humains nés en 2015 et 2017. Dès la naissance de notre aîné, nous avons eu à cœur de l’accompagner le plus respectueusement possible. Lors de rencontres sur la parentalité nous avons découvert l’ief. Céline, étant à ce moment-là encore instit, nous avons naturellement débuté le chemin de l’ief qui se conjugue parfaitement avec nos valeurs de bienveillance, de respect, de rythme, de liberté…\n" +
      "L’appel de Marjorie et Ramïn en 2022 pour la désobéissance civile nous a séduit. Cependant, il nous a fallu du temps pour cheminer et oser nous lancer en désobéissance civile.\n" +
      "Le temps de notre pleine souveraineté est arrivé et c’est avec joie que nous nous engageons aujourd’hui pour défendre le droit à l’instruction en famille et le retour au régime déclaratif.\n",
    date_declaration: "2024-08-31",
    latitude: 43.3175425,
    longitude: 3.1209421,
  },
  {
    id: "laetitia-samuel",
    noms: "Laëtitia et Samuel von Allmen",
    enfants: "Juliette, Antonin & Ferdinand",
    departement: "Haute-Saône",
    presentation:
      "Parents de trois enfants toujours instruits en famille afin de poursuivre ce que nous faisions depuis leur naissance : respecter leurs rythmes, leurs besoins et leurs personnalités. Nous avons toujours dit que nos enfants iraient à l'école s'ils en faisaient la demande, mais ce n'est pas leur souhait pour le moment.\n" +
      "Nous avions eu les autorisations en 2022-2023 et 2023-2024 pour les deux plus jeunes, l'aînée bénéficiait alors de la dérogation de plein droit pour cette même période. Cette année, refus pour les trois. Refus massifs au sein de l'académie de Besançon...\n" +
      "Demander l'autorisation était déjà difficile, recevoir un refus injuste n'est pas acceptable pour nous, après 8 contrôles très positifs.\n" +
      "Nous ne demanderons donc plus l'autorisation et entrons en désobéissance civile.\n",
    date_declaration: "2024-09-04",
    latitude: 47.6882446,
    longitude: 6.5520916,
  },
  {
    id: "alice-mikael",
    noms: "Alice et Mikaël Fruhauf",
    enfants: "Abraham et Mayawen",
    departement: "Jura",
    presentation:
      "Il y a 7 ans, l’arrivée d’une troisième personne dans notre équipage, nous a donné l’opportunité d’entreprendre un grand voyage. Pas à pas, main dans la main, nous nous sommes amusés à découvrir le monde et à apprendre à nous connaître les uns les autres et nous-même. Cette\n" +
      "aventure nous remplie de joie, dans ses hauts et dans ses bas. Ce périple nous ravie et apporte du sens à notre existence. Et il y a un an, au cours de notre voyage, un quatrième membre a rejoint notre équipage. L’aventure n’en est que plus palpitante et merveilleuse.\n" +
      "Mais un jour, alors que nous avancions avec entrain, on nous proposa de bifurquer, d’accélérer, d’entrer dans une frénésie mortifère et d’offrir quelques unes de nos années pour expérimenter une autre manière de voyager, d’appréhender notre humanité. Un rythme soutenu et\n" +
      "répétitif, des apprentissages imposés à un moment normé, une immobilité quotidienne, des résultats à donner et surtout ne pas écouter notre cœur qui a soif de souplesse et de liberté... après réflexion, cela ne nous convient pas.\n" +
      "Nous avons donc poliment refusé et rappeler la légalité de notre liberté. Mais notre « non » n’a pas été accepté. Et on a voulu nous imposer ce projet, nous obliger.\n" +
      "Nous marchons et ne souhaitons pas courir. Nous sommes heureux, en bonne santé et pour ces simples raisons, nous déclinons pour de bon la proposition. En confiance, nous formulons le vœu de poursuivre notre route en famille. Nous décidons de faire un pas de côté afin de sortir du sentier battu et balisé qu’impose notre pays. Nous continuons tout droit, en dehors de la loi, libres d’œuvrer pour un monde plus juste où chacun pourrait être créateur d’une paix durable et nécessaire.\n",
    date_declaration: "2024-09-04",
    latitude: 46.6209266,
    longitude: 5.4673389,
  },
  {
    id: "lucie-jerome",
    noms: "Lucie et Jérôme Coston",
    enfants: "Madeline et Eleonore",
    departement: "Seine-et-Marne",
    presentation:
      "Nous sommes Lucie et Jérôme, parents de trois enfants Madelyne 6 ans, Eléonore 4 ans et Daniel 2 ans.\n" +
      "L’instruction en famille à toujours été une évidence pour nous, un choix de vie et un projet mûrement réfléchi. Pouvoir passer le plus de temps possible ensemble, explorer les centres d’intérêts de nos enfants, vivre au rythme de notre famille et s’adapter aux besoins de chacun à chaque saison.\n" +
      "Nous choisissons l’instruction en famille pour l’épanouissement de nos enfants. Avec l’IEF nos enfants ont le choix d’apprendre ce qu’ils veulent, quand ils se sentent prêts et avec des pédagogies qui leurs correspondent. Parfois à table, parfois dans leurs lits, le plus souvent avec leurs animaux. Un confort qui ne leur serait pas permit à l’école.\n" +
      "Ils ont aussi le loisir de remplir leurs journées de bien d’autres choses que de l’instruction formalisée : jeux, créations artistiques, profiter de la nature, aller en sortie. Ils apprennent et ils s’amusent. Ils sont heureux.\n" +
      "Depuis le changement de la loi nous avons demandé à nos enfants si ils souhaitaient aller à l’école ou continuer d’apprendre à la maison. Leur réponse est sans appel ils souhaitent rester à la maison. C’est donc pour leur droit et leur liberté d’instruction que nous rejoignons les rangs des résistants et entrons en désobéissance civile.\n",
    date_declaration: "2024-07-18",
    latitude: 48.8716213,
    longitude: 2.6839132,
  },
  {
    id: "suzanne-elyes",
    noms: "Suzanne et Elyes Kessar",
    enfants: "Ismaïl et Maïssa",
    departement: "Loir-et-Cher",
    presentation:
      "Parents de deux enfants de 6 ans et 3 ans, nous avons passé une grande partie de notre vie de famille à l'étranger. Notre projet en rentrant en France était de prendre une année sabbatique pour profiter au mieux de nos enfants. Nous avions déjà songé à l'ief des années auparavant, puis cette année, elle a sonné comme une évidence. Qu'elle n'a pas été notre stupeur lorsqu'on s'est vu refuser nos demandes, nos RAPO. Nous avons cherché d'autres alternatives puis nous nous sommes rendus à une autre évidence. Celle de la désobéissance civile ! Nous sommes résistants face à cette loi liberticide, qui stigmatise une population, met à mal nos enfants... Nous souhaitons être libres, libres en tant que parents d'aimer nos enfants et de leur apporter le meilleur, quitte à prendre des risques !\n",
    date_declaration: "2024-09-05",
    latitude: 47.903348,
    longitude: 1.0978115,
  },
  {
    id: "delphine-jeanluc",
    noms: "Delphine et Jean-Luc Collignon",
    enfants: "Lucien",
    departement: "Moselle",
    presentation:
      "Nous sommes Delphine et Jean-Luc, parents de Lucien 11 ans en IEF depuis toujours.\n" +
      "L'instruction en famille a été un choix déterminant pour nos enfants, et nous en voyons aujourd'hui les fruits dans le parcours professionnel de nos 2 grands fils qui sont aujourd'hui mécanicien poids lourds et soudeur assembleur toujours en formation.\n" +
      "Nous avons subi au début de notre décision de ne jamais scolariser nos enfants tous les clichés possibles, manque de socialisation, enfants sauvages, ils n'auront jamais d'amis et encore moins de petites-amies etc...\n" +
      "Aujourd'hui avec le recul que nous avons sur ces bientôt 12 années d'IEF nous pouvons vous garantir que tous ces clichés sont faux.\n" +
      "Nos enfants sont épanouis et heureux ce qui leur permet de réussir brillamment dans leur vie professionnelle.\n" +
      "Ce choix s'est avéré être une excellente décision pour notre famille.\n" +
      "Ce fut un vrai choix d'amour pour nos 3 enfants !\n",
    date_declaration: "2024-09-03",
    latitude: 49.1607035,
    longitude: 6.077739,
  },
  {
    id: "alexandrine-olivier",
    noms: "Alexandrine & Olivier Delavaux",
    enfants: "Aloïs",
    departement: "Vosges",
    presentation:
      "Nos trois enfants sont passés par différentes phases de scolarisation puis d'instruction en famille, selon leur rythme et leurs besoins. Actuellement, seul notre dernier, Aloïs, 12 ans, est encore en âge d'être scolarisé, et comme pour ses aînés, nous respectons son rythme et ses besoins actuels en l'instruisant à la maison.\n" +
      "\n" +
      "Nous considérons que des personnes qui ne connaissent pas un enfant n’auront jamais la capacité de décider de façon éclairée, à la place de ses parents, ce qui est bon ou non pour lui, pas même sur la base d’un dossier aussi bien ficelé soit-il. **Un enfant n’est pas un numéro académique ou administratif ; il est une personne vivante, qui a des besoins et des droits**. Et notre responsabilité en tant que parents est de les faire respecter.\n",
    date_declaration: "2024-09-08",
    latitude: 48.4072699,
    longitude: 6.843762,
  },
  {
    id: "salome-galaad",
    noms: "Salomé Gaultier & Galaad Deboffle",
    enfants: "Taïa",
    departement: "Indre-et-Loire",
    presentation:
      "Il y a une dizaine d’années, après des parcours scolaires « classiques » et un grand voyage qui a changé nos vies, nous avons commencé à nous questionner sur l’École. Petit à petit nous nous sommes intéressé.es de plus en plus à l’« Éducation », aux relations adultes-enfants, au rapport à l’enfance en général. Dévorant livres, articles et films* sur ces sujets, " +
      "nous plongions dans un monde inconnu et fascinant, mais aussi parfois inconfortable car ces découvertes chamboulaient nos représentations, nos croyances et nos habitudes.\n" +
      "Nos attitudes ont changé et nous avons adopté un nouveau regard sur l’enfance.\n" +
      "\n" +
      "Nous découvrions que des enfants et adultes n’étant jamais allé.es à l’école et n’ayant jamais été scolarisé.es à la maison, n’étaient ni analphabètes, ni sans travail, ni « marginaux asociaux » mais bien au contraire des êtres épanouis, vivant leur vie avec enthousiasme, leurs passions étant devenues leurs métiers, à l’aise dans le monde et avec les autres ! (cf : film documentaire Être et devenir de Clara Bellar, livre … Et je ne suis jamais allé à l’école - histoire d’une enfance heureuse d’André Stern)\n" +
      "\n" +
      "Les recherches scientifiques actuelles, comme les travaux du neurobiologiste Gerald Hüther sur l’enthousiasme comme moteur des apprentissages ou du psychologue Peter Gray sur l’importance du jeu libre étayent ces constats : l’enfant apprend naturellement, tout le temps, à son rythme et selon ses intérêts propres, seul, avec les autres, dans son environnement.\n" +
      "\n" +
      "C’est ce que nous vivons depuis la naissance de Taïa qui vient d’avoir trois ans et nous ne voulons pas que cela s’arrête. C’est pourquoi nous ne mettrons pas Taïa à l’école (sauf si elle exprime le désire d’y aller) et entrons, du fait de la récente loi n°2021-1109 du 24 août 2021, en désobéissance civile (cf lettre ci-contre).\n" +
      "\n" +
      "* Clara Bellar, André Stern, Jean-Pierre Lepri, Peter Gray, François Bégaudeau pour citer certain.es autrices et auteurs.\n",
    date_declaration: "2024-09-02",
    latitude: 46.8504165,
    longitude: 0.8892077,
  },
  {
    id: "marie-michael",
    noms: "Marie Body et Michaël Dubosc",
    enfants: "Elyon et Cordelia",
    departement: "Corrèze",
    presentation:
      "Nous sommes Michaël et Marie, parents de deux filles nées en 2019 et en 2022.\n" +
      "\n" +
      "L'instruction en famille était un projet que nous envisagions avant même la naissance de notre aînée. Pouvoir s’instruire à son rythme tout en profitant de son environnement et pouvoir socialiser avec des personnes de tout âge nous semblait fondamental. Cette idée n'a pas changé et reste pour notre famille essentielle.\n" +
      "\n" +
      "Nous, nous lançons avec vous dans ce combat solidaire afin que tous les enfants, accompagnés de leurs parents, puissent avoir le choix quant à leur instruction.\n" +
      "\n" +
      "Et pour finir, nous souhaitions vous remercier, résistants et résistantes nous précédant, pour tous vos témoignages, vos mots nous ont donnés des ailes et l'aplomb pour entreprendre ce choix.\n",
    date_declaration: "2024-09-08",
    date_fin_resistance: "2025-03-10",
    latitude: 45.3074062,
    longitude: 1.3808186,
  },
  {
    id: "alison-yves",
    noms: "Alison Dubois et Yves Vannier",
    enfants: "Louison et Marceau",
    departement: "Jura",
    presentation:
      "Nous sommes Yves et Alison, parents de deux enfants instruits en famille depuis trois ans.\n" +
      "L’IEF est pour nous un mode de vie. Ce choix nous offre la possibilité d’être pleinement impliqués dans la vie de nos enfants, de prendre le temps de les voir grandir et de les éduquer en accord avec nos valeurs.\n" +
      "L’IEF n’est pas un choix fait contre l’école mais pour notre famille.\n" +
      "Aujourd'hui l'académie de Besançon nous refuse le droit de poursuivre l'IEF. Face à cette décision arbitraire et injuste nous avons pris la décision consciente d'entrer en désobéissance civile. Ce choix a pour but de préserver notre quotidien serein et épanouissant mais aussi de défendre le droit et la liberté de tout parent d’instruire ses enfants.\n",
    date_declaration: "2024-09-03",
    latitude: 46.7036614,
    longitude: 5.4160057,
  },
  {
    id: "maryline-erik",
    noms: "Maryline Fontaine et Erik Batut",
    enfants: "Maëlys",
    departement: "Seine-et-Marne",
    presentation:
      "Nous sommes Maryline et Erik, les parents de Kylian 15 ans et Maëlys 7 ans.\n" +
      "Famille recomposée, le choix de l'instruction en famille s'est fait suite au mal-être de l'ainé dans le système répétitif et rigide de l'école, qu'il a suivi, subit, jusqu'à la 6ème. Pour Maëlys c'est après une petite section et 2 mois de moyenne section que la bascule s'est faite de manière naturelle. Dans les deux cas, ce fut à leur demande.\n" +
      "Aujourd'hui, nos enfants s'épanouissent complètement dans ce mode de vie. Cette liberté de choisir d'instruire les enfants en respectant leurs besoins et leur choix, est une véritable source de satisfaction pour tous.\n" +
      "Cette année 2024, après la fin du plein droit, nous avons suivi les démarches pour obtenir l'autorisation. Autant pour Kylian l'autorisation a été accordée, autant ce fut refus sur refus pour Maëlys. Et, devant l'inhumanité et les postures dogmatiques de nos administrations, nous ne pouvions que faire le constat que nous n'aurions plus jamais gain de cause. Aussi avons nous opté sans aucune hésitation pour la désobéissance civile, qui correspond bien à notre valeur de liberté , qui a été tant malmenée par la loi d'abord et sa mise en application ensuite.\n" +
      "Nous rentrons donc en résistance, auprès d'Enfance Libre, l'esprit apaisé et aligné, pour un retour au régime déclaratif.\n",
    date_declaration: "2024-09-06",
    latitude: 48.7066273,
    longitude: 2.7754097,
  },
  {
    id: "alizee-vincent",
    noms: "Alizée Quéau et Vincent Guillois",
    enfants: "Noé et Robin",
    departement: "Ille-et-Vilaine",
    presentation:
      "Alizée et Vincent parents de Noé 14ans et Robin 11ans. Ils ont tout les deux connu l'école publique " +
      "et une école alternative mais leurs demandes après le confinement étaient de rester en IEF.\n" +
      "\n" +
      "Nous avons choisi L'IEF depuis plusieurs année pour le bien être de nos enfants, leurs propres " +
      "rythmes à la fois horaires et d'apprentissage, leurs " +
      "découvertes, leurs passions.\n" +
      "\n" +
      "L'apprentissage est plus varié avec bricolage, musiques ( batterie, guitare, trompette...), le soins aux animaux (équidés)... Alizée à travaillé dans une école alternative, on comprends que les enjeux scolaires ne sont pas les mêmes ; l'humain, l'enfant à sa place et est écouté.\n" +
      "\n" +
      "C'est un choix de vie familiale, c'est pourquoi le passage au *régime déclaratif en régime soumis à autorisation, ne nous convient pas. Nous décidons de renter en désobéissance civile, et de rejoindre le collectif Enfance libre.*\n",
    date_declaration: "2024-07-24",
    latitude: 47.7755997,
    longitude: -2.0973292,
  },
  {
    id: "julie-olivier",
    noms: "Julie et Olivier Mengin",
    enfants: "Jules, Camille et Simon",
    departement: "Doubs",
    presentation:
      "Nous avions imaginé notre famille à l’image de celle que nos parents nous ont transmise, une famille dite « traditionnelle ». Or, la vie nous a réservés bien des surprises… Elle a fait de nous et de nos enfants ce que nous sommes aujourd’hui, une famille à l’image de nos valeurs.\n" +
      "De part cette loi, nous nous sentons priver de nos droits parentaux et les besoins de nos enfants non respectés.\n" +
      "L’IEF depuis 2022 et maintenant la Désobéissance civile sont à ce jour vécues comme une évidence, mettant fin à une souffrance psychique. Certes, elles ont bouleversé notre quotidien, notre famille et à quel prix ? Celui du Bonheur d’être ensemble chaque jour que la vie fait !\n",
    date_declaration: "2024-09-30",
    latitude: 47.0524572,
    longitude: 6.3625849,
  },
  {
    id: "tiffany-samuel",
    noms: "Tiffany et Samuel Concaud",
    enfants: "Simon et Amelie",
    departement: "Corrèze",
    presentation:
      "Je m'appelle Tiffany et avec mon mari, Samuel, nous sommes les heureux parents de Simon et Amélie. Notre évolution écologique et notre choix pour devenir parents nous amènent vers l'IEF, pour notre plus grand bonheur de profiter de nos enfants et de les voir grandir, s'épanouir, apprendre avec joie et bienveillance. Notre système éducatif a besoin de changement et nous admirons les différents lieux d'apprentissages qui s'offrent à tous les français. Nous souhaitons garder ce droit et cette liberté pour nos familles, pour nos enfants. Au plaisir de se rencontrer.\n",
    date_declaration: "2024-09-04",
    latitude: 45.2125493,
    longitude: 1.6734343,
  },
  {
    id: "alysee-jeanfrancois",
    noms: "Alysée et Jean-François Blanquet",
    enfants: "Owen et Ewilane",
    departement: "Puy-de-Dôme",
    presentation:
      "Nous sommes la famillesacados63. Nous sommes Alysée et Jean-François, heureux parents d'Owen (9ans) et d'Ewilane (7ans). Nous habitons dans le Puy de Dôme (63) à Landogne. Nous sommes famille d'accueil depuis 5ans de deux petites sœurs (5ans et 2ans). Nous faisons des spectacles avec les enfants en tant que clowns magiciens marionnettiste sous le nom de Jojo et Zaza Compagnie et Jean-François est animateur guide nature patrimoine dans notre belle région. Nous sommes ravis d'intégrer l'équipe des résistants. A bientôt\n",

    date_declaration: "2024-09-12",
    latitude: 45.8861358,
    longitude: 2.6295035,
  },
  {
    id: "camille-adrien",
    noms: "Camille Perrin et Adrien Guillemin",
    enfants: "Sacha et Mael",
    departement: "Haute-Saône",
    presentation:
      "‌Nous sommes Adrien et Camille, parents de Sacha (Février 2018) et Maël (Février 2020), et nous vivons en Haute-Saône,\n" +
      "\n" +
      "Pour nous l'IEF est une évidence, un besoin fondamental d'être les acteurs principaux du développement de nos enfants. C'est pourquoi, après avoir été mère au foyer pendant 3ans, nos activités professionnelles ont évolué. Ainsi aujourd'hui nous sommes une équipe parentale présente au quotidien et quasiment à égalité de temps et d'énergie donnée. Après avoir modifié toute notre vie autour de ce projet commun qu'est l'instruction en famille, il nous semblait évident de ne pas laisser les lois nous prendre notre liberté.\n",
    date_declaration: "2024-09-16",
    latitude: 47.9023133,
    longitude: 6.3423131,
  },
  {
    id: "chloe-ambroise",
    noms: "Chloé Leboucher Bailly et Ambroise Bailly",
    enfants: "Ernest",
    departement: "Côte-d'Or",
    presentation:
      "Nous sommes Chloé & Ambroise, parents d’un enfant de 8 ans en IEF depuis trois ans. Après deux années d’une scolarisation difficile, l’instruction en famille est devenue une évidence pour répondre aux besoins de notre enfant et lui apporter un environnement sécurisant pour apprendre.\n" +
      "\n" +
      "Lors de notre première demande d’autorisation en 2024, nous avons pleinement mesuré le caractère injuste, arbitraire et dangereux du régime d’autorisation préalable.\n" +
      "\n" +
      "Le choix de l’instruction en famille doit pouvoir rester accessible à toutes les familles quelle qu’en soit la raison et quel qu’en soit le moment.\n" +
      "\n" +
      "Le retour au régime déclaratif nous semble la seule voie possible à la protection de la liberté d’instruction et des droits de tous les enfants actuels ou futurs iefeurs !\n",
    date_declaration: "2024-09-13",
    latitude: 47.3319205,
    longitude: 4.9910197,
  },
  {
    id: "vanessa-thierry",
    noms: "Vanessa Reynes et Thierry Montagne",
    enfants: "Lina et Fanny",
    departement: "Haute-Loire",
    presentation:
      "Nous sommes Thierry et Vanessa, les parents de Lina, 12 ans et Fanny, 9 ans. Nous nous sommes lancés dans l'aventure de L'instruction en famille il y a bientôt 4 ans. Nous vivons dans une petite commune de Haute-Loire, proche de notre ferme familiale dans laquelle nos filles se plaisent à s'occuper des animaux. Elles sont heureuses de pouvoir prendre le temps. Prendre le temps de jouer, d'inventer, d'apprendre de la Nature qui nous entoure, d'entreprendre sans stress ni compétition.\n" +
      "\n" +
      "Cette qualité de vie et toutes les richesses qu'apporte l'Instruction en Famille sont précieuses.\n" +
      "\n" +
      "Nous remercions Enfance Libre de nous accueillir et de porter nos voix pour défendre nos libertés d'instruction!\n",
    date_declaration: "2024-09-23",
    latitude: 45.3332344,
    longitude: 3.8160472,
  },
  {
    id: "sophie-berenger",
    noms: "Sophie Barek et Bérenger Cestre",
    enfants: "Charlie et Lilio",
    departement: "Nièvre",
    presentation:
      "Pour nos enfants, nous n'avons pas seulement aménagé nos emploi du temps, nous avons aménagé notre vie, nos ressources, notre espace, nos pensées, nos attentes, notre vision de la parentalité et de la vie en général pour les accueillir et les accompagner de la manière la plus juste pour nous !\n" +
      "Nous les avons fait naître dans notre foyer, nous les avons allaités et portés puis instruits car l'IEF était pour nous la suite logique de notre investissement auprès d'eux.\n" +
      "Nous sommes donc sidérés par cette loi qui voudrait nous enlever ce droit. Pourquoi nous priver d'un mode d'instruction qui fonctionne pour nous quand la loi nous accorde « le choix par priorité du mode d'instruction » (Déclaration universelle des droit de l'Homme) ? Pour que nos enfants\n" +
      "suivent le chemin qui les fera rentrer dans le moule ?\n" +
      "Non. Nous souhaitons nous battre pour que nos enfants aillent à l'école uniquement lorsque l'envie se fera ressentir pour eux, lorsque cela correspondra à un besoin de leur part et non au besoin du dysfonctionnement d'une institution qui ne permettra jamais de respecter les besoins, les rythmes et les particularités de nos enfants encore si jeunes !\n" +
      "Cette année, lors du dépôt des demandes d'autorisation nous nous sommes retrouvés dans l'incapacité de participer à cette mascarade, qui en plus d'être arbitraire de part son application est discriminante envers les non diplômés du BAC (qui ont pourtant jusqu'ici prouvé que la capacité\n" +
      "d'instruire ne dépendait pas d'un diplôme).\n" +
      "L'instruction en famille est un droit fondamental qui ne peut dépendre d'une décision arbitraire, et il est de notre devoir de signaler et d'agir contre des lois liberticides.\n" +
      "C'est pourquoi nous rejoignons les résistants d'enfance libre.\n",
    date_declaration: "2024-09-18",
    latitude: 47.241562,
    longitude: 3.6616105,
  },
  {
    id: "marine-florian",
    noms: "Marine et Florian Guillot",
    enfants: "Talya et Léya",
    departement: "Jura",
    presentation:
      "On s'appelle Marine et Florian, parents de deux merveilleuses filles, Talya et Leya,  7 ans et 4 ans. Cela fait déjà plus de 4 ans que nous pratiquons l'IEF au quotidien.\n" +
      "C'est un pilier central de notre vie de famille et de notre éducation qui nous tient à cœur de défendre.\n" +
      "Nous sommes donc ravis de pouvoir intégrer le mouvement Enfance Libre qui résonne profondément avec nos valeurs et convictions.\n",
    date_declaration: "2024-09-26",
    latitude: 46.5822045,
    longitude: 5.4820834,
  },
  {
    id: "audrey-jonathan",
    noms: "Audrey Eveque-Mourroux et Jonathan Scharschmidt",
    enfants: "Naoki et Adriel",
    departement: "Savoie",
    presentation:
      "C’était pour nous une évidence que nos enfants seraient instruits en famille.\n" +
      "Ainsi quand Naoki a pointé le bout de son nez en 2018, nous nous sommes renseignés sur les pédagogies alternatives et Audrey s’est formée à la pédagogie de Maria Montessori. Puis Adriel est né en 2020 et il a grandi dans cet environnement. Ils n’ont jamais été scolarisés.\n" +
      "Le fait d’accompagner et de vivre au rythme et à l’écoute de nos enfants, dans un cadre clair, nous invite également à être encore plus vrai dans le respect de chacun. Et aujourd’hui que l’administration nous refuse l’autorisation d’être responsable du développement physique, psychique, sensoriel et émotionnel de nos enfants, nous comprenons que ce ne sont pas seulement nos enfants qui sont pénalisés mais bien l’avenir de notre nation qui est remis en cause.\n" +
      "Nous sommes donc fiers de mener cette action à vos côtés.\n",
    date_declaration: "2024-09-27",
    latitude: 45.5090172,
    longitude: 6.2625772,
  },
  {
    id: "gwendoline-julien",
    noms: "Gwendoline et Julien Voltzenlogel",
    enfants: "Logan",
    departement: "Bas-Rhin",
    presentation:
      "Nous avons 7 enfants, tous non pas suivis un parcours en instruction en famille , celui de 11 ans est reparti au collège.\n" +
      "Mais nous souhaitons, pour les plus jeunes leur donner la chance de vivre l’ instruction en famille .\n" +
      "Leur permettre d’ avoir le choix, d’ aller à l’ école ou non .\n" +
      "Comment des personnes ne connaissant pas nos enfants pourraient choisir à notre place ce qui est le mieux pour eux, à partir du moment ou une bonne instruction est donnée ?\n" +
      "Aujourd’hui nous entrons en désobéissance civile, non pas juste pour notre fils concerné cette année, mais aussi pour tout nos enfants, pour tout les enfants de France, qui un jour aurai besoin ou envie de faire l’ instruction en famille.\n",
    date_declaration: "2024-09-25",
    latitude: 48.7566975,
    longitude: 7.893178,
  },
  {
    id: "sophie-jeremy",
    noms: "Sophie Gomez-Merisier et Jeremy Gomez",
    enfants: "Yelena et Esteban",
    departement: "Bouches-du-Rhône",
    presentation:
      "Nous sommes Sophie et Jérémy et nous entamons notre 5 eme année en \n" +
      "instruction en famille pour nos enfants Yelena 7 ans et Esteban 5 ans.\n" +
      "\n" +
      "Après 3 années à s’être soumis au régime d’autorisation pour notre fils  et \n" +
      "après avoir reçu des refus cette année pour nos 2 enfants, nous avons \n" +
      "enfin ouvert les yeux et décidé de nous battre pour préserver cette \n" +
      "liberté.\n" +
      "\n" +
      "L’instruction en famille nous permet de donner la chose la plus précieuse au monde à nos enfants: le temps !\n" +
      "\n" +
      "Nos enfants sont heureux, épanouis, instruits, socialisés, sociables et désirent poursuivre l’instruction en famille.\n" +
      "\n" +
      "C’est pour cela que nous défendrons la liberté  d’instruction, leur liberté \n" +
      "en se mettant en désobéissance civile et en devenant résistant auprès \n" +
      "d’enfance libre !\n",
    date_declaration: "2024-09-27",
    latitude: 43.5268879,
    longitude: 4.7899342,
  },
  {
    id: "priscilla-nicolas",
    noms: "Priscilla et Nicolas Bocchino",
    enfants: "Nathaniel",
    departement: "Creuse",
    presentation:
      "Nous sommes Nicolas et Priscilla, parents d’une jeune personne qui à presque 10 ans a eu la chance de ne jamais aller à l’école et de pouvoir *“*apprendre par lui-même, avec les autres et dans le monde*”*.\n" +
      "\n" +
      "Plus qu’un simple choix de mode d’instruction, c’est pour nous un choix de vie, qui s’est présenté à nous comme la suite naturelle de notre investissement parental auprès de notre enfant depuis sa naissance. Toute notre vie a été organisée et pensée autour de ce choix, certes atypique, mais qui nous correspond à tous les trois.\n" +
      "\n" +
      "Au fil des années, nous avons vu avec effarement l’évolution des lois, toujours plus restrictives, toujours plus pesantes pour les familles, et totalement déconnectées des besoins des enfants et de la réalité des familles : abaissement de l’âge obligatoire de scolarisation de 6 à 3 ans, exercices rendus obligatoires lors des contrôles, durcissement des contrôles, et enfin mise en place du régime d’autorisation.\n" +
      "\n" +
      "Des lois toujours plus lourdes, à l’application totalement arbitraire et injuste, qui font porter un poids et un stress incroyable aux familles et aux enfants, et qui aujourd’hui placent des familles dans une grande détresse en les forçant à rescolariser leur enfant ou en empêchant de sortir un enfant d’un milieu qui lui est toxique. Bien loin du respect des droits des enfants et de leur intérêt …\n" +
      "\n" +
      "Aujourd’hui, de par cette loi, des personnes qui ne connaissent rien de notre fils ni de notre vie ont décrété qu’il faudrait qu’il soit scolarisé, et ce malgré l’évidence que l’ief lui réussit et l’épanouit, alors que l’école le mettrait obligatoirement en échec au vu de ses particularités.\n" +
      "\n" +
      "En tant que parents, notre rôle premier est de protéger notre enfant, de faire au mieux\n" +
      "pour lui et de le respecter dans ses besoins et son individualité. I est donc hors de question pour nous de laisser une administration nier notre rôle et décider à notre place et la sienne de ce qui serait le mieux pour lui, et c’est pourquoi nous avons choisi d’entrer en désobéissance civile, pour le préserver mais aussi pour toutes les familles actuellement en détresse à cause de cette loi inique, et tous les enfants actuellement en souffrance à l’école et qu’il n’est désormais plus possible de déscolariser le temps de les laisser souffler …\n",
    date_declaration: "2024-09-11",
    latitude: 46.143431,
    longitude: 1.4351656,
  },
  {
    id: "angelique-patrick",
    noms: "Angélique Piantoni-Fleurot et Patrick Piantoni",
    enfants: "Mila et Joseph",
    departement: "Haute-Saône",
    presentation:
      "Nous sommes Patrick et Angélique, parents de deux enfants de 11 et 6 ans. Notre ainée a choisi de reprendre le chemin de l’école cette année. Notre cadet quant à lui poursuit son instruction à nos cotés.\n" +
      "L’instruction en famille s’est imposé à nous à l’approche des trois ans de notre ainée. Il nous semblait prématuré de la scolariser, et nous avions tant de choses à vivre et partager avec elle.\n" +
      "Elle a intégrer l’école en Cp et Ce1. Sa scolarisation fut difficile au niveau émotionnel pour elle.\n" +
      "Lorsque son frère a eu trois ans, nous avons finalement tous repris le chemin de l’IEF. C’est un mode d’instruction mais aussi de vie, parfois difficile mais tellement riche, et qui nous correspond pleinement.\n" +
      "Nous voulions leur offrir du temps. Du temps ensemble, du temps pour jouer, du temps pour s’instruire à leurs rythmes, du temps pour vivre. Nous souhaitions également leur permettre d’être acteur de leur instruction, leur transmettre le goût d’apprendre, cultiver en eux la curiosité et l’ouverture au monde, et qu’ils aient conscience que ce n’est finalement que pour eux qu’ils le font!\n" +
      "Nous rejoignons Enfance Libre pour défendre un droit fondamental, celui des parents de pouvoir choisir l’éducation la plus adaptée aux besoins de leurs enfants. L’administration ne peut se substituer aux parents, qui sont les plus à même de connaitre leurs enfants!\n",
    date_declaration: "2024-10-03",
    latitude: 47.8925405,
    longitude: 6.3789664,
  },
  {
    id: "florence-jacques",
    noms: "Florence Duchemin et Jacques Renaud",
    enfants: "Luna",
    departement: "Lot",
    presentation:
      "L’éveil de l’enfant est une grande richesse pour le pays. Chaque Être porte une étincelle et nul n’a le droit de l’étouffer, l’éteindre, la restreindre, notre accompagnement est de la rendre étincelante.\n" +
      "Que tout enfant sur terre puisse choisir le mode d’instruction qui lui convient et puisse garder foi en la vie à l’abri de l’amour, en reconnaissant qui il est, et combien ce qu’il aime Être et faire, peut servir le monde.\n" +
      "Restons confiant, c’est merveilleux ce qui s’en vient.\n" +
      "Soyons en gratitude quant à cette opportunité que l’IEF soit connue et reconnue du grand public.\n" +
      "L’instruction est obligatoire, l’école quant à elle, ne l’est pas.\n" +
      "Dès la conception de l’enfant, nous nous sommes intéressés de près à ses besoins et comment nous pouvions honorer la mission de parents qui nous était ainsi allouée.\n" +
      "Nous nous sommes passionnés pour l’allaitement, le portage, l’alimentation, l’écoute et la compréhension de ses besoins fondamentaux, la découverte du monde des émotions, l’impact de nos choix dans son développement. Quant est venue la question de l’éducation, il nous a semblé complètement naturel de poursuivre ce que nous avions commencer, être à ses cotés, l’accompagner dans ses apprentissages et donc choisir l’instruction en famille, pour lui tendre la main sur son chemin vers sa propre autonomie.\n" +
      "Ce mode d’apprentissage se fond complètement avec notre projet de vie, notre projet social et éducatif.\n" +
      "Nous nous sommes intéressés aux diverses pédagogies comme Steiner et Montessori. Elles sont pourtant presque contraires, les fusionner, fut très enrichissant, ça donne du Montsteiner et c’est très intéressant. L’approche de Céline Alvarez est aussi mère veilleuse:) et nous a beaucoup guidé.\n" +
      "Être dans son êtreté, un chemin possible.\n" +
      "Que lumière soit ! Viva l’IEF\n",
    date_declaration: "2024-10-02",
    latitude: 44.3944877,
    longitude: 1.3306421,
  },
  {
    id: "laetitia-olivier",
    noms: "Laëtitia Boivin et Olivier Saget",
    enfants: "Similan, Imanol, Anjali et Inaya",
    departement: "Nièvre",
    presentation:
      "Bonjour, nous somme Laëtitia, formatrice et Olivier, musicien, \n" +
      "parents dans une famille recomposée de 4 enfants qui vivent l’ief depuis\n" +
      " toujours… C’est un vrai choix de vie auquel on ne compte pas renoncer!\n" +
      "\n" +
      "Notre ief évolue au fil des envies et des années, on aime proposer \n" +
      "des expériences concrètes et de la manipulation ce qui ravi les enfants!\n" +
      " Malgré que l’on ait fait très peu de formel avant le “niveau collège” \n" +
      "les deux ainé·es de la famille on voulu passer leur brevet en candidat·e libre et l’ont obtenu en juillet 2024 à l’âge de 15 et 13 ans.\n" +
      "\n" +
      "On aime prendre le temps de vivre ensemble et voyager en famille.\n" +
      "\n" +
      "S. l’aînée de 16 ans bientôt, a choisi de rejoindre le lycée après toutes ces années en ief.. elle semble s’y épanouir!\n" +
      "\n" +
      "I.13ans a rejoint les étudiants en ligne de ClonLaraSchool en vue de passer son high school diploma.\n" +
      "\n" +
      "Il nous est impossible de se résoudre à scolariser nos plus jeunes sans leur consentement.\n" +
      "\n" +
      "Elles sont ravies de vivre l’ief comme elles l’ont toujours vu pour les grand·es.\n" +
      "\n" +
      "Après avoir profiter d’un “rab” avec le plein droit accordé aux \n" +
      "familles déjà en ief, nous avons fait notre première demande à contre \n" +
      "coeur, les refus ont été rapide et sans appel. Et ce malgré les très \n" +
      "nombreux rapports positifs cumulés au fils des années. Il nous a fallu \n" +
      "un peu de temps pour cheminer dans cette idée de passer en désobeissance\n" +
      " civile mais nous sommes maintenant convaincu·es que c’est la voie la plus juste et alignée à nos valeurs.\n" +
      "\n" +
      "Il nous semble essentiel défendre cette précieuse liberté \n" +
      "d'instruction et avons décidé d'entrer en désobéissance civile face à la\n" +
      " loi qui restreint ce droit. Notre objectif est de continuer à offrir à \n" +
      "nos enfants un environnement d'apprentissage riche, adapté à leur rythme\n" +
      " et à leurs intérêts, dans la joie et l’amour.\n",
    date_declaration: "2024-10-08",
    latitude: 47.057819,
    longitude: 3.488971,
  },
  {
    id: "gwendoline-lutz",
    noms: "Gwendoline Loterie et Lutz Wielgosch",
    enfants: "Tizian et Anouk",
    departement: "Haut-Rhin",
    presentation:
      "Papa allemand, maman française, nous communiquons entre nous en anglais et avec nos enfants de 5 et 7ans en allemand et français. Pour nous l'IEF est une évidence afin de pouvoir instruire nos enfants dans les 3 langues dès leur plus jeune âge et offrir un maximum d'activités au quotidien.\n" +
      "\n" +
      "Nous faisons ce que l'école n'est pas capable de faire.\n" +
      "\n" +
      "Se plier aux exigences de l'académie et n'avoir en retour que des refus distribués arbitrairement ou de façon à suivre des directives illégales n'est plus en cohérence avec nos valeurs.\n" +
      "\n" +
      'La décision de scolariser ou non nos enfants est une décision qui nous appartient à 100% et la désobéissance civile avec enfance libre nous permet d\'entrer dans une "famille" qui partage les mêmes valeurs que nous.\n',
    date_declaration: "2024-10-04",
    latitude: 47.6305206,
    longitude: 7.4475991,
  },
  {
    id: "maud-anthony",
    noms: "Maud Alix-Leneveu et Anthony Alix",
    enfants: "Louis, Margot, Lucie et Emma",
    departement: "Corrèze",
    presentation:
      "Nous sommes Maud et Anthony parents de Louis (2017), Margot(2019), Lucie (2021) et Emma(2024). Avant même l’arrivée de notre ainé, il était évident pour nous de faire l’IEF, nos choix et finalement toute notre vie émanent de cette volonté.\n" +
      "A la sortie de cet article 49, nous voulions entrer en désobéissance mais par méconnaissance et par peur nous avons fait le choix de nous plier aux exigences de l’administration.  Après deux ans de plein droit pour Louis, de demandes pour Margot (autorisées), nous avons constitué des dossiers pour les trois en 2024, Lucie ayant 3 ans. Une perte de temps et d’énergie car, comme nous nous en doutions, les demandes ont abouti à des refus.\n" +
      "Nous avons tenté les RAPO et les recours au TA mais nous nous rendons à l’évidence que les dés sont pipés et que de toute façon si nous avions gain de cause cela ne serait que du sursis et que le combat doit être mené de front.\n" +
      "Nous sommes donc déterminés plus que jamais à faire valoir le droit de nos enfants à vivre leur enfance de la manière qui leur convient le mieux et de pourvoir avoir le choix. Revenir au système déclaratif est primordial pour la sauvegarde de notre liberté à tous.\n",
    date_declaration: "2024-09-27",
    latitude: 45.4054051,
    longitude: 1.840081,
  },
  {
    id: "aziliz-julien",
    noms: "Aziliz et Julien Musacchia Infante",
    enfants: "Lyana et Aëlan",
    departement: "Seine-Saint-Denis",
    presentation:
      "Depuis toujours, notre fille aînée a bénéficié de l'instruction en famille, et nous avons toujours respecté nos engagements, en acceptant les visites de l'académie qui ont systématiquement confirmé le bien-fondé de notre démarche à travers des contrôles positifs.\n" +
      "Pour la première fois, nous avons fait une demande d’autorisation pour notre fils, qui a été arbitrairement refusée, comme pour de nombreux autres enfants depuis l’adoption de la loi liberticide restreignant l’instruction en famille.\n" +
      "Nous sommes profondément indignés de voir les familles pratiquant l'IEF injustement assimilées à des groupes séparatistes, alors que notre seul désir est de permettre à nos enfants d’apprendre dans un climat de joie, de partage et de respect.\n" +
      "Nous réclamons le retour au régime déclaratif et une prise en compte réelle de l'intérêt supérieur de l'enfant.\n" +
      "Nous défendons le droit à ce que l’école soit un choix parmi d’autres, tout comme l’instruction en famille, et nous nous opposons fermement à la scolarisation forcée de nos enfants.\n" +
      "Laissez nos enfants grandir à leur propre rythme, dans un cadre familial bienveillant, respectueux de leur individualité et de leurs besoins.\n",
    date_declaration: "2024-10-09",
    latitude: 48.871075,
    longitude: 2.472271,
  },
  {
    id: "ariel-maxime",
    noms: "Ariel Eyraud et Maxime Mosdale",
    enfants: "William et Izia",
    departement: "Hautes-Alpes",
    presentation:
      "Nous sommes une famille de 4.\n" +
      "Notre aîné attaque sa deuxième année en IEF et il s’épanouit dans cette forme d’instruction.\n" +
      "Notre vie s’organise pour grandir ensemble autour de ce projet.\n" +
      "Nous souhaitons agir pour défendre la liberté d’instruction pour tous.tes.\n" +
      "\n" +
      "Ariel Maxime William et Izia.\n",
    date_declaration: "2024-10-08",
    latitude: 44.677895,
    longitude: 6.067536,
  },
  {
    id: "sandrine-julien",
    noms: "Sandrine Guido & Julien Junique",
    enfants: "Sullivan et Célestin",
    departement: "Var",
    presentation:
      "Bonjour\n" +
      "Sandrine et Julien parents en ief depuis toujours. Nous avons deux enfants 6 ans et 1 an et demi. Notre grand souhaite garder sa liberté de ne pas aller à l'école. Nous devenons résistant.e.s en tant que parents avec le cœur afin de préserver nos libertés de choix. Nous sommes heureux que ce collectif d'enfance libre soit la pour se sentir en lien. Nous demandons un retour à un régime déclaratif.\n" +
      "Au plaisir d'échanger et de se rencontrer\n" +
      "Sandrine , Julien, Sullivan, Célestin\n",
    date_declaration: "2024-09-12",
    latitude: 43.6027429,
    longitude: 5.6515944,
  },
  {
    id: "marjorie-samuel",
    noms: "Marjorie Plou & Samuel Pozuelo",
    enfants: "Louise Anatole, Zoé et Mirabelle",
    departement: "Sarthe",
    presentation:
      "Bonjour, nous sommes Samuel et Marjorie, parents de Louise, Anatole, Zoé et Mirabelle; Nous pensons que les enfants viennent au monde pour vivre leur vie avec nous par amour. C'est donc pour, et avec amour, que nous voulons vivre avec eux, les protéger et leur donner toute l'instruction dont ils auront besoin pour devenir des êtres épanouis dans la société.Nous n'imaginons pas que l'amour puisse être imposé et sommes certains que les enfants savent ce qu'ils ont besoin,ou envie, d'apprendre. Nous nous efforçons d'y être attentif, puis, de nous modeler pour y répondre plutôt que d’essayer de les modeler eux.\n",
    date_declaration: "2024-10-05",
    latitude: 47.7939869,
    longitude: 0.3671325,
  },
  {
    id: "manon-klarence",
    noms: "Manon Soares & Klarence Goue",
    enfants: "Nelson et Camilia",
    departement: "Côte-d'Or",
    presentation:
      "Nous sommes Klarence et Manon, heureux parents de Nelson (6 ans) et Camilia (bientôt 3 ans), et nous résidons à Dijon. Il y a maintenant trois ans, nous avons décidé de nous lancer dans l'instruction en famille (IEF), un choix qui, au départ, était motivé par l'envie de faire la maternelle à la maison pour Nelson.\n" +
      "\n" +
      "Ce qui a commencé comme une simple expérience est rapidement devenu bien plus qu'un projet éducatif temporaire. Aujourd'hui, c'est un véritable choix de vie pour notre famille. Nelson entame maintenant sa 4ème année en IEF, et nous avons récemment commencé cette aventure avec Camilia, qui vient tout juste d'entamer sa première année.\n",
    date_declaration: "2024-09-25",
    latitude: 47.3321557,
    longitude: 4.9498981,
  },
  {
    id: "emilie-nicolas",
    noms: "Emilie Pasty et Nicolas Gilles",
    enfants: "Cody et Andy",
    departement: "Alpes-Maritimes",
    presentation:
      "Nous sommes les parents de deux garçons ayant toujours été instruits en famille et qui ne souhaitent pas être scolarisés pour l’instant.\n" +
      "Bien que nous soyons totalement contre cette nouvelle loi qui nous oblige à demander une autorisation pour instruire nos enfants en famille, nous avons tout de même tenté de nous conformer, croyant en l’équité de notre académie.\n" +
      "Cependant, ce ne fut pas le cas. Nous avons essuyé des refus pour notre fils aîné, que ce soit pour notre demande d’autorisation, le recours auprès de l’académie ou le tribunal administratif, malgré des contrôles pédagogiques très positifs pour lui comme pour son petit\n" +
      "frère pour qui nous avons eu l’autorisation de continuer l’IEF.\n" +
      "Plutôt que de consacrer notre énergie à constituer des dossiers traités de manière inégale et injuste, nous préférons la consacrer désormais au combat pour le retour à une déclaration\n" +
      "simple pour l'instruction en famille, aux côtés d’Enfance Libre.\n" +
      "Nous entrons donc en désobéissance civile et ne demanderons plus l’autorisation pour instruire nos enfants tant qu’ils feront ce choix.\n",
    date_declaration: "2024-10-09",
    latitude: 43.5931117,
    longitude: 6.9634671,
  },
  {
    id: "elodie-guillaume",
    noms: "Elodie Caduys et Guillaume Fradet",
    enfants: "Mathis et Noah",
    departement: "Lot-et-Garonne",
    presentation:
      "Nous sommes Elodie et Guillaume, parents de Mathis (6 ans) et Noah (3 ans).\n" +
      "Très tôt dans notre projet familial, nous nous sommes interrogés sur l’éducation, les valeurs et de fait les choix d’instruction que nous souhaitions apporter à nos enfants.\n" +
      "Mathis a fait le choix de l’école à son entrée en petite section de maternelle, un choix qui s’est finalement avéré décevant et douloureux pour lui. L’instruction en famille est donc devenue une évidence, pour le respect de ses besoins physiques, ainsi que sa sécurité émotionnelle et psychologique.\n" +
      "Après 3 ans d’IEF sanctionnés de résultats positifs aux évaluations, nous avons formulé des demandes d’autorisation en présentant notre projet éducatif, qui jusque-là avait toujours satisfait les contrôles pédagogiques.\n" +
      "Il nous a été refusé d’instruire nos enfants en famille, pour des raisons incompréhensibles. En tant que citoyens éclairés nous considérons qu’il est de notre devoir de faire barrage.\n" +
      "La loi, tout comme sa mise en application, montre que l’objectif du gouvernement est davantage de respecter des quotas que de lutter contre le séparatisme. Maintenant, permettre à nos enfants de bénéficier du projet pédagogique le plus adapté à leurs besoins, n’est plus un droit.\n" +
      "La Désobéissance Civile nous apparaît alors comme la démarche la plus évidente à adopter pour revendiquer un retour au régime déclaratif.\n",
    date_declaration: "2024-10-10",
    latitude: 44.44034,
    longitude: 0.928,
  },
  {
    id: "fanny-nicolas",
    noms: "Fanny Gavila et Nicolas Velin",
    enfants: "Paul et Lucie",
    departement: "Bouches-du-Rhône",
    presentation:
      "Nous sommes une famille de 4, avec un enfant en IEF depuis 1 an. Nous sommes allés jusqu’au jugement de fond du tribunal et avons eu, de nouveau, un refus. \n" +
      "Nous souhaitons instruire notre enfant pour lui offrir le meilleur, pour qu’il puisse apprendre à son rythme, dans un environnement où il aura toute la sécurité affective dont il a besoin pour se développer et grandir. Nous ne pouvons pas compromettre son bien-être. Nous allons donc continuer de l’instruire en famille, et nous luttons comme nous le pouvons contre cette loi, en tant que désobéissants.\n",
    date_declaration: "2024-10-15",
    latitude: 43.59512,
    longitude: 5.49913,
  },
  {
    id: "sabrina-aurélien",
    noms: "Sabrina Mancon et Aurélien Engels",
    enfants: "Romy et Ava",
    departement: "Pyrénées-Orientales",
    presentation:
      "Nous sommes Sabrina et Aurélien, parents de Romy 3 ans et Ava 2 ans.\n" +
      "\n" +
      "Depuis la naissance de notre première, nous avons voué notre vie à nos enfants en leur apportant le meilleur pour elles.\n" +
      "\n" +
      "Et c'est naturellement que nous avons su que l'instruction en famille ferait partie intégrante de nos vie.\n" +
      "\n" +
      "Nous sommes nomades et il est insensé pour nous de scolariser nos enfants au gré de nos déplacements.\n" +
      "\n" +
      "Suite au refus de notre demande d'instruction en famille, nous avons rejoint le collectif Enfance Libre. Nous nous battrons pour nos droits et ceux de nos enfants, et pour un retour au régime déclaratif.\n",
    date_declaration: "2024-10-14",
    latitude: 42.6906233,
    longitude: 2.8169171,
  },
  {
    id: "eve-gonzalo",
    noms: "Eve Brûnel et Gonzalo Diaz",
    enfants: "Esteban",
    departement: "Seine-et-Marne",
    presentation:
      "Nous sommes Eve et Gonzalo, les parents d’Esteban qui a toujours été instruit en famille. Suite à la fin du plein droit, nous avons fait une demande d’autorisation qui a été refusée. Nous souhaitons malgré tout poursuivre ce mode d’instruction, ce choix de vie et que toutes les familles qui le souhaitent puissent en faire autant.\n",
    date_declaration: "2024-10-12",
    latitude: 48.4800459,
    longitude: 2.6639681,
  },
  {
    id: "lucie-guillaume",
    noms: "Lucie et Guillaume Le Naour",
    enfants: "Elisabeth, Cyrille, Grégoire et Salomé",
    departement: "Finistère",
    presentation:
      "Nous sommes Lucie est Guillaume parents d’une famille avec 4 enfants.\n" +
      "Notre aînée de 11 ans est scolarisé en 6e au Collège.\n" +
      "Nos trois plus jeunes enfants, âgés de 9 ans, 6 ans et 4 ans, sont instruits en famille. Nous avons débuté l’instruction en famille en 2020 en profitant du congé parental de Lucie (inspiré par l'exemple du frère et de la belle-sœur de Lucie qui ont instruits leurs enfants pendant plusieurs années) et avons poursuivi jusqu'à présent.\n" +
      "L'instruction est essentiellement réalisé par Lucie. Nous avons actuellement trouvé un équilibre entre vie de famille rythmée par l’IEF et travail.\n" +
      "Les enfants peuvent ainsi apprendre à leur rythme. Ils profitent de la nature (activité Forest School), des sorties avec d'autres familles en IEF, des activités sportives, de temps de jeux libres.\n" +
      "Nous avons initialement rempli les dossiers de demande pour lesquels les retours ont été négatifs, les RAPO également et avons finalement décidé de partir en Désobéissance Civile devant l'absence totale d'écoute et les refus qui nous paraissent injustifiés. Cette décision est également motivée par notre souhait d’écouter nos enfants qui ont exprimé leur désir de rester en instruction en famille.\n" +
      "Nous plaidons pour un retour au régime déclaratif.\n",
    date_declaration: "2024-09-20",
    latitude: 48.435686,
    longitude: -4.468748,
  },
  {
    id: "lydia-carnevali",
    noms: "Lydia Carnevali",
    enfants: "Paul",
    departement: "Rhône",
    presentation:
      "Nous sommes une famille monoparentale installée à Lyon.\n" +
      "L’IEF est apparue dans notre vie en 2015 pour mon second fils (en 5ème à ce moment) avec une première expérience liée à de la violence scolaire.\n" +
      "Cette première courte expérience de 8 mois a sans doute été décisive dans l’apparition de nouvelles croyances, et de nouveaux espaces de liberté de choix.\n" +
      "En 2021 alors que mon troisième fils fréquente une école alternative je prends conscience que la liberté d’instruire est mise à mal.\n" +
      "Après discussion familiale, nous décidons de « reprendre » l’aventure en IEF en janvier 2021, et la décision de ne pas se soumettre à ce nouveau schéma est prise: nous ne ferons pas de demande.\n" +
      "Aujourd’hui nous nous sentons libres, profondément conscients de ce choix de désobéir. D’autant plus que j’étais fonctionnaire d’Etat pendant 15 ans, dont 10 comme officier dans l’Armée de terre et 5 dans l’Education nationale.\n",
    date_declaration: "2024-10-25",
    latitude: 45.748509,
    longitude: 4.786714,
  },
  {
    id: "iris-emilien",
    noms: "Iris Assfalg et Emilien Bontz",
    enfants: "Atlas et Orion",
    departement: "Vosges",
    presentation:
      "Nous sommes Iris et Emilien, parents de deux garçons, Atlas (5 ans) et Orion (2 ans). Nous avons choisi l'instruction en famille pour notre aîné, Atlas, car nous estimons que cette approche correspond le mieux à ses intérêts et à nos convictions.\n" +
      "Après deux années durant lesquelles nous avons effectué l'instruction en famille et reçu des contrôles très positifs, nous avons contre toute attente, et malheureusement, vu notre demande refusée cette année et ce sans réelle raison.\n" +
      "Ce refus du RAPO nous a conduits à prendre la décision de recourir à la désobéissance, convaincus que la meilleure réponse à cette injustice réside dans l'affirmation de nos choix. Nous espérons ainsi que nos voix seront mieux entendues.\n" +
      "Par ailleurs, nous souhaitons également poursuivre l’instruction en famille avec notre deuxième fils et continuer à partager des moments de bonheur, de curiosité et d'épanouissement avec nos enfants.\n",
    date_declaration: "2024-10-25",
    latitude: 48.505543,
    longitude: 7.0861311,
  },
  {
    id: "sylvie-frederick",
    noms: "Sylvie Cos et Frédérick Monfort",
    enfants: "Fantine",
    departement: "Ille-et-Vilaine",
    presentation:
      "Nous sommes Fantine, Frédérick et Sylvie.\n" +
      "Nous connaissions l’IEF sans penser un jour la vivre. \n" +
      "Puis, un événement nous a fait opter pour ce mode d’instruction. Fantine a retrouvé son enthousiasme et son goût de la création, notre famille entière a bénéficié de cette façon de vivre.\n" +
      "Nous mettons en pratique des notions que nous pensons primordiales: le respect des besoins physiologiques, se construire et grandir dans l’écoute de chacun, privilégier un mode de vie qui favorise la coopération , passer du temps de qualité ensemble.\n" +
      "Nous avons pu prendre ce chemin aisément, à l’époque, une déclaration suffisait pour changer de mode d’instruction. Le choix du mode d’instruction de la cellule familiale était respecté.\n" +
      "Nous voulons que le choix redevienne possible, pour nous et toute famille désirant vivre l’IEF. Que cela reste une décision familiale et non pas un parcours semé d’embûches générant anxiété et incertitudes, puisqu‘il n y a pas de cohérence ou de logique dans les décisions rendues par les différentes académies.\n" +
      "D’ailleurs, en septembre 2024, Caroline Pascal, directrice générale de l’enseignement scolaire, reconnaît, sans ambages, que l’objectif de cette loi était de réduire le nombre d’enfants bénéficiant de l’IEF.\n" +
      "Mais où se situe alors le bien-être supérieur de l’enfant ? Combien d’enfants, de familles en souffrance de part cette obligation ? Ce que nous sentions est reconnu clairement, sans faux semblants : cette loi génère des injustices, elle n’est pas là pour protéger l’enfant. Elle répond à une volonté de faire baisser le nombre de familles en IEF. Nous ne sommes pas des données dans un tableau, cette façon de procéder n’est pas digne d’un État de droit !\n" +
      "Nous sommes donc heureux aujourd’hui de rejoindre le collectif Enfance Libre.\n" +
      "Nous souhaitons nous rendre visibles pour dénoncer cette loi arbitraire et revenir à un régime déclaratif.\n",
    date_declaration: "2024-10-19",
    latitude: 48.626055,
    longitude: -1.891682,
  },
  {
    id: "maryam-philippe",
    noms: "Maryam Esmaeil Peykani et Philippe Graff",
    enfants: "Nils",
    departement: "Moselle",
    presentation:
      "Nous sommes Maryam et Philippe, parents de Nils (10 ans).\n" +
      "Nous avons commencé l'instruction à Nils il y a 4 ans et demi. Nous avons respecté la loi, fait nos déclarations et nos demandes d'autorisations en nous soumettant au différents contrôles imposés sans qu'aucun problème ne nous soit signalé. Cette année, l'autorisation nous a été refusé et après avoir usé des recours possibles, nous avons inscrit Nils dans un établissement scolaire.  Nils a mal vécu cette expérience et en accord avec lui, nous avons pris la décision de nous mettre en désobéissance civile. Cette expérience nous a montré combien notre fils se sent épanoui avec l'IEF. La connivence avec laquelle nous sommes tous les 3 unis est un bien inestimable, les joies que nous pouvons partager ne sont pas possibles avec une scolarité en établissement classique. Nous nous battons pour que nos droits et ceux de notre fils soient restaurés.\n",
    date_declaration: "2024-11-02",
    latitude: 49.093824,
    longitude: 6.146939,
  },
  {
    id: "frederique-pascal",
    noms: "Frédérique Stocker et Pascal Noël",
    enfants: "Jacks et Tyler",
    departement: "Moselle",
    presentation:
      "Nous sommes Frédérique et Pascal, parents de Jacks (8 ans) et Tyler (6 ans) et pratiquons l’Ief pour notre 6ème année.\n" +
      "Jusqu'à présent, tout allait bien, les contrôles étaient positifs, nous avions eu l'autorisation de plein droit pour les deux dernières années et paf, plus le droit.\n" +
      "Nos vies et le travail fourni par les enfants nous confirment pourtant que c'est la meilleure option pour nous 4 et nous avons pris la décision d'entrer en désobéissance civile pour continuer à avancer en accord avec nos choix.\n" +
      "Nous nous battrons pour revenir à un régime déclaratif, pour que nous parents instructeurs et enfants épanouis dans ce mode de vie puissions être respectés et pas suspectés.\n",
    date_declaration: "2024-10-21",
    latitude: 49.084766,
    longitude: 6.15752,
  },
  {
    id: "sabrina-cedric",
    noms: "Sabrina Cayon et Cédric Saillant",
    enfants: "Ambrine, Naoki et Seth",
    departement: "Allier",
    presentation:
      "Nous sommes une famille avec 3 enfants de 8, 5 et 3 ans, en instruction en famille depuis toujours par conviction. Nous habitons dans l'Allier où nous profitons de l'espace et des animaux. Nous cultivons notre potager en permaculture et profitons de la vie et de ces enseignements.\n",
    date_declaration: "2024-11-11",
    latitude: 45.9828,
    longitude: 3.64137,
  },
  {
    id: "pauline-sven",
    noms: "Pauline Roux et Sven Léger",
    enfants: "Anna, Enzo",
    departement: "Seine-et-Marne",
    presentation:
      "Nos deux enfants, Anna (11 ans) et Enzo (8 ans), ont toujours été instruits en famille.\n" +
      "Nous nous sommes lancés dans cette aventure alors que le régime était encore déclaratif, ayant découvert cette possibilité un peu par hasard. Nous n'avions alors pas d'objectif précis, mais cette perspective nous parlait davantage que d'envoyer notre fille à l'école alors qu'elle n'avait pas tout à fait 3 ans. Aujourd'hui, l'instruction en famille est au cœur de notre mode de vie. Une évidence, tout simplement.\n" +
      "En ce qui concerne notre cheminement jusqu'à la désobéissance civile et notre adhésion à Enfance libre, nos premières demandes d'autorisation d'instruction en famille formulées pour l'année scolaire 2024 / 2025 ont été refusées comme tant d'autres avant nous. Pas vraiment une surprise, mais un coup dur malgré tout. Nous avions décidé d'aller au bout des recours gratuits et avons donc tenté notre chance au tribunal.\n" +
      "Après plusieurs mois extrêmement éprouvants et un long dialogue de sourds, nous avons fini par perdre. Mais cela ne changera rien (ou presque) : nous continuerons à instruire nos enfants comme nous l'avons fait jusque-là, parce que c'est la meilleure option pour eux, quoi qu'on nous dise.\n",
    date_declaration: "2024-10-01",
    latitude: 48.87919,
    longitude: 2.76667,
  },
  {
    id: "lucille-damien",
    noms: "Lucille et Damien Monnier Hogrel",
    enfants: "Léandre en IEF, 2 aînés scolarisés",
    departement: "Loire-Atlantique",
    presentation:
      "Nous sommes Damien et Lucille, parents de quatre enfants, dont Léandre, qui est toujours en instruction en famille.\n" +
      "Nous sommes convaincus que ce mode de vie et d’apprentissage est bénéfique s’il est pratiqué avec envie et plaisir.\n" +
      "Pour nous, cette liberté est primordiale. Jusqu’à présent, nos démarches relevaient davantage de la peur que du choix. Aujourd’hui, nous sommes persuadés que la désobéissance civile est le seul moyen de préserver ce droit essentiel pour nos enfants.\n",
    date_declaration: "2024-09-20",
    latitude: 47.65868,
    longitude: -1.49114,
  },
  {
    id: "justine-guillaume",
    noms: "Justine et Guillaume Faurie",
    enfants: "Saya-Judith",
    departement: "Puy-de-Dôme",
    presentation:
      "Nous vivons avec notre fille Saya-Judith (6 ans ½ ) dans un hameau au cœur du Livradois-Forez où nous avons notre commerce. L'IEF s'est avérée être une évidence plus qu'un choix, correspondant complètement à nos convictions, à notre mode de vie et surtout aux besoins de notre fille.\n" +
      "Nous sommes en IEF depuis 2021. Nous avons toujours eu des résultats positifs aux différents contrôles de l'inspection académique. Pour l'année scolaire 2024/2025, bien que l'obligation d'autorisation nous déplaise fortement nous avons formulé une demande d'autorisation en IEF pour le motif 4 « situation propre à l'enfant ».\n" +
      "Comme de nombreuses familles nous avons reçu un refus d’autorisation d'instruction en famille émis sous la production d'un motif type stipulant la « non existence d'une situation propre à l’enfant ». Nous nous retrouvons otage d'une administration qui fait preuve d’une rigueur excessive dans son interprétation subjective et arbitraire de la loi. Le bien être de notre enfant et la continuité de son instruction étant primordial, nous ne donnerons pas notre consentement pour la scolariser contre sa volonté. La désobéissance civile est apparue pour nous être le moyen le plus approprié pour faire valoir notre droit à la liberté d'instruction.\n",
    date_declaration: "2024-10-30",
    latitude: 45.52996,
    longitude: 3.79902,
  },
  {
    id: "chloe-ange",
    noms: "Chloé et Ange Lukebadio",
    enfants: "Noé",
    departement: "Charente-Maritime",
    presentation:
      "Avant même d’avoir un enfant, nous avions cette conviction profonde que nous voulions nous investir entièrement dans son éducation. Une fois notre fils dans nos bras, c’était une évidence que nous ne déléguerions pas cette si belle mission et que nous l’accompagnerions dans son développement.\n" +
      "Malgré nos réticences quant à la justesse de cette loi, passant à une mode d’autorisation pour choisir d’instruire son enfant en famille, nous avons voulu croire que ce serait simplement une formalité pour vérifier l’implication des familles. Mais nous nous sommes vite rendus compte que cela ne faisait plutôt qu’accabler ces dernières, recevant des refus en masse !\n" +
      "Nous, parents de Noé, avons fait le choix de cœur de transformer nos vies pour enrichir la sienne. Consacrer notre temps à lui offrir le meilleur. C’est pourquoi nous tenons tant à cette liberté, droit fondamental qui est de pouvoir choisir d’instruire notre enfant en famille.\n" +
      "Et par notre entrée en désobéissance civile, nous voulons agir pour que cela soit respecté et que le retour à un système déclaratif soit voté. Pour l’intérêt supérieur des enfants qui vivent épanouis dans ce mode d’éducation, de vie.\n",
    date_declaration: "2024-11-15",
    latitude: 45.13878,
    longitude: -0.15451,
  },
  {
    id: "katy-gregori",
    noms: "Katy Pecqueur et Grégori Bron",
    enfants: "Aélis et Arwen",
    departement: "Cantal",
    presentation:
      "Nous sommes Katy et Gregori, parents d'Aélis et d'Arwen qui ont cinq ans toutes les deux. Bien avant l'arrivée de nos deux soleils dans le ventre, nous discutions déjà longuement de l'avenir.\n" +
      "Nous avons dès le départ vécu cette gémellité à quatre. Nous avons mis toutes nos forces et tout notre cœur dans nos choix, de l'allaitement double jusqu'à l’apprentissage autonome de l'alimentation. Nous avons découvert à quel point pour chaque étape d'autonomie elles avaient un rythme propre à chacune avec une méthode bien à elles.\n" +
      "Il était donc tout naturel pour nous d'éduquer nous-même nos filles en continuant cette découverte du monde à quatre, en respectant leur rythme de vie et d'apprentissage.\n" +
      "Soyons tous solidaire pour que le monde de demain, d'aujourd'hui soit rempli de joies et d'amour. Nous sommes très heureux de rejoindre le mouvement \"enfance libre\" pour défendre ensemble les libertés d'instruction de nos enfants.\n",
    date_declaration: "2024-10-08",
    latitude: 44.98789,
    longitude: 2.61552,
  },
  {
    id: "agathe-pierre",
    noms: "Agathe et Pierre Bernard",
    enfants: "3 enfants scolarisés, 1 en IEF et 2 de moins de 3 ans",
    departement: "Ille-et-Vilaine",
    presentation:
      "Bonjour, moi c'est Agathe, avec mon mari Pierre et nos 6 enfants. Je fais l'IEF (pour les niveaux PS MS GS) depuis que l'école est devenue obligatoire à 3ans. Tout s'est plutôt bien passé pour mes 3 aînés, jusqu'à cette année où l'on m'a refusé l'IEF pour la PS de ma fille Félicie. Nous nous apprêtions à la mettre à l'école à cette rentrée et refaire un dossier pour l'an prochain, mais mon mari a découvert l'association Enfance Libre. Nous n'avons pas réfléchi très longtemps, alors nous voilà !\n",
    date_declaration: "2024-10-15",
    latitude: 48.08955,
    longitude: -1.83747,
  },
  {
    id: "eudeline-virgile",
    noms: "Eudeline Joinet",
    enfants: "Sacha",
    departement: "Saône-et-Loire",
    presentation:
      "Eudeline, maman solo et Sacha, enfant libre et conscient, avons décidé d'entrer en désobéissance civile afin de rétablir le système déclaratif et défendre notre liberté de choisir comment nous voulons vivre.\n" +
      "Nous avons fait le choix de découvrir toutes les richesses et la diversité de ce monde en vivant au rythme des saisons, de nos besoins et de nos envies en étant nomades, l'ief est la suite logique et nous défendons notre liberté avec une grande détermination.\n",
    date_declaration: "2024-11-14",
    latitude: 46.6914,
    longitude: 3.6984,
  },
  {
    id: "severine-mikael",
    noms: "Séverine et Mikaël Lennoz",
    enfants: "Anaël",
    departement: "Isère",
    presentation:
      "Nous sommes Mikaël, Séverine et notre fils Anaël, âgé de 6 ans et demi.\n" +
      "Depuis sa naissance, nous avons choisi l'instruction en famille, convaincus que chaque enfant a droit à une éducation respectueuse de son rythme et de ses besoins. Ce choix, que nous avons toujours assumé avec responsabilité, a été validé lors de chaque inspection, sans jamais rencontrer de problème.\n" +
      'Cependant, cette année, nous nous sommes heurtés à un refus de l’autorisation d’instruire notre fils à domicile, une décision que nous jugeons non seulement arbitraire mais aussi en contradiction avec les principes fondamentaux des Droits de l’Homme, et plus particulièrement l’article 26 de la Déclaration universelle des droits de l’homme, qui stipule que "les parents ont, par priorité, le droit de choisir le genre d’éducation à donner à leurs enfants".\n' +
      "Face à cette atteinte à nos droits, nous avons décidé d'entrer en résistance pour défendre la liberté éducative de notre fils Anaël, et celle de toutes les familles qui se battent pour préserver ce choix.\n" +
      "Notre combat va au-delà de notre propre situation : il concerne tous les enfants et toutes les familles pour que soit respecté le droit à une éducation adaptée, et pour que les libertés fondamentales, dont celle de l’éducation, soient préservées.\n",
    date_declaration: "2024-10-21",
    latitude: 45.22009,
    longitude: 5.77774,
  },
  {
    id: "maite-tristan",
    noms: "Maïté Chotil et Tristan Delbot",
    enfants: "3 enfants",
    departement: "Finistère",
    presentation:
      "Nous sommes Maïté et Tristan parents de trois enfants de 1, 4 et 6 ans.\n" +
      "Nous avons choisi d’avoir des enfants et d’être disponibles pour eux, pouvoir les accompagner au quotidien, les voir grandir et répondre à leur besoins. L’instruction en famille est un choix de vie qui permet à nos enfants de s’épanouir à leur rythme.\n" +
      "Nous adhérons à Enfance Libre et entrons en désobéissance civile pour défendre la liberté d’instruction, qui nous semble fondamentale. Nous souhaitons un retour au régime déclaratif.\n" +
      "\n" +
      "Comme l’indique un manuel d’instruction civique de 2011 (p. 143, Librairie des Écoles) :\n" +
      "« Les citoyens doivent pouvoir éduquer leurs propres enfants comme ils veulent, parce qu’ils en sont responsables et que la transmission éducative est à leur charge. C’est la liberté d’enseignement. Un gouvernement qui embrigade les enfants dans des écoles ou des groupes de jeunesse obligatoires est une dictature.\n" +
      "Si un gouvernement le fait, il est “oppresseur”. Or c’est un droit de l’homme de résister à l’oppression.\n" +
      "Il n’y a aucune raison de nous interdire ce que nous sommes capables de faire, à condition que cela ne nuise pas aux autres, ni à l’intérêt général. »\n" +
      "\n" +
      "Aujourd’hui, le gouvernement, à travers son administration, nous oblige à scolariser nos enfants. Nous avons joué le jeu des demandes de dérogation. L’administration semble pouvoir modifier les règles de manière arbitraire. Nous sortons du jeu.\n",
    date_declaration: "2024-11-06",
    latitude: 48.07819,
    longitude: -4.32861,
  },
  {
    id: "aude-jonathan",
    noms: "Aude Renson et Jonathan Larribau",
    enfants: "Luca et Noah",
    departement: "Alpes-Maritimes",
    presentation:
      "Bonjour à tous,\n" +
      "Nous sommes Aude et Jonathan, parents de Noah et Luca, 11 ans tous les deux.\n" +
      "Nous avons commencé notre 5ème année en IEF et sommes toujours ravis.\n" +
      "Nous habitons dans le 06 à Nice.\n",
    date_declaration: "2024-11-22",
    latitude: 43.72403,
    longitude: 7.24504,
  },
  {
    id: "justine-lukeo",
    noms: "Justine et Joseph Lukeo",
    enfants: "Victoria et Naomi",
    departement: "Haute-Savoie",
    presentation:
      "Nous sommes une famille franco-kenyane vivant en Haute Savoie. Après deux ans d'école maternelle pour notre fille Victoria (née en 2012) et une tentative de création d'école alternative, nous avons fait le choix de l'instruction en famille. Quelle chance d'être entourés de si nombreuses familles IEF dans notre région! Notre deuxième fille, Naomi, est arrivée en 2019. L'année de ses 3 ans concordait avec celle de l'application de la nouvelle loi. Nous avons donc fait des demandes d'autorisation pendant les 3 dernières années, autorisations qui nous ont été accordées.\n" +
      "Mais en cette fin d'année 2024, 8ème année d'IEF, nous sommes prêts à défendre la liberté d'instruction et entrons en désobéissance civile avec joie, pour nous, pour toutes les familles qui le souhaitent et pour celles qui en auront besoin un jour. Nous souhaitons également que nos choix pédagogiques soient réellement respectés lors des contrôles, nous ne voulons plus que nos enfants portent la responsabilité d'un niveau scolaire pour pouvoir continuer l'instruction en famille.\n",
    date_declaration: "2024-11-30",
    latitude: 46.25756,
    longitude: 6.35113,
  },
  {
    id: "sara-khobaib",
    noms: "Sara et Khobaib Aarras",
    enfants: "Ala",
    departement: "Meurthe-et-Moselle",
    presentation:
      "Nous sommes Sara et Khobaib, les parents d’Ala, notre fille de trois ans. Nous pensons que l’instruction en famille permet de personnaliser l’apprentissage selon les besoins et le rythme de chaque enfant, ce qui est bénéfique pour Ala.\n" +
      "Malheureusement, notre demande d'autorisation pour l’IEF a été refusée, ainsi que notre recours. Nous défendons notre choix éducatif, car nous croyons que la liberté d’éducation est un droit fondamental.\n" +
      "Chaque famille devrait pouvoir choisir l’approche éducative qui lui convient le mieux.\n" +
      "C’est pourquoi nous soutenons le mouvement Enfance Libre dans sa démarche de désobéissance civile pour rétablir un régime déclaratif.\n",
    date_declaration: "2024-11-25",
    latitude: 48.66075,
    longitude: 6.17337,
  },
  {
    id: "noemie-jerome",
    noms: "Noémie Proponnet et Jérôme Benoit",
    enfants: "Jonah, Inès et Armand",
    departement: "Jura",
    presentation:
      "Nous sommes Jérôme et Noémie, parents de Jonah, 9 ans, Inès, 7 ans, et Armand, 3 ans.\n" +
      "Nos enfants n'ont jamais été scolarisés, nous avons fait le choix de l'IEF afin de respecter au mieux leurs besoins et de continuer de suivre leurs rythmes respectifs.\n" +
      "Confiants face aux comptes rendus positifs des inspections annuelles, nous avons présenté nos trois projets éducatifs pour obtenir nos autorisations. Après trois refus de nos dossiers et trois refus de nos RAPO, nous avons décidé de poursuivre malgré tout notre choix de vie.\n" +
      "Nous entrons en désobéissance civile. Nos enfants resteront instruits en famille aussi longtemps que cela correspondra à notre équilibre familial.\n",
    date_declaration: "2024-11-15",
    latitude: 46.81041,
    longitude: 6.11105,
  },
  {
    id: "gwenn-xavier",
    noms: "Gwenn Donnart et Xavier Lagrange",
    enfants: "Antoine, Julie, Maxime et Éloïse",
    departement: "Seine-et-Marne",
    presentation:
      "Nous sommes Gwenn et Xavier, les parents de 4 enfants : Antoine 10 ans, Julie 8 ans, Maxime 6 ans, Eloïse 4 ans.\n" +
      "Le premier confinement nous a fait prendre conscience du mal-être de nos enfants à l’école et d’une autre façon d’instruire. L’instruction en famille s’est donc imposée comme un moyen de vivre autrement, en accord avec nos principes éducatifs, où l’enfant est au cœur de ses apprentissages, de ses choix, et où les activités en extérieurs sont un moteur essentiel à leur développement.\n" +
      "Cette liberté est pour nous primordiale et nous prenons la décision de lutter contre ce droit menacé de disparaitre, pour nos enfants et tous les autres amenés, un jour peut-être, à devoir choisir ce mode d’instruction.\n" +
      "Nous souhaitons un retour au régime déclaratif.\n",
    date_declaration: "2024-11-18",
    latitude: 48.5002,
    longitude: 2.78654,
  },
  {
    id: "prescillia-baptiste",
    noms: "Prescillia et Baptiste Fontaine",
    enfants: "Inaya et Jordan",
    departement: "Seine-et-Marne",
    presentation:
      "Nous, c'est Baptiste et Prescillia, parents d'Inaya, 6 ans, et Jordan, 4 ans. Bien loin du tableau que l'idéologie collective peut se faire des parents instructeurs, nous ne sommes pas riches, loin de là, nous n'avons aucun privilège particulier, nous ne sommes pas des fanatiques religieux ou encore des marginaux et non, nous ne menons pas de bataille contre l'école mais pour nos enfants.\n" +
      "Nous sommes deux honnêtes citoyens, pour qui la naissance de notre fille a été un réel chamboulement dans notre vie, une véritable matrescence en tant que jeune maman. Elle nous a fait découvrir la parentalité dite positive, les pédagogies alternatives et l'instruction en famille, qui est aujourd'hui devenu un mode de vie. Jordan est venu agrandir notre famille et a suivi le même chemin que sa sœur, tous deux épanouis dans leurs apprentissages.\n" +
      "Le respect des besoins de nos enfants, qu'ils soient fondamentaux, d'ordre physiques, intellectuels, sociaux ou encore affectifs, est la pierre angulaire de notre parentalité. Et l'instruction en famille permet de les respecter, dans leur intégralité.\n" +
      "Lorsque nos demandes d'instruction en famille ont été refusées, devant la détresse de nos enfants et de tous ceux qui sont victimes de cette loi injuste, nous avons fait le choix d'entrer en désobéissance civile aux côtés d'enfance libre et de ses résistants, de défendre le droit de choisir le mode d'instruction le plus adapté à nos enfants et de réclamer le retour au régime déclaratif. Notre combat se veut pacifiste, motivé par l'amour de nos enfants, plus grande force qu'il soit.\n",
    date_declaration: "2024-11-06",
    latitude: 48.80662,
    longitude: 2.62437,
  },
  {
    id: "julie-jean-marc",
    noms: "Julie et Jean Marc Staley",
    enfants: "Iona, Swane et Siloé",
    departement: "Allier",
    presentation:
      "Nous sommes Julie, Jean-Marc, et nos 4 enfants, Siloé, Swane, Iona et Natanel âgés de 10 à 17 ans.\n" +
      "Nous habitons dans l'Allier, et nous sommes en instruction en famille depuis 2018.\n" +
      "Passer du temps en famille et voir nos enfants grandir a toujours été une priorité pour nous, et l'instruction en famille s'est inscrite dans nos vies de manière fluide, après une rentrée en 6ème de notre fils aîné vécue par lui comme un lourd fardeau (qui a depuis septembre 2024 commencé un apprentissage en ébénisterie de manière sereine et enthousiaste, avec les Compagnons du Devoir!)\n" +
      "Nous souhaitons offrir à nos enfants une vie que l'on sent belle pour eux, nous voulons les voir se chercher, se trouver et s'épanouir dans ce qu'ils sont chacun profondément. Nous aimons avoir des projets en famille, nous trouvons chacun notre compte dans ce mode de vie IEF. Et il nous est complètement inconcevable de remettre nos enfants à l'école, contre leur gré, sous contrainte d'une loi que nous considérons abusive et injuste.\n" +
      "Nous rejoignons Enfance Libre dans la paix, conscients de l'enjeu de cette lutte.\n",
    date_declaration: "2024-10-03",
    latitude: 46.33871,
    longitude: 2.62281,
  },
  {
    id: "louise-emmanuelle-frederic",
    noms: "Louise-Emmanuelle et Frédéric Benel",
    enfants: "Charles et Charlotte",
    departement: "Gard",
    presentation:
      "Bonjour,\n" +
      "Nous sommes Frédéric et Louise-Emmanuelle, parents de deux bouts de chou, Charles 4 ans et Charlotte 2 ans et demi.\n" +
      "Pour nous, l'IEF a été une évidence lorsque Charles est né. Nous avons tellement aimé le voir découvrir le monde dans ses premiers mois, nous avons pris à cœur notre rôle de parents pour lui transmettre le plus de connaissances possible.\n" +
      "Nous avons fait une première année d'IEF pour sa PSM (nous avions eu l'autorisation assez rapidement), et avons voulu renouveler pour sa MSM. Suite au refus de l'académie de Montpellier, nous avons décidé d'entrer en désobéissance civile pour poursuivre l'IEF.\n" +
      "A cet âge, les enfants apprennent plus en étant dehors qu'enfermés dans une salle de classe. Nous saisissons chaque occasion pour leur faire découvrir le monde qui les entoure, et finalement tout est prétexte à l'apprentissage.\n" +
      "Louise-Emmanuelle et Frédéric\n",
    date_declaration: "2024-11-28",
    latitude: 43.75068,
    longitude: 4.15527,
  },
  {
    id: "volatiana-guillaume",
    noms: "Volatiana Rajaspera et Guillaume Colas",
    enfants: "Lysandre et Apollinaire",
    departement: "Hautes-Pyrénées",
    presentation:
      "Nous sommes Guillaume Colas et Volatiana Rajaspera, parents de deux garçons : Lysandre 4 et Apollinaire 2 ans.\n" +
      "Bien que l'instruction en famille a toujours été une évidence pour nous, nous avons scolarisé notre fils aîné dans l'espoir  qu'il puisse se forger sa propre opinion dans le cadre scolaire. \n" +
      "Cependant, son enthousiasme s'est rapidement effondré. Très vite, nous avons observé chez lui une accumulation d'angoisse et de fatigue, soulignant ainsi les limites du cadre scolaire.\n" +
      "Nous refusons de  nous soumettre à une loi injuste et arbitraire qui nous contraint à demander chaque année une autorisation pour instruire nos propres enfants. \n" +
      "Cette exigence, non seulement contraignante, bafoue notre liberté fondamentale d'éducation et place les familles sous le joug d'une réglementation instable et aléatoire. C'est pourquoi, en septembre 2024, nous avons fait le choix de la désobéissance civile, conscients que cette loi va à l'encontre de l'intérêt de nos enfants et du bien-être familial.\n" +
      "Nous nous battons pour un retour au régime déclaratif, car la loi actuelle, dans son caractère intrusif et inégal, ne respecte ni les droits des familles ni les besoins des enfants.\n",
    date_declaration: "2024-09-18",
    latitude: 43.25045,
    longitude: 0.49593,
  },
  {
    id: "yasmine-ahmed",
    noms: "Yasmine et Ahmed Abrikous",
    enfants: "Marwa, Aliy et Muhammad",
    departement: "Bas-Rhin",
    presentation:
      "Bien le bonjour à toutes et à tous,\n" +
      "\n" +
      "Arrivent parmi vous du pays de la cigogne, Yasmine et Ahmed des Alsaciens d'adoption ainsi que leur 3 enfants, Marwa, Muhammad et Aliy.\n" +
      "\n" +
      "Avant d'être confrontés à l'école publique, nous n’avions aucune idée de ce qu'était l'Instruction En Famille (IEF).\n" +
      "\n" +
      "Durant les années de maternelle de nos enfants, nous étions très impliqués dans la vie de leurs différentes classes (parent délégué, participation aux sorties scolaires et à différents évènements).\n" +
      "La découverte du CP avec Marwa fût, malheureusement, une douloureuse et pénible expérience par rapport à ses facilités d'apprentissages (ennui et négligence du corps éducatif). Malgré un changement d‘école, Marwa n’a pas retrouvé sa sérénité passée et le contexte sanitaire en 2020 n’a pas arrangé la situation.\n" +
      "\n" +
      "Notre recherche d’alternative nous a fait découvrir d’autres propositions pédagogiques faisant sens pour nous et surtout répondant aux besoins de nos enfants. Nous avons donc pour l'année scolaire 2020/2021, commencé l'IEF et choisi un accompagnement à distance avec un établissement scolaire proposant la pédagogie inversée d’Eric Mazur et assurant un suivi complet à l’aide d’outils variés.\n" +
      "\n" +
      "Cette expérience a conforté nos choix et répondu à notre priorité, l’épanouissement, le bien-être et la sécurité de nos enfants.\n" +
      "\n" +
      "Par ailleurs, le choix de l’IEF nous permet aussi de transmettre à nos enfants des valeurs qui nous sont chères, comme l’empathie, la solidarité, la coopération. De plus, nous accordons une place essentielle à la pratique sportive, aux sorties culturelles, en pleine nature et aux voyages. L’IEF nous offre donc la liberté et le temps nécessaire pour mettre en place tous ces projets.\n" +
      "\n" +
      "Voici ce qui rythme notre quotidien et ce que nous souhaitons protéger.\n" +
      "\n" +
      "Nous ne pensions pas nous trouver, un jour, dans cette situation, mais pour préserver notre liberté d’instruction, nous avons fait le choix de devenir des désobéissants, pacifistes, aux côtés de l’association Enfance Libre et nous œuvrons ensemble pour un retour au régime déclaratif.\n",
    date_declaration: "2024-09-18",
    latitude: 43.25045,
    longitude: 0.49593,
  },
  {
    id: "domitille-philippe",
    noms: "Domitille et Philippe Rossier",
    enfants: "Célestin, Raphaël et Marine",
    departement: "Manche",
    presentation:
      "Nous sommes une famille du département de la Manche, prés du Mont Saint Michel, composée de 7 personnes. Les jeunes Célestin (6ans), Raphaël (12ans), Marine (16ans), Myriam (19ans), Youri (21ans), les moins jeunes Domitille (48 ans) et Philippe (50 ans).\n" +
      "Nous avons basculés en instruction en famille en février 2013, après qu’un de nos jeunes ait subi déjà 3 ans de harcèlement scolaire.\n" +
      "Étant allé au bout de ses ressources et nous au bout des possibilités de résolutions du problème sans que sa situation ne s’améliore, nous avons, de concert avec nos jeunes choisi, de changer nos vies pour retrouver en premier la sécurité, puis la joie de vivre…\n" +
      "Depuis, aucun des cinq n’a choisi de mettre un pied à l’école… Si ce n’est l’aîné qui a intégré une école de jazz l’année dernière.\n" +
      "Nous avons découvert d’année en année que cela élargissait nos horizons grandement. Nous avons découvert des réalités qui nous étaient alors inconnues… appris à lâcher prise et à reconsidérer tout ce qui concerne l’instruction et pas seulement car nous avons élargi le concept à l’accueil de ma maman qui a la maladie d’Alzheimer et ne peut plus vivre seule.\n" +
      "Pourquoi demander une autorisation alors que nous ne nous occuperons pas de la réponse? Cela n’avait aucun sens, donc pas de demande =pas de refus. D’autant plus que nous sommes les témoins depuis 2022, des souffrances des familles, générées par les maltraitances de l’administration qui ne s’embarrasse guère de la conformité aux textes de loi.\n",
    date_declaration: "2024-12-11",
    latitude: 48.66895,
    longitude: -1.23385,
  },
  {
    id: "vanessa-stephane",
    noms: "Vanessa Wursten et Stéphane Overlen",
    enfants: "une fille",
    departement: "Haute-Savoie",
    presentation:
      "Nous vivons en Haute-Savoie avec notre fille de 6 ans 1/2.\n" +
      'Nous avons fait partie de la dernière volée de familles bénéficiant du régime déclaratif alors que notre fille faisait sa toute première "non rentrée" en maternelle en 2021. Notre aventure en IEF s\'est ensuite poursuivie avec une autorisation "de plein droit" pour les deux années suivantes.\n' +
      "C'est en cette année 2024, pour le CP, que nous avons été pour la première fois soumis à une demande d'autorisation pour l'IEF puis confrontés à un refus de l'académie de Grenoble. Refus qu'ils ont réitéré suite à notre RAPO.\n" +
      "Notre fille n'était pas encore née que nous savions déjà que nos choix en matière d'instruction obligatoire se porteraient vers des pédagogies alternatives, plus à même de répondre aux réels besoins d'un enfant et de valoriser son unicité.\n" +
      "Après avoir visité des écoles alternatives et nous être informés sur les différentes possibilités qui s'offraient à nous tout en résonnant avec nos valeurs éducatives, nous nous sommes renseignés de plus en plus sur l'IEF jusqu'à aller à la rencontre d'autres familles.\n" +
      "Ces rencontres très riches ont fini de nous convaincre que l'IEF était ce que nous pourrions offrir de plus précieux à notre fille comme choix d'instruction et mode de vie pour garantir son intérêt supérieur de façon respectueuse et bienveillante.\n",
    date_declaration: "2024-11-26",
    latitude: 46.0978214,
    longitude: 6.0995802,
  },
  {
    id: "helicia-maxence",
    noms: "Helicia Groseil et Maxence Ravelomanantsoa",
    enfants: "Isée",
    departement: "Côtes-d'Armor",
    presentation:
      "Nous sommes Maxence et Helcia, parents d'Isée -bientôt 4 ans.\n" +
      "Être parent signifie pour nous : écouter les besoins de notre enfant.\n" +
      "Un de ses besoins est notre présence, pilier de son bien-être.\n" +
      "C'est donc naturellement que nous l'accompagnons depuis sa naissance dans l'apprentissage de la vie, et que nous avons rencontré l'Instruction En Famille.\n" +
      "L'IEF est ce qui répond le plus aux besoins d'Isée.\n" +
      "Nous avons constitué un dossier qui a été refusé, puis un RAPO qui a, lui aussi, été refusé.\n" +
      "Devenir résistant est pour nous la suite logique de notre histoire, soutenant de tout cœur les familles qui prennent le chemin de l'IEF avec ou sans accord.\n" +
      "Donner la possibilité aux familles qui souhaitent faire IEF en revenant au régime déclaratif est essentiel pour le bien être de nos enfants.\n",
    date_declaration: "2024-12-13",
    latitude: 48.50722,
    longitude: -2.77727,
  },
  {
    id: "eva-maxime",
    noms: "Eva Bachelard et Maxime Tschanturia",
    enfants: "Victor",
    departement: "Charente",
    presentation:
      "Nous sommes les parents de Victor (6 ans). Nous vivons dans une petite commune de Charente, entourés d'animaux et de la nature. Victor est en école à la maison depuis la maternelle, après 6 mois de petite section. Il apprend avec confiance et grandit avec assurance, en observant, en jouant, avec curiosité, dans la joie de l'enfance. Tout est sujet à découverte et apprentissage. Cette qualité de vie et la joie de voir son enfant s'épanouir au sein de l'Instruction en Famille nous sont précieuses. Nous remercions toute l'équipe d'Enfance Libre de nous accueillir et de nous accompagner dans notre choix et la défense de la liberté d'instruction pour tous les parents !\n",
    date_declaration: "2024-09-11",
    latitude: 45.95604,
    longitude: -0.06386,
  },
  {
    id: "leslie-hugues",
    noms: "Leslie Hanet et Hugues Brentegani",
    enfants: "Louise",
    departement: "Gard",
    presentation:
      "Depuis août dernier, nous avons décidé d'entrer en désobéissance civile déclarée.\n" +
      'Pourquoi?... à la base, nous voulions rester dans "ce qu\'il faut faire" pour être en IEF.\n' +
      "On nous demandait d'être inspecté, on se préparait pour être inspecté!\n" +
      "On nous demandait un dossier pédagogique, on a bossé des heures sur un dossier ! Qui a été refusé!\n" +
      "On a fait un Rapo... même sort!\n" +
      "Un recours gracieux... idem!\n" +
      "Et bien là, nous ne sommes plus d'accord!!!\n" +
      "Trop injuste c'est trop injuste!\n" +
      "Alors nous avons choisi , après d'amples temps de réflexions, d'entrer en désobéissance civile et de continuer à instruire nos 2 filles de 2 et 8 ans !\n",
    date_declaration: "2024-11-21",
    latitude: 44.30202,
    longitude: 3.98542,
  },
  {
    id: "laura-francois",
    noms: "Laura Muller et François Muntsch",
    enfants: "Bradley et Mylann",
    departement: "Cher",
    presentation:
      "Nous nous engageons aujourd'hui dans ce mouvement de désobéissance civile afin que l'intérêt supérieur de nos enfants, leur épanouissement, leurs liberté d'apprendre, où et quand ils veulent soient respectés.\n",
    date_declaration: "2024-12-27",
    latitude: 47.09146,
    longitude: 3.00404,
  },
  {
    id: "rachel-nicolas",
    noms: "Rachel Victoire et Nicolas Lebastard",
    enfants: "Malo et Aël",
    departement: "Morbihan",
    presentation:
      "Bonjour tout le monde,\n" +
      "Nous sommes Nicolas et Rachel, parents de 2 lutins bretons (8 et 4 ans).\n" +
      "Nous ne connaissions pas l’instruction en famille jusqu’à l’entrée en maternelle de notre fils aîné. Cela ne s'est passé comme attendu! Nous avons donc du « réagir » et c’est ainsi que nous avons commencé cette belle aventure familiale consolidée par l’arrivée de son frère. Et depuis, c’est devenu un choix de vie. Les rencontres, les impacts positifs sur nous et notre mode de vie nous ont conquis. Les contrôles positifs et l’épanouissement de nos enfants nous ont conforté dans cette voie.\n" +
      "Jusqu'à ce que l'administration en décide autrement, en refusant notre dernière demande d'autorisation (pour finalement en accepter 1 sur les 2 après le recours). Décision arbitraire, incohérente et n'allant clairement pas de l'intérêt de l'enfant.\n" +
      "\n" +
      "La désobéissance civile nous apparaît comme le moyen le plus approprié pour lutter, alerter contre cette loi inutile et faire valoir le droit à la liberté d'instruction pour toutes les familles qui en ont le besoin ou l’envie.\n",
    date_declaration: "2025-01-08",
    latitude: 47.73749,
    longitude: -3.37272,
  },
  {
    id: "nadege-eduardo",
    noms: "Nadége et Eduardo Serodio",
    enfants: "Lisa et Anaëlle",
    departement: "Hérault",
    presentation:
      "Nous sommes Nadège (ex-codirectrice d'une école alternative) et Eduardo (ingénieur bâtiment durable), parents de Lisa (13 ans) et Anaëlle (9 ans et demi). Pourtant sensibilisé.e.s dès la naissance de notre première fille à une démarche éducative respectueuse, nous l'avons scolarisée jusqu'au CE1 dans une école à pédagogie classique. Elle a appris à se soumettre et à obéir sans pouvoir écouter sa boussole intérieure ni faire respecter ses besoins.\n" +
      "A la naissance de notre seconde fille porteuse d'une maladie génétique, nous étions déterminé.e.s à choisir une voie alternative : Nadège a cocréé une école alternative en chemin vers le 3ème type et nos filles y ont grandi pleinement heureuses durant 6 ans...\n" +
      "L'école n'ayant pas de niveau 4ème pour Lisa et aspirant à ralentir notre rythme de vie, nous avons fait le choix familial merveilleux d’accompagner nos filles en IEF en 2024.\n" +
      "Accordée pour sa sœur, notre demande d'autorisation pour Lisa a été refusée, après RAPO, pour des raisons incompréhensibles pour nous, d'autant que la même demande avait été acceptée l'année passée. Nous sommes en colère et profondément tristes de constater qu'en France, les familles ne sont plus libres de choisir la vie qu'ils souhaitent. Les parents les plus lucides et actifs pour changer le monde sont ceux à qui on met des bâtons dans les roues .... Nous sommes convaincus que, pour devenir des adultes respectueux, épanouis, avec un sens éthique élevé, les jeunes personnes doivent être traitées avec respect, confiance, bienveillance et amour. Nous devons leur laisser le temps d'être à l’écoute d'eux.elles-mêmes, de prendre des décisions en fonction de leurs besoins et aspirations, de faire des activités ayant du sens pour eux.elles, d'apprendre à coopérer et s’autogérer.\n" +
      "\n" +
      "Nous sommes donc déterminés plus que jamais à faire valoir le droit des enfants à vivre leur enfance de la manière qui leur convient le mieux et de pouvoir avoir le choix. La Désobéissance Civile nous apparaît alors comme la démarche la plus juste à adopter pour revendiquer un retour au régime déclaratif.\n",
    date_declaration: "2025-01-16",
    latitude: 43.44719,
    longitude: 3.75104,
  },
  {
    id: "emeline-eddy",
    noms: "Émeline Salmon et Eddy Félicité",
    enfants: "Kanelle et Oléa",
    departement: "Gard",
    presentation:
      "Nous faisons l’ief depuis 2021 suite à la demande de notre petite fille Kanelle 3 ans.\n" +
      "Après 18 mois de réflexion, nous avons osé la sortir de l’école et commencé l’ief. Et là tout c’est amélioré : notre petite fille de 4 ans a retrouvé sa joie, son calme et sa santé. A nous parents il nous a fallu encore un peu de temps pour savoir comment on allait s’organiser, choisir une pédagogie, comprendre le fonctionnement de l’académie… \n" +
      "Puis très vite l’ief est devenue un choix de famille, un mode de vie.\n" +
      "En 2022 on a commencé l’ief pour Oléa née en 2019. Il n’est pas concevable de scolariser en établissement nos filles de force.\n" +
      "Dans nos choix éducatifs, nous œuvrons pour un monde meilleur et cela passe par l’écoute et la prise au sérieux de la parole de chacun. Et la parole de l’enfant ne compte pas moins que celle de l’académie. Nous pensons qu’il est plus approprié de faire des propositions, d’argumenter et de trouver des solutions ensemble.\n" +
      "Nous avons consacré beaucoup de temps et d’énergie à rédiger des demandes d’autorisation, des RAPO, choisir un avocat, corriger un référé suspension… Chaque recommandé engendre un stress énorme. Lors de la réception de notre 1ère mise en demeure, notre avocat nous a écrit de scolariser nos filles en établissement, nous avons pris le temps de la réflexion, analysé les différentes options puis nous avons « par hasard » découvert l’association Enfance Libre. \n" +
      "Et là tout est devenu évident : nous avons dit STOP à la mascarade de l’académie. Nous n’avons aucune chance d’obtenir une autorisation d’ief, nous avons gaspillé trop d’énergie avec l’académie et le TA, nous ne voulons surtout pas cautionner ce système. Notre seul regret avoir été ignorant de la désobéissance civile.\n" +
      "Cette action pacifique en collaboration avec l’association Enfance Libre, ouverte à la discussion, pousse à une prise de conscience pour un monde meilleur, tant au point de vue politique, sociétal, humain et surtout des atteintes à nos libertés.\n",
    date_declaration: "2025-01-03",
    latitude: 44.0862,
    longitude: 4.00183,
  },
  {
    id: "emilie-sophie",
    noms: "Émilie Pierre et Sophie Duva",
    enfants: "Louise",
    departement: "Doubs",
    presentation:
      "Nous sommes Émilie, maman de Louise et Sophie, maman en procédure d’adoption de Louise, et Louise 10 ans et demi, parce que les demis, ça compte !\n" +
      "Nous avons passé 3 années à chercher, en vain, une école à l’écoute des besoins de Louise et respectueuse de qui elle est, au prix de bien des souffrances pour toute la famille.\n" +
      "Nous sommes convaincues que l’empathie et le respect sont les piliers du bien être et les moteurs pour apprendre. L’IEF est aujourd’hui le seul mode d’instruction qui permette à notre fille de se sentir sereine, heureuse et motivée. C’est après avoir étudié le sujet dans tous les sens, que toutes les trois, à la demande de Louise, nous nous lançons dans la l’IEF et en toute conscience, mais non sans appréhension, dans la désobéissance civile pour moi (Émilie) et bientôt légalement pour Sophie.\n" +
      "Aucun enfant ni parent ne devraient être contraint à une instruction où la scolarisation uniquement extérieure à son foyer serait obligatoire. Le choix du mode d’instruction de son enfant est le fondement même d’une démocratie et c’est cette conviction profonde qui nous amène à entrer en désobéissance civile, tant que la loi demeurera anti démocratique et tant que notre fille souhaitera que son instruction se fasse en famille.\n",
    date_declaration: "2025-01-26",
    latitude: 47.44156,
    longitude: 6.82807,
  },
  {
    id: "betty-david",
    noms: "Betty Martin et David Moreira",
    enfants: "Nina Oulia et Adam",
    departement: "Morbihan",
    presentation:
      "Notre choix d'instruire nos enfants en dehors des murs de l'école et de le faire en désobéissance civile s'est présenté comme une évidence. Depuis, nos enfants ont retrouvé le sourire et ils apprennent à réécouter leur curiosité naturelle.\n" +
      "Nous contestons le passage du régime déclaratif au régime d'autorisation préalable et nous souhaitons faire avancer les droits de tous les enfants.\n",
    date_declaration: "2025-01-24",
    latitude: 48.06776,
    longitude: -2.74675,
  },
  {
    id: "marion-sebastien",
    noms: "Marion Berger et Sébastien Croyet ",
    enfants: "Leny et Liam",
    departement: "Ardennes",
    presentation:
      "Nous sommes Sébastien et Marion, parents de Lény (5 ans) et Liam (3 ans).\n" +
      "Lény est en troisième année d’IEF, et Liam a commencé cette année. Nous avons toujours voulu offrir le meilleur à nos enfants. C’est donc tout naturellement que nous nous sommes tournés vers l’IEF. Il est hors de question pour nous de scolariser nos enfants tant qu’ils n’en ressentiront pas le besoin et ne le demanderont pas eux-mêmes. Nous respecterons leurs besoins et leurs envies, car leur bien-être passe avant tout. Nous pensons que c’est la clé d’un apprentissage complet et bien plus épanoui.\n" +
      "Nous sommes déterminés à nous battre aux côtés de toutes les familles qui mènent le même combat, pour défendre nos droits et ceux de nos enfants, jusqu’à ce que le régime déclaratif soit rétabli.\n",
    date_declaration: "2024-12-14",
    latitude: 49.93788,
    longitude: 4.63057,
  },
  {
    id: "charline-nicolas",
    noms: "Charline Mercier & Nicolas Galli",
    enfants: "Wynona, Aleck et Mia",
    departement: "Vosges",
    presentation:
      "Bonjour, nous sommes Charline et Nicolas, les parents de 3 enfants, dont 2 en instruction en famille. L'instruction en famille est pour nous le prolongement de la parentalité proximale. Une évidence. Nous avons à cœur de grandir avec eux. Nous apprécions les voir s'émerveiller de tout dans la douceur et le respect.\n" +
      "Nous défendons notre liberté avec détermination.\n",
    date_declaration: "2025-01-31",
    latitude: 48.01525,
    longitude: 6.71665,
  },
  {
    id: "cecile-jean-paul",
    noms: "Cécile Lechevallier et Jean-Paul Eyckermans ",
    enfants: "Emi, Mây-Lan et Jun",
    departement: "Var",
    presentation:
      "Nous entrons en résistance car nous sommes convaincus que le régime d’autorisation est une privation de libertés fondamentales pour les familles, et qu’il n’est pas motivé par le bien-être supérieur de l’enfant. Nous espérons que ce combat fera changer les choses et triompher le bon sens !\n",
    date_declaration: "2025-02-13",
    latitude: 43.134049,
    longitude: 5.915766,
  },
  {
    id: "jessica-pierrick",
    noms: "Jessica Bordier et Pierrick Lemay",
    enfants: "Jolene, Jaëlle et Jemma",
    departement: "Sarthe",
    presentation:
      "Pratiquants l’instruction en famille depuis sept ans, nous rejoignons la grande famille d’Enfance Libre afin de défendre les enfants face aux abus de l’Etat. Victimes d’un contrôle abusif et traumatisant pour toute la famille, victimes des dérives de cette loi immonde : NON, nous ne scolariserons pas nos enfants si ce n’est pas leur souhait et NON, nous n’effectuerons pas ces demandes d’autorisation énergivores. Nous n’avons pas à demander l’autorisation, en tant qu’adultes responsables de nos enfants, nous sommes les mieux placés pour répondre à leurs besoins et faire les choix en fonction de ceux-ci.\n" +
      "Nous nous engageons à désobéir tant que le régime d’autorisation sera en place et que ces dérives, abus de pouvoir et excès de zèle n’auront pas pris fin.\n",
    date_declaration: "2025-02-28",
    latitude: 47.88179,
    longitude: 0.553578,
  },
  {
    id: "angelique-fabrice",
    noms: "Angélique Blanchet & Fabrice Holtz",
    enfants: "Elwen",
    departement: "Ille-et-Vilaine",
    presentation:
      "Bonjour, nous sommes Angélique et Fabrice, parents d'Elwen 3 ans. L'IEF est la suite logique de nos choix de vie et de parentalité : Respecter les rythmes naturels le plus possible, remplir les journées d'activités dans lesquelles on trouve du sens, agir selon nos valeurs, les défendre et les transmettre à Elwen...\n" +
      "Quand nous avons découvert la volonté politique d'ôter à chacun la liberté d'instruire ses enfants comme bon lui semble, le choix de rejoindre Enfance Libre s'est rapidement imposé, tant pour défendre notre liberté et celle d'Elwen, que pour participer à la construction d'une société alternative qui cherche à préserver une place centrale à cette liberté.\n",
    date_declaration: "2024-10-08",
    latitude: 47.87545,
    longitude: -1.688,
  },
];
