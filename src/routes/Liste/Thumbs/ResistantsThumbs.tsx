import React from "react";
import { Grid } from "@mantine/core";
import { Resistant } from "../../../Resistant";
import { ResistantThumb } from "./ResistantThumb";

interface Props {
  resistants: Resistant[];
}

export const ResistantsThumbs = ({ resistants }: Props) => (
  <Grid columns={4} gutterXs="md" gutterXl="md">
    {resistants.map((r) => (
      <Grid.Col span={"auto"} key={r.id}>
        <ResistantThumb resistant={r} />
      </Grid.Col>
    ))}
  </Grid>
);
