import { BackgroundImage, Text } from "@mantine/core";
import React from "react";
import { useNavigate } from "react-router";
import { Resistant } from "../../../Resistant";
import { EmbeddedYoutube } from "../../../components/EmbeddedYoutube";
import { HomeContext } from "../../../context/HomeContext";
import { getResistantPageUrl } from "../../../getResistantPageUrl";
import { getResistantPhotoUrl200 } from "../../../getResistantPhotoUrl";
import { getResistantGrayscaleRule } from "../../../style/getResistantGrayscaleRule";

interface Props {
  resistant: Resistant;
}

export const ResistantThumb = ({ resistant }: Props) => {
  const navigate = useNavigate();
  const { isHome } = React.useContext(HomeContext);

  const handleClick = () =>
    isHome
      ? window.location.assign(getResistantPageUrl(resistant))
      : navigate(resistant.id);

  if (resistant.video) {
    return (
      <EmbeddedYoutube
        youtubeId={resistant.video}
        width={"200"}
        height={"200"}
        className="resistant-thumb"
        onClick={handleClick}
        style={getResistantGrayscaleRule(resistant)}
      />
    );
  }

  return (
    <BackgroundImage
      src={getResistantPhotoUrl200(resistant)}
      className="resistant-thumb"
      style={getResistantGrayscaleRule(resistant)}
    >
      <div
        className="thumb resistant-thumb"
        onClick={handleClick}
        style={{ position: "relative" }}
      >
        <div className="thumb-name">
          <Text size="sm">{resistant.noms}</Text>
        </div>
      </div>
    </BackgroundImage>
  );
};
