import {
  Center,
  Group,
  Loader,
  SegmentedControl,
  TextInput,
} from "@mantine/core";
import {
  IconCameraSelfie,
  IconList,
  IconMap,
  IconUsers,
} from "@tabler/icons-react";
import React, { Suspense, lazy } from "react";
import { Outlet } from "react-router";
import { isAncienResistant, resistantsOrder } from "../../Resistant";
import { Separator } from "../../components/Separator";
import { DisplayModeContext } from "../../context/DisplayModeContext";
import { HomeContext } from "../../context/HomeContext";
import { resistants } from "../../resistants";
import { ViewMode } from "../ViewMode";
import { FiltreAcademie } from "./FiltreAcademie";
import { FiltreDepartement } from "./FiltreDepartement";
import { ResistantRow } from "./List/ResistantRow";
import { ResistantsThumbs } from "./Thumbs/ResistantsThumbs";
import { getAcademie } from "../../getAcademie";

const normalize = (text: string) =>
  (text || "")
    .toLowerCase()
    .normalize("NFD")
    .replace(/\p{Diacritic}/gu, "");

const LazyResistantsMap = lazy(() => import("./Map/ResistantsMap"));

export const ListeResistants = () => {
  const { mode, setMode } = React.useContext(DisplayModeContext);
  const { isHome } = React.useContext(HomeContext);

  const [departement, setDepartement] = React.useState<string | null>(null);
  const [academie, setAcademie] = React.useState<string | null>(null);
  const [nom, setNom] = React.useState<string | undefined>(undefined);

  const filtreNom = (event: React.ChangeEvent<HTMLInputElement>) =>
    setNom(event.target.value);

  const nbFamillesResistantes = resistants.filter(r => !isAncienResistant(r)).length;

  const filteredResistants = resistants
    .filter(
      (r) => !nom || normalize(r.noms).includes(normalize(nom.toLowerCase()))
    )
    .filter(
      (r) =>
        !departement ||
        normalize(r.departement).includes(normalize(departement))
    )
    .filter(
      (r) => !academie || normalize(getAcademie(r.departement)).includes(normalize(academie))
    )
    .sort(resistantsOrder);

  return (
    <>
      <h2 style={{textAlign:"center"}}>Les {nbFamillesResistantes} familles résistantes !</h2>
      <div id="listeResistants">
        <Group position="center">
          <SegmentedControl
            data={
              [
                isHome
                  ? ""
                  : {
                      value: "list",
                      label: (
                        <Center>
                          <IconList />
                          Liste
                        </Center>
                      ),
                    },
                {
                  value: "photos",
                  label: (
                    <Center>
                      <IconCameraSelfie />
                      Photos
                    </Center>
                  ),
                },
                {
                  value: "map",
                  label: (
                    <Center>
                      <IconMap />
                      Carte
                    </Center>
                  ),
                },
              ].filter(Boolean) as Array<{
                value: string;
                label: React.ReactNode;
              }>
            }
            size="xs"
            value={mode}
            onChange={(value) => setMode(value as ViewMode)}
          />
          <TextInput
            icon={<IconUsers />}
            placeholder="Nom"
            onChange={filtreNom}
          />
          <FiltreDepartement
            resistants={resistants}
            onChange={setDepartement}
          />
          <FiltreAcademie resistants={resistants} onChange={setAcademie} />
        </Group>

        <Separator />

        {mode === "list" &&
          filteredResistants.map((r) => (
            <ResistantRow resistant={r} key={r.id} />
          ))}

        {mode === "photos" && (
          <ResistantsThumbs resistants={filteredResistants} />
        )}

        {mode === "map" && (
          <Suspense
            fallback={<Loader style={{ display: "block", margin: "auto" }} />}
          >
            <LazyResistantsMap resistants={filteredResistants} />
          </Suspense>
        )}
        <Outlet />
      </div>
    </>
  );
};
