import { Select } from "@mantine/core";
import { IconMapPin } from "@tabler/icons-react";
import React from "react";
import { Resistant } from "../../Resistant";

interface Props {
  resistants: Resistant[];
  onChange: (value: string) => void;
}

export const FiltreDepartement = ({ resistants, onChange }: Props) => (
  <Select
    placeholder="Filtrer par département"
    onChange={onChange}
    clearable
    searchable
    nothingFound="Aucun résistant ici"
    icon={<IconMapPin />}
    style={{ width: "inherit" }}
    data={[...new Set(resistants.map((r) => r.departement))]
      .filter(Boolean)
      .sort()
      .map((d) => ({ label: d, value: d }))}
    styles={{ input: { paddingRight: 0, width: "inherit" } }}
  />
);
