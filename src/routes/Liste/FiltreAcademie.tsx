import React from "react";
import { Select } from "@mantine/core";
import { IconBuilding } from "@tabler/icons-react";
import { Resistant } from "../../Resistant";
import { getAcademie } from "../../getAcademie";

interface Props {
  resistants: Resistant[];
  onChange: (value: string) => void;
}
export const FiltreAcademie = ({ resistants, onChange }: Props) => (
  <Select
    placeholder="Filtrer par académie"
    onChange={onChange}
    clearable
    searchable
    nothingFound="Aucun résistant ici"
    icon={<IconBuilding />}
    data={[...new Set(resistants.map((r) => getAcademie(r.departement)))]
      .filter(Boolean)
      .sort()
      .map((a) => ({ label: a, value: a }))}
    styles={{ input: { paddingRight: 0, width: "inherit" } }}
  />
);
