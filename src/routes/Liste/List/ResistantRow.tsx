import { useNavigate } from "react-router";
import { EmbeddedYoutube } from "../../../components/EmbeddedYoutube";
import { ExternalLink } from "../../../components/ExternalLink";
import { Separator } from "../../../components/Separator";
import { getResistantDeclarationUrl } from "../../../getResistantDeclarationUrl";
import { getResistantPhotoUrl200 } from "../../../getResistantPhotoUrl";
import {
  getResistantDateDeclarationMoisAnnee,
  getResistantDateFinMoisAnnee,
  isAncienResistant,
  Resistant,
} from "../../../Resistant";
import { getResistantGrayscaleRule } from "../../../style/getResistantGrayscaleRule";
import { getAcademie } from "../../../getAcademie";

interface Props {
  resistant: Resistant;
}

export const ResistantRow = ({ resistant }: Props) => {
  const navigate = useNavigate();

  return (
    <>
      <div
        className="row sqs-row resistant-row"
        onClick={() => navigate(resistant.id)}
      >
        <div
          className="col sqs-col-4 span-4"
          id="yui_3_17_2_1_1674987238932_138"
        >
          <div
            className="sqs-block image-block sqs-block-image sqs-text-ready"
            data-block-type="5"
            id="block-yui_3_17_2_1_1662985860031_30104"
            style={{ paddingBottom: "5px" }}
          >
            <div
              className="sqs-block-content"
              id="yui_3_17_2_1_1674987238932_137"
            >
              <div
                className="image-block-outer-wrapper layout-caption-below design-layout-inline combination-animation-none individual-animation-none individual-text-animation-none sqs-narrow-width"
                data-test="image-block-inline-outer-wrapper"
                id="yui_3_17_2_1_1674987238932_136"
              >
                {resistant.video ? (
                  <EmbeddedYoutube
                    youtubeId={resistant.video}
                    width={"200"}
                    height={"200"}
                    onClick={() => navigate(resistant.id)}
                    style={getResistantGrayscaleRule(resistant)}
                  />
                ) : (
                  <img
                    src={getResistantPhotoUrl200(resistant)}
                    alt={resistant.noms}
                    style={{
                      ...getResistantGrayscaleRule(resistant),
                      width: "200px",
                      height: "200px",
                    }}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="col sqs-col-8 span-8">
          <div
            className="sqs-block html-block sqs-block-html"
            data-block-type="2"
          >
            <div className="sqs-block-content">
              <p id="ramin-marjorie">
                <strong>
                  {resistant.noms}, parents de {resistant.enfants}
                </strong>
              </p>

              <p className="" style={{ whiteSpace: "pre-wrap" }}>
                <strong>
                  ({resistant.departement}, Académie de {getAcademie(resistant.departement)}
                  )<br />
                </strong>
                <em>{resistant.mention || ""}</em>
              </p>

              <p className="" style={{ whiteSpace: "pre-wrap" }}>
                {isAncienResistant(resistant) ? (
                  <>
                    En désobéissance civile (
                    <ExternalLink href={getResistantDeclarationUrl(resistant)}>
                      déclaration
                    </ExternalLink>
                    ) de {getResistantDateDeclarationMoisAnnee(resistant)} à{" "}
                    {getResistantDateFinMoisAnnee(resistant)}
                  </>
                ) : (
                  <>
                    <ExternalLink href={getResistantDeclarationUrl(resistant)}>
                      Notre déclaration de désobéissance civile
                    </ExternalLink>
                    , en {getResistantDateDeclarationMoisAnnee(resistant)}
                  </>
                )}
              </p>
            </div>
          </div>
        </div>
      </div>

      <Separator />
    </>
  );
};
