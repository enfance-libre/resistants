import maplibregl from "maplibre-gl";
import "maplibre-gl/dist/maplibre-gl.css";
import React from "react";
import Map, { Marker, NavigationControl, Popup } from "react-map-gl";
import { isAncienResistant, Resistant } from "../../../Resistant";
import { Picto } from "./Picto";
import { PopupContent } from "./PopupContent";

interface Props {
  resistants: Resistant[];
}

const accessToken = "LiH20XNxcFiTXyT4fgjM";

export default ({ resistants }: Props) => {
  const [selectedResistant, selectResistant] = React.useState<Resistant | null>(
    null
  );

  return (
    <Map
      initialViewState={{
        longitude: 2.5,
        latitude: 46.33,
        zoom: 5,
      }}
      style={{ width: "100%", height: 650 }}
      mapLib={maplibregl}
      mapStyle={`https://api.maptiler.com/maps/streets-v2/style.json?key=${accessToken}`}
    >
      <NavigationControl />
      {resistants.map((r, idx) => (
        <Marker
          key={`marker-${r.id}`}
          longitude={r.longitude}
          latitude={r.latitude}
          anchor="center"
          onClick={(e) => {
            e.originalEvent.stopPropagation();
            selectResistant(r);
          }}
          style={{     
            // Affiche les marqueurs des résistants en haut de liste avec le zIndex le plus élevé.
            zIndex: resistants.length - idx 
          }}
        >
          <Picto resistant={r} />
        </Marker>
      ))}
      {selectedResistant && (
        <Popup
          key={`popup-${selectedResistant.id}`}
          longitude={selectedResistant.longitude}
          latitude={selectedResistant.latitude}
          style={{ minWidth: "40em", padding: "1em", zIndex: resistants.length + 10 }}
          onClose={() => selectResistant(null)}
        >
          <PopupContent resistant={selectedResistant} />
        </Popup>
      )}
    </Map>
  );
};
