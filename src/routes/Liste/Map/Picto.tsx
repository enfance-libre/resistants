import { isAncienResistant, Resistant } from "../../../Resistant";
import pictoFondBlancNbUrl from "./picto-fond-blanc-nb.png?url";
import pictoFondBlancUrl from "./picto-fond-blanc.png?url";

interface Props {
  resistant: Resistant;
}

export const Picto = ({ resistant }: Props) => {
  const ancienResistant = isAncienResistant(resistant);
  const size = ancienResistant? "33px": "40px";
  return (
  <img
    src={ancienResistant ? pictoFondBlancNbUrl : pictoFondBlancUrl}
    alt={resistant.noms}
    style={{ 
      cursor: "pointer",
    }}
    height={size}
    width={size}
  />
);
}
