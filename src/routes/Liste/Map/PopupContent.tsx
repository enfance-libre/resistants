import React from "react";
import { useNavigate } from "react-router";
import { EmbeddedYoutube } from "../../../components/EmbeddedYoutube";
import { HomeContext } from "../../../context/HomeContext";
import { getResistantPageUrl } from "../../../getResistantPageUrl";
import { getResistantPhotoUrl200 } from "../../../getResistantPhotoUrl";
import {
  getResistantDateDeclarationMoisAnnee,
  getResistantDateFinMoisAnnee,
  isAncienResistant,
  Resistant,
} from "../../../Resistant";
import { getResistantGrayscaleRule } from "../../../style/getResistantGrayscaleRule";
import { getAcademie } from "../../../getAcademie";

export const PopupContent = ({ resistant }: { resistant: Resistant }) => {
  const navigate = useNavigate();
  const { isHome } = React.useContext(HomeContext);

  return (
    <div
      style={{ minWidth: "30em", minHeight: "15em", cursor: "pointer" }}
      onClick={() =>
        isHome
          ? window.location.assign(getResistantPageUrl(resistant))
          : navigate(resistant.id)
      }
    >
      {resistant.video ? (
        <EmbeddedYoutube
          youtubeId={resistant.video}
          width={"220"}
          height={"120"}
          onClick={() => {}}
          style={{
            float: "left",
            maxHeight: "15em",
            marginRight: "1em",
            paddingBottom: "3em",
          }}
        />
      ) : (
        <img
          src={getResistantPhotoUrl200(resistant)}
          style={{
            ...getResistantGrayscaleRule(resistant),
            float: "left",
            maxHeight: "15em",
            marginRight: "1em",
          }}
          alt={resistant.noms}
        />
      )}
      <strong style={{ overflow: "hidden" }}>{resistant.noms}</strong>
      <br />
      👨‍👩‍👧‍👦 Parent(s) de {resistant.enfants} <br />
      {resistant.mention || ""}
      <br />
      <br />
      🇫🇷 {resistant.departement}, Académie de {getAcademie(resistant.departement)}
      <br />
      {isAncienResistant(resistant) ? (
        <>
          📅 En désobéissance de{" "}
          {getResistantDateDeclarationMoisAnnee(resistant)} à{" "}
          {getResistantDateFinMoisAnnee(resistant)}
        </>
      ) : (
        <>
          📅 En désobéissance depuis{" "}
          {getResistantDateDeclarationMoisAnnee(resistant)}
        </>
      )}
      <br />
    </div>
  );
};
