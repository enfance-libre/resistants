import { AspectRatio } from "@mantine/core";
import { useContext } from "react";
import { useNavigate, useParams } from "react-router";
import {
  getResistantDateDeclarationMoisAnnee,
  getResistantDateFinMoisAnnee,
  isAncienResistant,
} from "../Resistant";
import { getYoutubeEmbedLink } from "../Youtube";
import { ExternalLink } from "../components/ExternalLink";
import { HomeContext } from "../context/HomeContext";
import { getResistantDeclarationUrl } from "../getResistantDeclarationUrl";
import { getResistantPhotoUrl2048 } from "../getResistantPhotoUrl";
import { getResistantsPageUrl } from "../getResistantsPageUrl";
import { resistants } from "../resistants";
import { getAcademie } from "../getAcademie";

export const PageResistant = () => {
  const { idResistant } = useParams();
  const navigate = useNavigate();
  const { isHome } = useContext(HomeContext);

  const resistant = resistants.find((r) => r.id === idResistant);

  if (!resistant) {
    window.location.assign(getResistantsPageUrl());
    return <>Résistant introuvable</>;
  }

  if (isHome) {
    window.location.assign(getResistantsPageUrl());
    return null;
  }

  const declarationUrl = getResistantDeclarationUrl(resistant);
  return (
    <>
      <button
        style={{ padding: "10px", marginBottom: "10px" }}
        onClick={() => navigate("/")}
      >
        Tous les résistants
      </button>
      <div className="row sqs-row">
        <div
          className="col sqs-col-4 span-4"
          id="yui_3_17_2_1_1674987238932_138"
        >
          <div
            className="sqs-block image-block sqs-block-image sqs-text-ready"
            data-block-type="5"
            id="block-yui_3_17_2_1_1662985860031_30104"
          >
            <div
              className="sqs-block-content"
              id="yui_3_17_2_1_1674987238932_137"
            >
              <div
                className="image-block-outer-wrapper layout-caption-below design-layout-inline combination-animation-none individual-animation-none individual-text-animation-none sqs-narrow-width"
                data-test="image-block-inline-outer-wrapper"
                id="yui_3_17_2_1_1674987238932_136"
              >
                <figure
                  className="sqs-block-image-figure intrinsic "
                  style={{ maxWidth: "2048px" }}
                  id="yui_3_17_2_1_1674987238932_135"
                >
                  <div
                    className="image-block-wrapper"
                    data-animation-role="image"
                    id="yui_3_17_2_1_1674987238932_134"
                  >
                    <div
                      className="sqs-image-shape-container-element has-aspect-ratio "
                      style={{
                        position: "relative",
                        paddingBottom: "100%",
                        overflow: "hidden",
                      }}
                      id="yui_3_17_2_1_1674987238932_133"
                    >
                      <noscript>
                        <img
                          src={getResistantPhotoUrl2048(resistant)}
                          alt={resistant.noms}
                        />
                      </noscript>
                      <img
                        className="thumb-image loaded"
                        data-src={getResistantPhotoUrl2048(resistant)}
                        data-image={getResistantPhotoUrl2048(resistant)}
                        data-image-dimensions="2048x2048"
                        data-image-focal-point="0.5,0.5"
                        data-load="false"
                        data-image-id="63cfa9c1bc68a5418b4a2126"
                        data-type="image"
                        style={{
                          left: 0,
                          top: 0,
                          width: "100%",
                          height: "100%",
                          position: "absolute",
                        }}
                        alt={resistant.noms}
                        data-image-resolution="500w"
                        src={getResistantPhotoUrl2048(resistant)}
                      />
                    </div>
                  </div>
                </figure>
              </div>
            </div>
          </div>
          <div
            className="sqs-block html-block sqs-block-html"
            data-block-type="2"
            id="block-yui_3_17_2_1_1663343055963_122180"
          >
            <div className="sqs-block-content">
              {declarationUrl && (
                <p className="" style={{ whiteSpace: "pre-wrap" }}>
                  {isAncienResistant(resistant) ? (
                    <>
                      En désobéissance civile (
                      <ExternalLink href={declarationUrl}>
                        déclaration
                      </ExternalLink>
                      ) de {getResistantDateDeclarationMoisAnnee(resistant)} à{" "}
                      {getResistantDateFinMoisAnnee(resistant)}
                    </>
                  ) : (
                    <>
                      <ExternalLink href={declarationUrl}>
                        Notre déclaration de désobéissance civile
                      </ExternalLink>
                      , en {getResistantDateDeclarationMoisAnnee(resistant)}
                    </>
                  )}
                </p>
              )}
            </div>
          </div>
        </div>
        <div className="col sqs-col-8 span-8">
          <div
            className="sqs-block code-block sqs-block-code"
            data-block-type="23"
            id="block-yui_3_17_2_1_1666274786250_108799"
          >
            <div className="sqs-block-content">
              <p id="ramin-marjorie">
                <strong>
                  {resistant.noms}, parents de {resistant.enfants}
                </strong>
              </p>
            </div>
          </div>
          <div
            className="sqs-block html-block sqs-block-html"
            data-block-type="2"
            id="block-yui_3_17_2_1_1602775217144_4986"
          >
            <div className="sqs-block-content">
              <p className="" style={{ whiteSpace: "pre-wrap" }}>
                <strong>
                  ({resistant.departement}, Académie de {getAcademie(resistant.departement)})
                  <br />
                </strong>
                <em>{resistant.mention || ""}</em>
              </p>
              {resistant.video && (
                <AspectRatio
                  ratio={1080 / 720}
                  maw={600}
                  style={{ marginLeft: "auto", marginRight: "auto" }}
                >
                  <iframe
                    src={getYoutubeEmbedLink(resistant.video)}
                    width="100%"
                    height="100%"
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    allowFullScreen
                  ></iframe>
                </AspectRatio>
              )}
              <p style={{ whiteSpace: "pre-wrap" }}>
                <em>{resistant.presentation}</em>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
