import { Academie } from "./Academie";
import { Departement } from "./Departement";
import { mapDepartementsAcademie } from "./departements";

export function getAcademie(dept: Departement): Academie {
  return mapDepartementsAcademie[dept];
}
