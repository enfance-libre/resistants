export const getPortableAssetAbsoluteUrl = (assetUrl: string): string => {
  const moduleUrl = import.meta.url;
  return new URL(assetUrl, moduleUrl).toString();
};
