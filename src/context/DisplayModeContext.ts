import React from "react";
import { ViewMode } from "../routes/ViewMode";

const defaultValue: DisplayModeContextType = {
  mode: "map",
  setMode: () => {},
};

interface DisplayModeContextType {
  mode: ViewMode;
  setMode: (mode: ViewMode) => void;
}

export const DisplayModeContext =
  React.createContext<DisplayModeContextType>(defaultValue);
