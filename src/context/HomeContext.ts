import React from "react";

const defaultValue: HomeContextType = {
  isHome: false,
  setMode: () => {},
};

interface HomeContextType {
  isHome: boolean;
  setMode: (isHome: boolean) => void;
}

export const HomeContext = React.createContext<HomeContextType>(defaultValue);
