export const getYoutubeLink = (id: string): string =>
  `https://www.youtube.com/watch?v=${id}`;

export const getYoutubeEmbedLink = (id: string): string =>
  `https://www.youtube-nocookie.com/embed/${id}`;
