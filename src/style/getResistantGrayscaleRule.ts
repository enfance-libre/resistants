import { isAncienResistant, Resistant } from "../Resistant";
import React from "react";

export const getResistantGrayscaleRule = (
  resistant: Resistant
): React.CSSProperties => ({
  filter: `grayscale(${isAncienResistant(resistant) ? "80%" : "0"})`,
});
