import { Academie } from "./Academie";
import { mapAcademiesDepartements } from "./departements";

export type Departement = (typeof mapAcademiesDepartements)[Academie][number];

export type DepartementOfAcademie<A extends Academie> =
  (typeof mapAcademiesDepartements)[A][number];
