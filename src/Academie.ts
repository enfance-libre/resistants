import { mapAcademiesDepartements } from "./departements";

export type Academie = keyof typeof mapAcademiesDepartements;
