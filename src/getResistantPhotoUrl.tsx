import { Resistant } from "./Resistant";
import { getPortableAssetAbsoluteUrl } from "./getPortableAssetAbsoluteUrl";

const photoUrls2048 = import.meta.glob("./photos/*.jpg", {
  query: "?url&w=2048&h=2048&format=jpg",
  eager: true,
  import: "default",
});

const photoUrls500 = import.meta.glob("./photos/*.jpg", {
  query: "?url&w=500&h=500&format=jpg",
  eager: true,
  import: "default",
});

const photoUrls200 = import.meta.glob("./photos/*.jpg", {
  query: "?url&w=200&h=200&format=jpg",
  eager: true,
  import: "default",
});

export const getResistantPhotoUrl2048 = (resistant: Resistant): string => {
  const photoUrl = photoUrls2048[`./photos/${resistant.id}.jpg`] as string;
  return getPortableAssetAbsoluteUrl(photoUrl);
};

export const getResistantPhotoUrl500 = (resistant: Resistant): string => {
  const photoUrl = photoUrls500[`./photos/${resistant.id}.jpg`] as string;
  return getPortableAssetAbsoluteUrl(photoUrl);
};

export const getResistantPhotoUrl200 = (resistant: Resistant): string => {
  const photoUrl = photoUrls200[`./photos/${resistant.id}.jpg`] as string;
  return getPortableAssetAbsoluteUrl(photoUrl);
};
