import { Resistant } from "./Resistant";
import { getResistantsPageUrl } from "./getResistantsPageUrl";

export const getResistantPageUrl = (resistant: Resistant) =>
  `${getResistantsPageUrl()}/#/${resistant.id}`;
