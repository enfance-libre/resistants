import { Resistant } from "./Resistant";
import { getPortableAssetAbsoluteUrl } from "./getPortableAssetAbsoluteUrl";

const declarationUrls = import.meta.glob("./declarations/*.pdf", {
  query: "?url",
  eager: true,
  import: "default",
});

const getDefaultResistantDeclarationUrl = (resistantId: string): string => {
  const declarationUrl = declarationUrls[
    `./declarations/${resistantId}.pdf`
  ] as string;
  return getPortableAssetAbsoluteUrl(declarationUrl);
};

export const getResistantDeclarationUrl = (resistant: Resistant): string => {
  if (resistant.lien_declaration === undefined) {
    return getDefaultResistantDeclarationUrl(resistant.id);
  }
  return resistant.lien_declaration;
};
