import React from "react";
import ReactDOM from "react-dom/client";
import { App } from "./App";
import { ViewMode } from "./routes/ViewMode";

const renderResistants = (defaultViewMode: ViewMode, isHome: boolean) => {
  ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
    <React.StrictMode>
      <App defaultViewMode={defaultViewMode} home={isHome} />
    </React.StrictMode>
  );
};

const render = () => {
  const isOnPageResistants = location.href.includes("resistants");
  const defaultViewMode: ViewMode = isOnPageResistants
    ? defaultViewModeSearchParam(location.search) || "photos"
    : "map";
  const isHome = !isOnPageResistants;

  renderResistants(defaultViewMode, isHome);
};

export const isEmbeddedInSquareSpace = (): boolean => {
  return "Squarespace" in window;
};

if (isEmbeddedInSquareSpace()) {
  // @ts-ignore
  window.Squarespace.onInitialize(Y, render);
} else {
  // Load in dev style that is normally injected by squarespace
  import("./style/squarespace-site.css");
  render();
}

function defaultViewModeSearchParam(search: string): ViewMode | undefined {
  const params = new URLSearchParams(search);
  if (!params.has("defaultViewMode")) {
    return undefined;
  }

  const defaultViewModeParam = params.get("defaultViewMode");
  switch (defaultViewModeParam) {
    case "map":
    case "photos":
    case "list":
      return defaultViewModeParam;
    default:
      return undefined;
  }
}
