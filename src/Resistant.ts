import { formatWithOptions, isAfter, parse } from "date-fns/fp";
import { fr } from "date-fns/locale";
import { Academie } from "./Academie";
import { Departement } from "./Departement";

export interface Resistant {
  id: string;
  noms: string;
  enfants: string;
  departement: Departement;
  /**
   * Texte de présentation de la famille.
   */
  presentation: string;
  /**
   * Identifiant youtube de la video de présentation lorsqu'elle existe.
   */
  video?: string;
  /**
   * Mention additionnelle sur la famille. Laissé vide la plupart du temps
   */
  mention?: string;
  /**
   * Lien declaration DC.
   * A laisser "undefined" pour les nouvelles familles dont la declaration doit être mise sous forme de PDF dans le répertoire declarations
   * A définir avec l'URL de la déclaration pour les quelques familles historiques ayant une déclaration externe et à "" pour les quelques familles historique sans déclaration référencée.
   */
  lien_declaration?: string;
  date_declaration: string;
  date_fin_resistance?: string;

  latitude: number;
  longitude: number;
}

export interface AncienResistant extends Resistant {
  date_fin_resistance: string;
}

export const isAncienResistant = (
  resistant: Resistant
): resistant is AncienResistant =>
  Boolean(
    resistant.date_fin_resistance && resistant.date_fin_resistance !== ""
  );

export const isResistantActif = (resistant: Resistant) =>
  !isAncienResistant(resistant);

const r1First = -1;
const r2First = 1;

export const resistantsOrder = (r1: Resistant, r2: Resistant): number => {
  if (r1.id === "ramin-marjorie") return r1First;
  if (r2.id === "ramin-marjorie") return r2First;

  if (isResistantActif(r1) && isAncienResistant(r2)) return r1First;
  if (isAncienResistant(r1) && isResistantActif(r2)) return r2First;

  if (isAfter(getResistantDateDeclaration(r2), getResistantDateDeclaration(r1)))
    return r1First;
  if (isAfter(getResistantDateDeclaration(r1), getResistantDateDeclaration(r2)))
    return r2First;

  return 0;
};

export const sortResistants = (liste: Array<Resistant>): Array<Resistant> =>
  liste.sort(resistantsOrder);

export const getResistantDateDeclaration = (resistant: Resistant): Date =>
  parse(new Date(), "yyyy-MM-dd", resistant.date_declaration);

export const getResistantDateDeclarationMoisAnnee = (
  resistant: Resistant
): string =>
  formatWithOptions(
    { locale: fr },
    "MMMM yyyy",
    getResistantDateDeclaration(resistant)
  );

export const getResistantDateFin = (resistant: AncienResistant): Date =>
  parse(new Date(), "yyyy-MM-dd", resistant.date_fin_resistance);

export const getResistantDateFinMoisAnnee = (
  resistant: AncienResistant
): string | undefined =>
  formatWithOptions(
    { locale: fr },
    "MMMM yyyy",
    getResistantDateFin(resistant)
  );
