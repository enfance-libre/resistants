import { Academie } from "./Academie";
import { Departement } from "./Departement";

// Source https://www.education.gouv.fr/les-regions-academiques-academies-et-services-departementaux-de-l-education-nationale-6557
export const mapAcademiesDepartements = {
  "Aix-Marseille": [
    "Alpes-de-Haute-Provence",
    "Hautes-Alpes",
    "Bouches-du-Rhône",
    "Vaucluse",
  ],
  Amiens: ["Aisne", "Oise", "Somme"],
  Besançon: ["Doubs", "Jura", "Haute-Saône", "Territoire de Belfort"],
  Bordeaux: ["Dordogne", "Gironde", "Landes", "Lot-et-Garonne"],
  "Clermont-Ferrand": ["Allier", "Cantal", "Haute-Loire", "Puy-de-Dôme"],
  Corse: ["Haute-Corse", "Corse-du-Sud"],
  Créteil: ["Seine-et-Marne", "Seine-Saint-Denis", "Val-de-Marne"],
  Dijon: ["Côte-d'Or", "Nièvre", "Saône-et-Loire", "Yonne"],
  Grenoble: ["Ardèche", "Drôme", "Isère", "Savoie", "Haute-Savoie"],
  Lille: ["Nord", "Pas-de-Calais"],
  Limoges: ["Corrèze", "Creuse", "Haute-Vienne"],
  Lyon: ["Rhône"],
  Montpellier: ["Aude", "Gard", "Hérault", "Lozère", "Pyrénées-Orientales"],
  "Nancy-Metz": ["Meurthe-et-Moselle", "Meuse", "Moselle", "Vosges"],
  Nantes: ["Loire-Atlantique", "Maine-et-Loire", "Mayenne", "Sarthe", "Vendée"],
  Nice: ["Alpes-Maritimes", "Var"],
  Normandie: ["Calvados", "Eure", "Manche", "Orne", "Seine-Maritime"],
  "Orléans-Tours": [
    "Cher",
    "Eure-et-Loir",
    "Indre",
    "Indre-et-Loire",
    "Loir-et-Cher",
    "Loiret",
  ],
  Paris: ["Paris"],
  Poitiers: ["Charente", "Charente-Maritime", "Deux-Sèvres", "Vienne"],
  Reims: ["Ardennes", "Aube", "Marne", "Haute-Marne"],
  Rennes: ["Côtes-d'Armor", "Finistère", "Ille-et-Vilaine", "Morbihan"],
  Strasbourg: ["Bas-Rhin", "Haut-Rhin"],
  Toulouse: [
    "Ariège",
    "Aveyron",
    "Haute-Garonne",
    "Gers",
    "Lot",
    "Hautes-Pyrénées",
    "Tarn",
    "Tarn-et-Garonne",
  ],
  Versailles: ["Yvelines", "Essonne", "Hauts-de-Seine", "Val-d'Oise"],
} as const;

export const mapDepartementsAcademie = Object.fromEntries(
  Object.entries(mapAcademiesDepartements).flatMap(([a, deps]) =>
    deps.map((d) => [d as Departement, a as Academie])
  )
);
